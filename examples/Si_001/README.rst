Si 001 example ADF calculation
==============================

The configuration and crystal file in this directory are for a simulation of a 2x2x37 unit cell silicon crystal with
beam direction in 001. The mean spare displacement specified in the crystal file is calculated for T=300K. A defocus
series is calculated with seven single defoci, and for each defocus 10 frozen phonon configurations are simulated.

At 40 probe positions per nm, the resulting image has a resolution of 43x43. The real- and k-space grid at 360./nm, as
specified in the parameter file, is 392x392.

The total amount of multi-slice simulations in this example is 7 x 10 x 43 x 43 = 129430. At a slice thickness of one Si
unit cell, the simulation performs roughly 129430 x 38 x 2 = 9836680 fast fourier transforms.

There are two parameter files in this directory, ``Si_001.cfg`` and ``Si_001_mod.cfg``. The former includes all
parameters that can be specified, while the latter includes only those parameters, that are not the default. We
recommend to always include all parameters in your configuration files to eliminate errors and differences between
*STEMsalabim* versions!