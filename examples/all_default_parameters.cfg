application: {
    random_seed = 0;                   # the random seed. 0 -> generate random random seed
}

simulation: {
    title = "";                        # title of the simulation
    bandwidth_limiting = true;         # bandwidth limit the wave functions?
    normalize_always = false;          # normalize wave functions after each slice?
    output_file = "";                  # output file name
    output_compress = false;           # compress output. Reduces the output size, but increases IO times.
    chunking_enabled = true;           # chunk the stored data.
    save_probe_wavefunction = false    # save the probe wavefunction at each scan point
}

probe: {
    c5 = 5e6;                          # Fifth order spherical aberrations coefficient. in nm
    cs = 2e3;                          # Third order spherical aberrations coefficient. in nm
    defocus = 0;                       # defocus in nm.
    delta_defocus_max = 6.0;           # The maximum value which will be added/subtracted to/from the mean defocus value. Used for simulating a defocus series for Cc.
    fwhm_defocus_distribution = 7.5;   # Full width at half maximum of the Gaussian which determines the defocus weigths for defocus series for simulations of chromatic aberration.
    num_defoci = 1;                    # number of the defoci when simulating a defocus series for Cc. Should be odd.
    astigmatism_ca = 0;                # Two-fold astigmatism. in nm
    astigmatism_angle = 0;             # Two-fold astigmatism angle. in mrad
    min_apert = 0.0;                   # Minimum numerical aperture of the objective. in mrad
    max_apert = 24.0;                  # Maximum numerical aperture of the objective. in mrad
    beam_energy = 200.0;               # Electron beam energy. in kV
    scan_density = 40;                 # The sampling density for the electron probe scanning. in 1/nm
    aperture_bitmap_file = ""          # Path to file which gives values for aperture function

}

specimen: {
    max_potential_radius = 0.3;        # potential cut-off radius. in nm
    crystal_file = "";                 # xyz file with columns [Element, x, y, z, MSD]
}

grating: {
    density = 360.0;                   # The density for the real space and fourier space grids. in 1/nm
    slice_thickness = 0.2715;          # Multi-slice slice thickness. in nm
}

frozen_phonon: {
    enabled = true;                    # whether or not to use the frozen phonon approximation at all.
    number_configurations = 1;         # Number of frozen phonon configurations to calculate
    fixed_slicing = true;              # When this is true, the z coordinate is not varied during phonon vibrations.
}

adf: {
    enabled = true;                    # whether ADF intensities are calculated and stored
    x = (0.0, 1.0);                    # [min, max] where min and max are in relative units
    y = (0.0, 1.0);                    # [min, max] where min and max are in relative units
    detector_min_angle = 1.0;          # inner detector angle in mrad
    detector_max_angle = 300.0;        # outer detector angle in mrad
    detector_num_angles = 301;         # number of bins of the ADF detector.
    detector_interval_exponent = 1.0;  # possibility to set non-linear detector bins.
    save_slices_every = 1;             # save only every n slices. 0 -> only the sample bottom is saved.
    average_configurations = true;     # average the frozen phonon configurations in the output file
    average_defoci = true;             # average the defoci in the output file
}

cbed: {
    enabled = false;                   # whether CBED intensities are calculated and stored
    x = (0.0, 1.0);                    # [min, max] where min and max are in relative units
    y = (0.0, 1.0);                    # [min, max] where min and max are in relative units
    save_slices_every = 0;             # save only every n slices. 0 -> only the sample bottom is saved.
    size = [0, 0];                     # When provided, this parameter determines the size of CBEDs saved
                                       # to the output file. The CBEDs are resized using bilinear interpolation.
    average_configurations = true;     # average the frozen phonon configurations in the output file
    average_defoci = true;             # average the defoci in the output file
    max_angle = 0;                     # Maximum angle up to which CBEDs are saved
}

plasmon_scattering: {
    enabled = false;                            # whether plasmon scattering is calculated
    mean_free_path = 120;                       # mean free path of a plasmon in nm
    plasmon_energy = 16.6;                      # plasmon energy in eV
    plasmon_fwhm = 3.7;                         # plasmon energy FWHM in eV
    energy_loss = 16.6;                         # energy loss in eV
    plasmons_scatteringfunction_cutoff = 3;     # radial cutoff of the plasmon scattering function. in nm
    number_of_potential_positionsPerSlice = 0;  # the number of the positions to which the plasmon scattering function is translated
    calculate_plasmons_in_all_slices = true;    # calculate plasmon scattering in all slices?
    save_slices_every = 0;                      # Calculate on only every n slices. 0 -> only the sample bottom is saved.
}

specimen_tilt: {
        enabled = false;                        # whether specimen tilt is included
        specimen_tilt_angle_x = 0;              # tilt angle in x direction in mrad
        specimen_tilt_angle_y = 0;              # tilt angle in y direction in mrad
        adjust_slice_thickness = false;         # whether to adjust the slice thickness

    }
beam_tilt: {
        enabled = false;                        # whether specimen tilt is included
        beam_tilt_angle_x = 0;                  # tilt angle in x direction in mrad
        beam_tilt_angle_y = 0;                  # tilt angle in y direction in mrad
        adjust_slice_thickness = false;         # whether to adjust the slice thickness
    }
