/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "catch.hpp"
#include <cstdio>
#include <string>
#include <netcdf.h>
#include <iostream>

#include "../src/utilities/netcdf_utils.hpp"

using namespace stemsalabim;

std::string errmsg(int errcode) {
    const char *msg = nullptr;
    if(NC_ISSYSERR(errcode)) {
        msg = strerror(errcode);
        msg = msg ? msg : "NetCDF: Unknown system error";
    } else {
        msg = nc_strerror(errcode);
    }
    return std::string(msg);
}

TEST_CASE("Test create file", "[io]") {
    std::string filename = std::tmpnam(nullptr);

    {
        NCFile a(filename, true);
    }

    int id, grpid;
    int ret = nc_open(filename.c_str(), NC_WRITE, &id);
    REQUIRE(ret == NC_NOERR);
    nc_close(id);

    SECTION("Creating a group") {
        {
            NCFile a(filename);
            a.defineGroup("test");
        }

        int ret_open = nc_open(filename.c_str(), NC_NOWRITE, &id);
        int ret_grp = nc_inq_grp_ncid(id, "test", &grpid);
        INFO(errmsg(ret_grp));
        REQUIRE(ret_open == NC_NOERR);
        REQUIRE(ret_grp == NC_NOERR);
    }

    SECTION("Retrieving a group") {
        {
            NCFile a(filename);
            a.defineGroup("test");
        }

        {
            NCFile a(filename);
            REQUIRE_NOTHROW(a.group("test"));
        }
    }


    remove(filename.c_str());
}
