/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "catch.hpp"
#include <cmath>
#include <iostream>
#include "../src/utilities/netcdf_utils.hpp"
#include "../src/utilities/algorithms.hpp"

// Since the high level tests always depend on paths to netcdf files, they are left optional.
// The tester has to make sure to create the right nc files with the right parameters and set the paths correctly.
bool test_adf_zero_loss = false;
bool test_adf_plasmon_last_slice_loss = false;
bool test_defocus_weights = false;
bool test_com_full_CBED_vs_old_version = false;
bool test_adf_plasmons_in_own_variable = false;
bool test_external_potential_file = false;


std::vector<float> get_adf_zero_loss_intensity(std::string filename_nc){
    typedef std::vector<size_t> vs;

    stemsalabim::NCFile nc_file(filename_nc, false, true);
    auto adf_group_reference = nc_file.group("adf");
    auto adf_intensities_nc_type_reference = adf_group_reference.var("adf_intensities");

    size_t number_defoci = adf_group_reference.getDimensionLength("adf_defocus");
    size_t number_scan_points_x = adf_group_reference.getDimensionLength("adf_position_x");
    size_t number_scan_points_y = adf_group_reference.getDimensionLength("adf_position_y");
    size_t number_phonon_configs = adf_group_reference.getDimensionLength("adf_phonon");
    size_t number_slices = adf_group_reference.getDimensionLength("adf_slice");
    size_t number_angles = adf_group_reference.getDimensionLength("adf_detector_angle");

    unsigned int plasmon_index = 1; // Choose only zero loss dimension

    return adf_intensities_nc_type_reference.get<float>(vs({0, 0, 0, 0, 0, 0, 0}),
                                                                                    vs({number_defoci,
                                                                                        number_scan_points_x,
                                                                                        number_scan_points_y,
                                                                                        number_phonon_configs,
                                                                                        number_slices,
                                                                                        plasmon_index,
                                                                                        number_angles}));
}

std::vector<float> get_adf_intensity_plasmon_last_slice(std::string filename_nc){
    typedef std::vector<size_t> vs;

    stemsalabim::NCFile nc_file(filename_nc, false, true);
    auto adf_group_reference = nc_file.group("adf");
    auto adf_intensities_nc_type_reference = adf_group_reference.var("adf_intensities");
    std::vector<float> adf_intensities_vector_reference;

    size_t number_defoci = adf_group_reference.getDimensionLength("adf_defocus");
    size_t number_scan_points_x = adf_group_reference.getDimensionLength("adf_position_x");
    size_t number_scan_points_y = adf_group_reference.getDimensionLength("adf_position_y");
    size_t number_phonon_configs = adf_group_reference.getDimensionLength("adf_phonon");
    size_t number_slices = adf_group_reference.getDimensionLength("adf_slice");
    size_t number_angles = adf_group_reference.getDimensionLength("adf_detector_angle");
    size_t number_plasmon = adf_group_reference.getDimensionLength("adf_plasmon_dimension");

    unsigned int plasmon_begin = 1;
    unsigned int plasmon_end = number_plasmon - 1;
    return adf_intensities_nc_type_reference.get<float>(vs({0, 0, 0, 0, number_slices -1, plasmon_begin, 0}),
                                                        vs({number_defoci,
                                                            number_scan_points_x,
                                                            number_scan_points_y,
                                                            number_phonon_configs,
                                                            1,
                                                            plasmon_end, //number_plasmon,
                                                            number_angles}));

}

std::vector<float> get_adf_intensities_plasmon_total(std::string filename_nc) {
    typedef std::vector<size_t> vs;

    stemsalabim::NCFile nc_file(filename_nc, false, true);
    auto adf_group_reference = nc_file.group("adf");
    auto adf_intensities_nc_type_reference = adf_group_reference.var("adf_intensities");
    std::vector<float> adf_intensities_vector_reference;

    size_t number_defoci = adf_group_reference.getDimensionLength("adf_defocus");
    size_t number_scan_points_x = adf_group_reference.getDimensionLength("adf_position_x");
    size_t number_scan_points_y = adf_group_reference.getDimensionLength("adf_position_y");
    size_t number_phonon_configs = adf_group_reference.getDimensionLength("adf_phonon");
    size_t number_slices = adf_group_reference.getDimensionLength("adf_slice");
    size_t number_angles = adf_group_reference.getDimensionLength("adf_detector_angle");
    size_t number_plasmon = adf_group_reference.getDimensionLength("adf_plasmon_dimension");

    unsigned int plasmon_begin = 1;
    unsigned int plasmon_end = number_plasmon - 1;
    return adf_intensities_nc_type_reference.get<float>(vs({0, 0, 0, 0, 0, plasmon_begin, 0}),
                                                        vs({number_defoci,
                                                            number_scan_points_x,
                                                            number_scan_points_y,
                                                            number_phonon_configs,
                                                            number_slices,
                                                            plasmon_end, //number_plasmon,
                                                            number_angles}));
}

std::vector<float> get_adf_intensities_plasmon_total_from_own_variable(std::string filename_nc) {
    typedef std::vector<size_t> vs;

    stemsalabim::NCFile nc_file(filename_nc, false, true);
    auto adf_plasmon_group = nc_file.group("plasmon");
    auto adf_plasmon_intensities_nc_type = adf_plasmon_group.var("adf_plasmon_intensities");
    std::vector<float> adf_plasmon_intensities_vector;

    size_t number_defoci = adf_plasmon_group.getDimensionLength("adf_defocus");
    size_t number_scan_points_x = adf_plasmon_group.getDimensionLength("adf_position_x");
    size_t number_scan_points_y = adf_plasmon_group.getDimensionLength("adf_position_y");
    size_t number_phonon_configs = adf_plasmon_group.getDimensionLength("adf_phonon");
    size_t number_slices = adf_plasmon_group.getDimensionLength("adf_slice");
    size_t number_angles = adf_plasmon_group.getDimensionLength("adf_detector_angle");
    size_t number_B_positions = adf_plasmon_group.getDimensionLength("plasmon_potential_positions");

    return adf_plasmon_intensities_nc_type.get<float>(vs({0, 0, 0, 0, 0, 0, 0}),
                                                      vs({number_defoci,
                                                          number_scan_points_x,
                                                          number_scan_points_y,
                                                          number_phonon_configs,
                                                          number_slices,
                                                          number_B_positions,  // probably -1 must be added
                                                          number_angles}));
}

// Test if the zero-loss-only computation gives the right results for ADF.
// Plasmons have to be switched off in cfg file, because otherwise the zero loss intensity will be reduced.
// This test has to be done with the results from a computation with the parameters in tests/reference_files/Si_zero-loss_ADF_ssb-ver5-1-0.cfg
TEST_CASE("ADF: Zero Loss", "[high_level]") {
    if (test_adf_zero_loss){
        typedef std::vector<size_t> vs;

        std::string filename_nc_reference = "../../tests/reference_files/Si_zero-loss_ADF_ssb-ver5-1-0.nc";
        std::string filename_to_test = "/home/damien/CLionProjects/STEMsalabim/cmake-build-debug/src/Si_from_stemsalabim.nc";

        std::vector<float> adf_intensities_vector_reference;
        adf_intensities_vector_reference = get_adf_zero_loss_intensity(filename_nc_reference);

        std::vector<float> adf_intensities_vector_to_test;
        adf_intensities_vector_to_test = get_adf_zero_loss_intensity(filename_to_test);

        float tolerance = 0.001;
        REQUIRE(stemsalabim::algorithms::is_almost_equal<float>(adf_intensities_vector_to_test, adf_intensities_vector_reference, tolerance));
    }

}

// Test if the plasmon computation for last slice only gives the right results for ADF.
// This test has to be done with the results from a computation with the parameters in tests/reference_files/Si_plasmon_ADF_ssb-ver5-2-0.cfg.
TEST_CASE("ADF: Plasmons last slice", "[high_level]") {
    if(test_adf_plasmon_last_slice_loss){
        typedef std::vector<size_t> vs;

        std::string filename_nc_reference = "../../tests/reference_files/Si_plasmon_ADF_ssb-ver5-2-0.nc";
        std::string filename_to_test = "../src/Si_def0.nc";

        std::vector<float> adf_intensities_vector_reference;
        adf_intensities_vector_reference = get_adf_intensity_plasmon_last_slice(filename_nc_reference);

        std::vector<float> adf_intensities_vector_to_test;
        adf_intensities_vector_to_test = get_adf_intensity_plasmon_last_slice(filename_to_test);

        float tolerance = 0.000001;
        REQUIRE(stemsalabim::algorithms::is_almost_equal<float>(adf_intensities_vector_to_test,
                                                                adf_intensities_vector_reference,
                                                                tolerance));
    }
}

std::vector<float> get_defoci_from_nc_file(std::string filename_nc){
    stemsalabim::NCFile nc_file(filename_nc, false, true);

    auto adf_group_reference = nc_file.group("adf");
    size_t number_defoci = adf_group_reference.getDimensionLength("adf_defocus");

    typedef std::vector<size_t> vs;
    auto params_group = nc_file.group("params");
    auto defocus_nc_format = params_group.var("defocus");
    return defocus_nc_format.get<float>(vs({0}), vs({number_defoci}));
}

std::vector<float> get_defocus_weigths_from_nc_file(std::string filename_nc){
    stemsalabim::NCFile nc_file(filename_nc, false, true);

    auto adf_group_reference = nc_file.group("adf");
    size_t number_defoci = adf_group_reference.getDimensionLength("adf_defocus");

    typedef std::vector<size_t> vs;
    auto params_group = nc_file.group("params");
    auto defocus_nc_format = params_group.var("defocus_weights");
    return defocus_nc_format.get<float>(vs({0}), vs({number_defoci}));

}

// Test if the defoci and defocus weights are set correctly.
// This test has to be done with the results from a computation with the parameters in tests/reference_files/Si_defocus.cfg.
    TEST_CASE("Defocus weights", "[high_level]") {
    if (test_defocus_weights) {
        typedef std::vector<size_t> vs;

        std::string filename_to_test = "../src/Si_def0.nc";
        std::vector<float> defoci_reference = {-12, -8, -4, 0, 4, 8, 12};
        std::vector<float> defocus_weights_reference = {0.0004, 0.0214, 0.2277, 0.5010, 0.2277, 0.0214, 0.0004};

        std::vector<float> defoci_from_function = get_defoci_from_nc_file(filename_to_test);

        std::vector<float> defocus_weights_from_function = get_defocus_weigths_from_nc_file(filename_to_test);

        float tolerance = 1;
        REQUIRE(stemsalabim::algorithms::is_almost_equal<float>(defoci_from_function,
                                                                defoci_reference,
                                                                tolerance) == true);

        tolerance = 0.0001;
        REQUIRE(stemsalabim::algorithms::is_almost_equal<float>(defocus_weights_from_function,
                                                                defocus_weights_reference,
                                                                tolerance) == true);
    }
}

std::vector<float> get_com_from_ssb5_3_0_or_lower(std::string filename_nc){
    stemsalabim::NCFile nc_file(filename_nc, false, true);
    auto adf_group_reference = nc_file.group("adf");

//    auto com_nc_format = adf_group_reference.var("center_of_mass");


    typedef std::vector<size_t> vs;
    auto com_nc_format = adf_group_reference.var("center_of_mass");
    std::vector<float> com;

    size_t number_defoci = adf_group_reference.getDimensionLength("adf_defocus");
    size_t number_scan_points_x = adf_group_reference.getDimensionLength("adf_position_x");
    size_t number_scan_points_y = adf_group_reference.getDimensionLength("adf_position_y");
    size_t number_phonon_configs = adf_group_reference.getDimensionLength("adf_phonon");
    size_t number_slices = adf_group_reference.getDimensionLength("adf_slice");
    size_t com_directions = adf_group_reference.getDimensionLength("coordinate_dim");

    unsigned int plasmon_index = 1; // Choose only zero loss dimension

    return com_nc_format.get<float>(vs({0, 0, 0, 0, 0, 0, 0}),
                                    vs({number_defoci,
                                        number_scan_points_x,
                                        number_scan_points_y,
                                        number_phonon_configs,
                                        number_slices,
                                        plasmon_index,
                                        com_directions}));


}

// Test if the com which is calculated over the whole CBED as it was implemented up to version 5.3.0 still gives the same result as in 5.3.0.
TEST_CASE("com_full_CBED_vs_old_version", "[high_level]") {
    if (test_com_full_CBED_vs_old_version) { ;
        std::string filename_nc_reference = "../../tests/reference_files/Si_zero-loss_ADF_ssb-ver5-1-0.nc";
        std::string filename_to_test = "../src/Si_def0.nc";

        std::vector<float> com_total_from_function;
        std::vector<float> com_total_reference;

        com_total_reference = get_com_from_ssb5_3_0_or_lower(filename_nc_reference);
        com_total_from_function = get_com_from_ssb5_3_0_or_lower(filename_to_test);

        float tolerance = 0.0001;

        REQUIRE(stemsalabim::algorithms::is_almost_equal<float>(com_total_from_function,
                                                                com_total_reference,
                                                                tolerance) == true);
    }

}

// Test if the variable which stores the plasmon ADFs gives the same results as in the former version
// for the case that it is calculated at all slices. If not all slices would be calculated, then the
// random positions for the plasmon scattering function B(x) would differ.
// This test has to be done with the results from a computation with the parameters in tests/reference_files/Si_plasmon_ADF_ssb-ver5-3-0.cfg.
TEST_CASE("ADF: Plasmons in own variable", "[high_level]") {
    if(test_adf_plasmons_in_own_variable){
        typedef std::vector<size_t> vs;

        std::string filename_nc_reference = "../../tests/reference_files/Si_plasmon_ADF_ssb-ver5-3-0.nc";
        std::string filename_to_test = "../../files_for_debugging/Si_plasmon_ADF_ssb-ver5-3-0_from_new_version.nc";

        std::vector<float> adf_intensities_reference;
        adf_intensities_reference = get_adf_intensities_plasmon_total(filename_nc_reference);

        std::vector<float> adf_intensities_to_test;
        adf_intensities_to_test = get_adf_intensities_plasmon_total_from_own_variable(filename_to_test);

        float tolerance = 0.000001;
        REQUIRE(stemsalabim::algorithms::is_almost_equal<float>(adf_intensities_to_test,
                                                                adf_intensities_reference,
                                                                tolerance));
    }
}

// Test if storing potentials with ssb-mkin in a separate file gives the same results as the usual calculation.
// To run the test, do:
//  - run ssb-mkin with --stored-potentials --external-potential-file potentials_file.nc
//  - run ssb-run with '--external-potential-file potentials_file.nc'
// This test has to be done with the results from a computation with the parameters in tests/reference_files/Si_zero-loss_ADF_ssb-ver5-1-0.cfg.
TEST_CASE("ADF: stored-potentials in extra file", "[high_level]") {
    if(test_external_potential_file){
        typedef std::vector<size_t> vs;

        std::string filename_nc_reference = "../../tests/reference_files/Si_zero-loss_ADF_ssb-ver5-1-0.nc";
        std::string filename_to_test = "/home/damien/CLionProjects/STEMsalabim/cmake-build-debug/src/Si_from_ssb-mkin.nc";

        std::vector<float> adf_intensities_reference;
        adf_intensities_reference = get_adf_zero_loss_intensity(filename_nc_reference);

        std::vector<float> adf_intensities_to_test;
        adf_intensities_to_test = get_adf_zero_loss_intensity(filename_to_test);

        float tolerance = 0.0001;
        REQUIRE(stemsalabim::algorithms::is_almost_equal<float>(adf_intensities_to_test,
                                                                adf_intensities_reference,
                                                                tolerance));
    }
}