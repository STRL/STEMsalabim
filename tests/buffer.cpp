/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "catch.hpp"
#include <random>
#include <iostream>
#include <functional>
#include "../src/utilities/memory.hpp"

#define NUM_NUMBERS 5

using namespace stemsalabim;

TEST_CASE("Test the memory buffer", "[buffer]") {

    std::random_device rnd_device;
    std::mt19937 mersenne_engine(rnd_device());
    std::uniform_real_distribution<float> dist(1, 10000);

    auto gen = std::bind(dist, mersenne_engine);

    memory::buffer::number_buffer<float> a(NUM_NUMBERS, 10);

    std::vector<float> vec1(NUM_NUMBERS, 10);
    auto it1 = a.add(vec1);

    std::vector<float> vec2(NUM_NUMBERS, 20);
    auto it2 = a.add(vec2);

    std::vector<float> vec3(NUM_NUMBERS, 30);
    auto it3 = a.add(vec3);

    std::vector<float> vec4(NUM_NUMBERS, 40);
    auto it4 = a.add(vec4);

    REQUIRE(a.get(it1).byte_size() == sizeof(float) * NUM_NUMBERS);

    for(auto &val: a.get(it1)) {
        REQUIRE(val == 10);
    }

    a.remove(it3);

    std::vector<float> vec5(NUM_NUMBERS, 99);
    auto it5 = a.add(vec5);

    for(size_t i = 0; i < it5.size(); ++i) {
        REQUIRE(a.value(it5, i) == 99);
    }

    *a.ptr(it5) = 10;
    REQUIRE(a.value(it5, 0) == 10);



}