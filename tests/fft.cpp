/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "catch.hpp"
#include <complex>
#include "../src/utilities/output.hpp"
#include "../src/utilities/algorithms.hpp"
#include "../src/utilities/Wave.hpp"

TEST_CASE("Fourier Transforms", "[fft]") {
    stemsalabim::Wave test1, test2;
    test1.init(10, 10, std::complex<float>(3.0, 2.0));
    test2.init(10, 10, std::complex<float>(2.0, 1.0));
    REQUIRE(test1[1].real() == 3.0);
    REQUIRE(test1[1].imag() == 2.0);

    test1[1] = std::complex<float>(5.0, 4.0);
    REQUIRE(test1[1].real() == 5.0);
    REQUIRE(test1[1].imag() == 4.0);

    test2 *= test1;

    auto res = std::complex<float>(5.0, 4.0) * std::complex<float>(2.0, 1.0);

    REQUIRE(test2[1].real() == res.real());
    REQUIRE(test2[1].imag() == res.imag());

}
