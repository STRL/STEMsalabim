/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "catch.hpp"
#include <complex>
#include <cmath>

static double bessi0(double x) {
    double ax, ans;
    double y;


    if((ax = fabs(x)) < 3.75) {
        y = x / 3.75, y = y * y;
        ans = 1.0 +
              y *
              (3.5156229 +
               y * (3.0899424 + y * (1.2067492 + y * (0.2659732 + y * (0.360768e-1 + y * 0.45813e-2)))));
    } else {
        y = 3.75 / ax;
        ans = (exp(ax) / sqrt(ax)) *
              (0.39894228 +
               y *
               (0.1328592e-1 +
                y *
                (0.225319e-2 +
                 y *
                 (-0.157565e-2 +
                  y *
                  (0.916281e-2 +
                   y * (-0.2057706e-1 + y * (0.2635537e-1 + y * (-0.1647633e-1 + y * 0.392377e-2))))))));
    }
    return ans;
}

/*!
 * irregular modified cylindrical Bessel function order 0
 * @param x argument
 * @return irregular modified cylindrical Bessel function order 0 of x
 */
static double bessk0(double x) {
    double y, ans;

    if(x <= 2.0) {
        y = x * x / 4.0;
        ans = (-log(x / 2.0) * bessi0(x)) +
              (-0.57721566 +
               y *
               (0.42278420 +
                y * (0.23069756 + y * (0.3488590e-1 + y * (0.262698e-2 + y * (0.10750e-3 + y * 0.74e-5))))));
    } else {
        y = 2.0 / x;
        ans = (exp(-x) / sqrt(x)) *
              (1.25331414 +
               y *
               (-0.7832358e-1 +
                y *
                (0.2189568e-1 + y * (-0.1062446e-1 + y * (0.587872e-2 + y * (-0.251540e-2 + y * 0.53208e-3))))));
    }
    return ans;
}

TEST_CASE("Bessel functions", "[math]") {
    REQUIRE(std::cyl_bessel_i(0, 2.0) == Approx(bessi0(2.0)));
    REQUIRE(std::cyl_bessel_k(0, 2.0) == Approx(bessk0(2.0)));

}
