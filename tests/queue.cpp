/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "catch.hpp"

#include <thread>

#include "../src/utilities/TaskQueue.hpp"


struct TestStructure {
    int id;
};

TEST_CASE("Testing the task queue", "[queue]") {

    std::vector<TestStructure> tasks(100);
    int cnt = 0;
    for(auto & t : tasks) {
        t.id = cnt++;
    }

    stemsalabim::TaskQueue work;
    work.append(tasks);

    std::vector<std::thread> threads;
    for(unsigned int nt = 0; nt < 3; nt++) {
        threads.emplace_back([&work] {
            unsigned int t;
            while(work.pop(t)) {
                work.finish(t);
            }
        });
    }

    unsigned int t;
    while(work.pop(t)) {
        //std::cout << t.id() << ":" << t.object()->id << std::endl;
        work.finish(t);
    }

    for(auto & thr: threads)
        thr.join();

    REQUIRE(work.finished());
}
