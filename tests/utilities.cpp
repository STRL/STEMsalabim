/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "catch.hpp"
#include "../src/utilities/output.hpp"
#include "../src/utilities/algorithms.hpp"

using namespace stemsalabim;

TEST_CASE("Time format", "[time]") {

    std::chrono::microseconds ms1(10);
    REQUIRE(!output::humantime(ms1).compare("010us"));

    std::chrono::microseconds ms2(1001);
    REQUIRE(!output::humantime(ms2).compare("001ms:001us"));

    std::chrono::milliseconds ms3(1001);
    REQUIRE(!output::humantime(ms3).compare("01s:001ms"));

    std::chrono::hours ms4(4);
    REQUIRE(!output::humantime(ms4).compare("04h:00m:00s"));

    std::chrono::microseconds ms5((unsigned long)(1 * 1e6 * 3600 * 24 + 1));
    REQUIRE(!output::humantime(ms5).compare("01d:00h:00m"));

    std::chrono::microseconds ms6((unsigned long)(1222515));
    REQUIRE(!output::humantime(ms6).compare("01s:222ms"));
}

TEST_CASE("Distributed bins", "[algorithms]") {
    auto a = algorithms::adaptiveSpace(5, 20, 3, 1.5, true);

    REQUIRE(a[0] == 5);
    REQUIRE(a[2] == 20);
    REQUIRE(std::abs(a[1] - 10.3033) < 0.01);


    auto b = algorithms::adaptiveSpace(0, 300, 301, 1.0, true);

    std::cout << b[0] << "  " << b[1]<< "  " << b[300] << std::endl;

    auto i = algorithms::getIndexOfAdaptiveSpace(300.5, 0, 300, 301, 1.0, true);
    std::cout << i << std::endl;

    //REQUIRE(b[0] == 5);
    //REQUIRE(b[2] == 20);

}