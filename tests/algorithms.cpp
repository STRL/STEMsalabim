/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "catch.hpp"
#include <cmath>
#include <iostream>
#include "../src/utilities/algorithms.hpp"


TEST_CASE("Algorithms", "[Algorithms]") {
    SECTION("is_almost_equal") {
        std::vector<float> v1 = {1, 2, 3};
        std::vector<float> v2 = {1, 2, 3.01};

        float tolerance = 0.1;
        REQUIRE(stemsalabim::algorithms::is_almost_equal<float>(v1, v2, tolerance) == true);

        tolerance = 0.0001;
        REQUIRE(stemsalabim::algorithms::is_almost_equal<float>(v1, v2, tolerance) == false);
    }

    SECTION("defoci weights") {
        double fwhm = 12;
        double mean = 0;
        unsigned int num_defoci = 7;
        float fwhm_defocus_distribution = 7.5;

        std::vector<float> defoci_reference = {-12, -8, -4, 0, 4, 8, 12};
        std::vector<float> defocus_weights_reference = {0.0004, 0.0214, 0.2277, 0.5010, 0.2277, 0.0214, 0.0004};

        std::vector<float> defoci_from_function;
        std::vector<float> defocus_weights_from_function;

        stemsalabim::algorithms::set_defoci_and_defocus_weights(fwhm,
                                                                mean,
                                                                num_defoci,
                                                                fwhm_defocus_distribution,
                                                                defoci_from_function,
                                                                defocus_weights_from_function);

        float tolerance = 1;
        REQUIRE(stemsalabim::algorithms::is_almost_equal<float>(defoci_from_function, defoci_reference, tolerance) ==
                true);

        tolerance = 0.0001;
        REQUIRE(stemsalabim::algorithms::is_almost_equal<float>(defocus_weights_from_function,
                                                                defocus_weights_reference,
                                                                tolerance) == true);
    }

}
