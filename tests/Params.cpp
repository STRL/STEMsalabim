/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "catch.hpp"
#include <cmath>
#include "../src/classes/Params.hpp"


// Test if the work_package_size together with the other parallelization parameters gives a valid ensemble.
// Make sure that all CPU cores have work to do and are not idle from the start.
TEST_CASE("Package size", "[Package size]") {
    stemsalabim::Params &p = stemsalabim::Params::getInstance();
    // Valid
    int work_package_size = 40;
    int number_adf_scanpoints = 400;
    int mpi_env_size = 2;
    int number_of_threads = 4;
    REQUIRE(p.workPackageSizeValid(work_package_size, number_adf_scanpoints, mpi_env_size, number_of_threads));

    // Not valid, because with the number of nodes not all nodes get work with the chosen package size.
    work_package_size = 40;
    number_adf_scanpoints = 400;
    mpi_env_size = 11;
    number_of_threads = 4;
    REQUIRE(not p.workPackageSizeValid(work_package_size, number_adf_scanpoints, mpi_env_size, number_of_threads));

    // Not valid, because the package size is too small for the number of cores per node.
    work_package_size = 10;
    number_adf_scanpoints = 400;
    mpi_env_size = 11;
    number_of_threads = 20;
    REQUIRE(not p.workPackageSizeValid(work_package_size, number_adf_scanpoints, mpi_env_size, number_of_threads));
}