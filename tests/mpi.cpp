/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "catch.hpp"
#include "../src/utilities/mpi.hpp"
#include "../src/classes/GridManager.hpp"

using namespace stemsalabim;

TEST_CASE("MPI operations that require MPI Environment", "[mpi]") {
    mpi::Environment &env = mpi::Environment::getInstance();
    if(env.isMpi()) {

        SECTION("testing float vector") {

            std::vector<float> bla(100, 0);

            if(env.isMaster()) {

                for(unsigned int i = 1; i < bla.size(); i++)
                    bla[i] = bla[i - 1] + (float) i;

                env.send(bla, 1, 0);
            } else if(env.rank() == 1) {
                env.recv(bla, 0, MPI_ANY_TAG);
                REQUIRE(bla[1] == 1);
                REQUIRE(bla[2] == 3);
            }
        }

        SECTION("testing double vector") {

            std::vector<double> bla(100, 0);

            if(env.isMaster()) {

                for(unsigned int i = 1; i < bla.size(); i++)
                    bla[i] = bla[i - 1] + (double) i;

                env.send(bla, 1, 0);
            } else if(env.rank() == 1) {
                env.recv(bla, 0, MPI_ANY_TAG);
                REQUIRE(bla[1] == 1);
                REQUIRE(bla[2] == 3);
            }
        }

        SECTION("testing string send/recv") {
            std::string bla;

            if(env.isMaster()) {
                bla = "abc";
                env.send(bla, 1, 0);
            } else if(env.rank() == 1) {
                env.recv(bla, 0, MPI_ANY_TAG);
                REQUIRE(!bla.compare("abc"));
            }

        }

        SECTION("Testing string broadcast") {

            std::string bla;

            if(env.isMaster())
                bla = "abc";

            env.broadcast(bla, 0);

            if(env.isSlave())
                REQUIRE(!bla.compare("abc"));
        }

    }

}
