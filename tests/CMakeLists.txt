# STEMsalabim: Magical STEM image simulations
#
# Authors: Jan Oliver Oelerich
#          Damien Heimes <damien.heimes@physik.uni-marburg.de>
#
# Copyright (c) 2016-2019 Jan Oliver Oelerich
# Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
# Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!


# compilation
add_executable(ssb-test main.cpp mpi.cpp queue.cpp utilities.cpp fft.cpp buffer.cpp math.cpp nc.cpp plasmons.cpp high_level.cpp algorithms.cpp Params.cpp)


# linking ################################################

target_link_libraries(ssb-test m ${LIBS})

# MPI ####################################################

if(MPI_COMPILE_FLAGS)
    set_target_properties(ssb-test PROPERTIES COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif(MPI_COMPILE_FLAGS)

if(MPI_LINK_FLAGS)
    set_target_properties(ssb-test PROPERTIES LINK_FLAGS "${MPI_LINK_FLAGS}")
endif(MPI_LINK_FLAGS)

# installation ###########################################

install(TARGETS ssb-test DESTINATION bin)
