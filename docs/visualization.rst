.. _visualize:

Visualization of crystals and results
=====================================

Now that our simulation finished successfully, we can continue with visualizing the results.

Visualize the supercell with Ovito
----------------------------------

The STEMsalabim output files (somewhat) comply with the `AMBER specifications <http://ambermd.org/netcdf/nctraj.xhtml>`_
to visualize the specimen structure. However, all the dimensions and variables of AMBER live within the NetCDF group
``/AMBER`` in the NC file. This means that STEMsalabim output files **will not be compatible to
visualization programs requiring the pure AMBER specs!**

However, the authors of the excellent cross-platform `Ovito <https://www.ovito.org/>`_ software, which is a
visualization program for atomic structures (and much more), have added support for the ``/AMBER`` sub-group, so that
STEMsalabim NetCDF result files can be visualized seamlessly in Ovito.

What you will see is the atomic structure of the input specimen. In addition to the positional coordinates of each atom,
you find the mean square displacement (``msd``), the slice ID and coordinate (``slice`` and ``slice_coordinates``), the equilibrium coordinates
(``lattice_coordinates``), elements (``elements``) and atomic radii (``radii``) as variables. Each frozen lattice
configuration is one ``frame`` in the AMBER specs, so you can see the atoms wiggling around if you use the frame slider
of Ovito.

Generate an ADF STEM image
--------------------------

In the :code:`examples/Si_001` folder you will find the two files ``make_haadf.m`` and ``make_haadf.py``.
Both extract an HAADF image from the result NetCDF file. Please have a look at the code to get an idea of how
to work with the NetCDF result files.

* `MATLAB® <https://de.mathworks.com/products/matlab.html>`_ uses HDF5 for its ``.mat`` format for a couple of versions
  now, and is therefore perfectly capable of reading STEMsalabim result files. For quick analysis and image generation
  is a great tool.
* Python with the `NetCDF4 <http://unidata.github.io/netcdf4-python/>`_ module is also a great tool to
  analyze and visualize STEMsalabim result files, especially combined with numerical libraries such
  as `numpy <http://www.numpy.org/>`_ or `pandas <https://pandas.pydata.org/>`_.

What now?
---------

You have now completed your first simulation and looked at some of its results. In order to use STEMsalabim
for your research you should dig deeper into this documentation by reading the documentation links on the left.
