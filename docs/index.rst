STEMsalabim
===========

The STEMsalabim software aims to provide accurate scanning transmission electron microscopy (STEM) image simulation of
a specimen whose atomic structure is known. It implements the frozen lattice multi-slice algorithm as described in
great detail in the book `Advanced computing in electron microscopy <http://dx.doi.org/10.1007/978-1-4419-6533-2>`_ by
Earl J. Kirkland.

While there are multiple existing implementations of the same technique, at the time of this writing none of them is
suitable for leveraging massive parallelization available on high-performance computing (HPC) clusters, making it
possible to simulate large supercells and parameter sweeps in reasonable time.

The purpose of STEMsalabim is to fill this gap by providing a multi-slice implementation that is well parallelizable
both within and across computing nodes, using a mixture of threaded parallelization and message passing interface (MPI).


.. toctree::
    :maxdepth: 2
    :caption: Getting Started

    what
    install
    usage
    visualization

bla

.. toctree::
    :maxdepth: 2
    :caption: More information

    general
    parameters
    file_formats
    faq
    whats_new
    citing
    articles


Contact us!
===========

STEMsalabim is a relatively young software package and was not heavily tested outside the scope of our group.
We are glad to help you getting your simulations to run.

Please contact **strl-stemsalabim [at] lists.uni-marburg.de** for support or feedback.

Credits
=======

  * We acknowledge the creators of the supplementary libraries that STEMsalabim depends on.
  * We would also like to acknowledge the creators of `STEMsim <http://dx.doi.org/10.1007/978-1-4020-8615-1_36>`_,
    which we used as a reference implementation to test STEMsalabim.
  * Once again, we would like to highlight the book
    `Advanced computing in electron microscopy <http://dx.doi.org/10.1007/978-1-4419-6533-2>`_ by Earl J. Kirkland for
    its detailed description of the implementation of multi-slice algorithms.
  * STEMsalabim was written in the `Structure & Technology Research Laboratory <https://www.uni-marburg.de/wzmw/strl>`_
    of the `Philipps-Universität Marburg <https://www.uni-marburg.de/>`_ with financial support by
    the `German Research Foundation <http://www.dfg.de/en/>`_

+--------------------------------+----------------------------------------------+
| Philipps-Universität Marburg   | Structure & Technology Research Laboratory   |
+================================+==============================================+
| |image0|                       | |image1|                                     |
+--------------------------------+----------------------------------------------+

.. |image0| image:: images/umr.png
.. |image1| image:: images/strl.jpg
