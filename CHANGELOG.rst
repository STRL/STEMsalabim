What's new
==========


STEMsalabim 5.4.0
-----------------

June 17th, 2022

- Added angular-dependent calculation of center of mass (COM) of the diffraction pattern.
  The COM now is not only calculated for the whole diffraction pattern, but also for each ring element
  specified by the ADF grid. This is done without normalizing by the intensity as this is meant to be done in
  post-processing with the stored ADF intensities.
  The x and y direction of the COM are now stored in the new variables com_x and com_y in the NetCDF file.

- Plasmon scattering:
    - Make it possible to calculate only every n-th slice.
    - Create own group plasmon_scattering in NetCDF file. Keep the plasmon dimensions with size 1 in the usual ADF
      group to ensure backwards compatibility.
    - Add writing of the plasmon slice coordinates to the nc file.
    - New paramter in cfg file: Instead of setting the energy loss always equal to the plasmon energy, set it as a
      single parameter in the config file.
- Added switch in config file for saving the probe wave function at each scanpoint.
- Fixed bugs concerning:
    - Output of ``ssb-mkin`` with ``--stored-potentials``
    - Dynamic calculation of work_package_size. Also add termination condition for the corresponding for loop.
- Add beam tilt in x and y direction. Include a group for it in the cfg file.
- Implemented storing of potentials in an external file, i.e. when running a simulation with ssb-mkin and ssb-run it is
  now optionally possible to store the potentials not in the usual NetCDF output file, but in an additional NetCDF
  file. This is done because the potentials can take up very much storage space compared to the other data.
  Also added output which tells the user if the simulation was done with stored potentials and if so, in which file the
  potentials were stored.
- New high-level tests for
    - ADF for potentials in external file
    - Calculation of COM over the whole diffraction pattern, which is stored in the NetCDF file in ADF.center_of_mass
    - Plasmons stored in their own group in the NetCDF file
    - Slice coulomb potentials stored in extra file


STEMsalabim 5.3.0
-----------------

August 24th, 2021

- Added high level tests for ADF. They are left optional with a switch in source code, as they always depend on the paths to nc files. The idea is to generate nc files with the code version to test and to assert they contain the correct results.
- The package size is now checked to fit the number of MPI processes and threads. The goal is to prevent the case that some MPI processes and/or CPU cores stay idle. This has shown to be the case sometimes for few scan points. Package size will be decreased if necessary. Also a warning for the case of nmb CPU cores > nmb scan points is added.
- The switch plasmon_scattering.calculate_plasmons_in_all_slices has been added to config file. If set to true, the single plasmon scattering will only be calculated for the last slice to save time. In the current version, memory space in the netcdf file will still be allocated for all slices and left unchanged for all other slices but the last one. This still has to be improved.
- Added the parameter calculate_plasmons_in_all_slices to make the choice possible, if the plasmon loss will be calculated for all slices or only the last one.
- Updated parameters in documentation, i.e. add new ones and remove deprecated ones.
- Fixed bug concerning defocus weights.
    - Implemented the calculation of defocus weights as it is described in [Beyer, A., et al., Journal of microscopy 262.2 (2016): 171-177., https://onlinelibrary.wiley.com/doi/10.1111/jmi.12284].
    - Changed parameter probe.fwhm_defoci to probe.delta_defocus_max and added parameter probe.fwhm_defocus_distribution. e.g. the ensemble (probe.defocus = 0, probe.delta_defocus_max = 12, probe.num_defoci = 7) would give the defoci [-12, -8, -4, 0, 4, 8, 12]. The parameter probe.fwhm_defoci still can be used instead to keep backwards compatibility and not constitute an API break.
    - Added tests: both a unit test as well as a high level test reading the values from nc file.

STEMsalabim 5.2.0
-----------------

June 24, 2021

- Changes on plasmon part:
    - Corrected normalization of wave functions so that the sum of intensities is equal to the zero loss intensity (i.e. without accounting for plasmons).
    - Fixed bug which made plasmon calculations with multiple MPI processes impossible.



STEMsalabim 5.1.1
-----------------

December 18th, 2020

- Bugfix: now custom slicing is possible again by giving the slice of each atom in an extra column.
- Changes on plasmon part:
    - Now only one plasmon energy is used, i.e. no more integration and no more energy resolution.
    - The plasmon scattering cross section is now moved to multiple positions in real space.


STEMsalabim 5.1.0
-----------------

March 27th, 2020

- Added switch simulation.chunking_enabled to make chunking in netcdf file optional. In former version for big data amounts, especially when storing CBEDs, chunking sizes sometimes exceed the maximum possible value, leading to errors. This can be prevented by switching off chunking completely.
- Changed scattering back to as it was done in STEMsalabim 4.
- Added function for saving wave functions. It has to be used in source code, there is no option to use it as a user.
- Improvements on plasmon part.



STEMsalabim 5.0.0
-----------------

February 28th, 2019

**IMPORTANT**

The parameters `application.verbose` and `simulation.skip_simulation` are deprecated now.
The groups `adf/adf_intensities`, `cbed/cbed_intensities`, and `adf/center_of_mass` now have
a dimension for energy loss. It is usually `1` unless plasmon scattering feature is used.

Highlights
^^^^^^^^^^

- Speed improvements by increasing the grid sizes to match efficient FFT sizes. Note, that this may result
  in a higher simulation grid density than specified in `grating.density` parameter!
- Alternative parallelization scheme, see :ref:`parallelization-scheme`. When appropriate, different MPI procs
  now calculate different frozen phonon configurations / defoci in parallel. This reduces the required amount
  of communication between the processors.
- Automatic calculation of `center of mass` of the CBEDs for all ADF points. The COMs are calculated when
  `adf.enabled = true` and stored in the NC file next to `adf/adf_intensities` in `adf/center_of_mass`. Unit is mrad.
- New executables `ssb-mkin` and `ssb-run`. The former prepares an **input** NC file from which the latter can run
  the simulation. This has multiple advantages. See :ref:`simulation-structure` for more information.
- Single plasmon scattering.


Other changes
^^^^^^^^^^^^^

- Removed `application.verbose` parameter.
- Removed `simulation.skip_simulation`.
- Ability to disable thermal displacements via `frozen_phonon.enable = false` parameter.
- Fixed a serious bug with the integrated defocus averaging.
- Input XYZ files can now contain more than one space or TAB character for column separation.
- Removed Doxygen documentation and doc string comments.
- Default FFTW planning is now `FFTW_MEASURE`. This improves startup times of the simulation slightly.
- Changed the chunking of the `adf/adf_intensities` and `cbed/cbed_intensities` variables for faster write speed.
- Added `AMBER/slice_coordinates` variable to the output file, that contains the `z` coordinate of the upper boundary
  of each slice in nm.
- Removed HTTP reporting and CURL dependency.
- Significant code refactoring and some minor bugs fixed.
- Improved documentation.


STEMsalabim 4.0.1, 4.0.2
------------------------

March 23rd, 2018
March 21st, 2018

- Bug fixes
- Changed chunking of the ADF variable


STEMsalabim 4.0
---------------

March 9th, 2018

**IMPORTANT**

I'm releasing this version as 4.0.0, but neither the input nor output files changed. The parameter `precision` has
become deprecated and there is a parameter `tmp-dir`. Please see the documentation.

- Removed option for double precision. When requested, this may be re-introduced, but it slowed down compilation times
  and made the code significantly more complicated. The multislice algorithm with all its approximations, including the
  scattering factor parametrization, is not precise enough to make the difference between single and double precision
  significant.
- Improved the Wave class, so that some important parts can now be vectorized by the compiler.
- Introduced some more caches, so that performance could greatly be improved. STEMsalabim should now be about twice as
  fast as before.
- Results of the MPI processors are now written to temporary files and merged after each configuration is finished.
  This removes many MPI calls which tended to slow down the simulation. See the `--tmp-dir` parameter.
- Moved the `Element`, `Atom`, and `Scattering` classes to their own (isolated) library `libatomic`. This is easier to
  maintain.
- Simplified MPI communication by getting rid of serialization of C++ objects into char arrays. This is too error-prone
  anyway.
- Added compatibility with the Intel parallel studio (Compilers, MKL for FFTs, Intel MPI).
  Tested with Intel 17 only.
- Some minor fixes and improvements.


STEMsalabim 3.1.0, 3.1.1, 3.1.2, 3.1.3, 3.1.4
---------------------------------------------

February 23nd, 2018

- Added GPL-3 License
- Moved all the code to Gitlab
- Moved documentation to readthedocs.org
- Added Gitlab CI

STEMsalabim 3.0.1 and 3.0.2
---------------------------

February 22nd, 2018

- Fixed a few bugs
- Improved the CMake files for better build process

STEMsalabim 3.0.0
-----------------

January 3rd, 2018

- Reworked input/output file format.
- Reworked CBED storing. Blank areas due to bandwidth limiting are now removed.
- Changes to the configuration, mainly to defocus series.
- Compression can be switched on and off via config file now.
- Prepared the project for adding a Python API in the future.
- Added tapering to smoothen the atomic potential at the edges as explained in
  `I. Lobato, et al, Ultramicroscopy 168, 17 (2016) <https://www.sciencedirect.com/science/article/pii/S030439911630081X>`_.
- Added analysis scripts for Python and MATLAB to the Si 001 example.

STEMsalabim 2.0.0
-----------------

August 1st, 2017

- Changed Documentation generator to Sphinx
- Introduced a lot of memory management to prevent memory fragmentation bugs
- split STEMsalabim into a core library and binaries to ease creation of tools
- Added diagnostics output with --print-diagnostics
- Code cleanup and commenting


STEMsalabim 2.0.0-beta2
-----------------------

April 20th, 2017

-  Added possibility to also save CBEDs, i.e., the kx/ky resolved
   intensities in reciprocal space.
-  Improved documentation.
-  Switched to NetCDF C API. Dependency on NetCDF C++ is dropped.
-  Switched to distributed (parallel) writing of the NC files, which is
   required for the CBED feature. This requires NetCDF C and HDF5 to be
   compiled with MPI support.

STEMsalabim 2.0.0-beta
----------------------

March 27th, 2017

-  Lots of code refactoring and cleanup
-  Added Doxygen doc strings
-  Added Markdown documentation and ``make doc`` target to build this
   website.
-  Refined the output file structure
-  Added HTTP reporting feature
-  Added ``fixed_slicing`` option to fix each atom's slice througout the
   simulation
-  Got rid of the boost libraries to ease compilation and installation

STEMsalabim 1.0
---------------

November 18th, 2016

-  Initial release.
