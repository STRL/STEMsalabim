/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include <iostream>

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

#include "3rdparty/args/args.hxx"

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif

#include "3rdparty/sole/sole.hpp"

#include "utilities/Wave.hpp"
#include "classes/Simulation.hpp"
#include "utilities/initialize.hpp"
#include "classes/Params.hpp"


using namespace std;
using namespace stemsalabim;

void Params::initFromCLI(int argc, const char **argv) {
    using namespace args;

    ArgumentParser parser("STEMsalabim usage instructions.", "Please refer to the User documentation.");
    HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
    ValueFlag<string> params(parser, "params", "The parameter file path.", {'p', "params"});
    ValueFlag<unsigned int> num_threads(parser, "num-threads", "The number of threads per MPI proc.", {"num-threads"});
    ValueFlag<float> defocus(parser, "defocus", "The probe defocus.", {'d', "defocus"});
    ValueFlag<unsigned int> pkgsize(parser, "package-size", "Number of tasks in a MPI work package.", {"package-size"});
    ValueFlag<string> output_file(parser, "output-file", "The output file path.", {'o', "output-file"});
    ValueFlag<string> crystal_file(parser, "crystal-file", "The crystal file path.", {'c', "crystal-file"});
    ValueFlag<string> tmp_dir(parser, "tmp-dir", "The path to the temporary directory.", {"tmp-dir"});
    Flag altern_para(parser, "alternative-parallelization", "Parallelize over configurations", {"alternative-parallelization"});


    try {
        parser.ParseCLI(argc, argv);
    } catch(Help &) {
        cout << parser;
        exit(0);
    } catch(ParseError &e) {
        cerr << e.what() << endl;
        cerr << parser;
        exit(1);
    }

    if(argc > 1) {
        vector<string> all_args;
        all_args.assign(argv + 1, argv + argc);
        _command_line_arguments = algorithms::join(all_args, " ");
    }

    if(params) {
        _cli_options.push_back(params.Name());
        _param_file = get(params);
    } else {
        output::error("a parameter file MUST be specified!\n");
    }

    if(num_threads) {
        _cli_options.push_back(num_threads.Name());
        _num_threads = get(num_threads);
    }

    if(pkgsize) {
        _cli_options.push_back(pkgsize.Name());
        _work_package_size = get(pkgsize);
    }

    if(output_file) {
        _cli_options.push_back(output_file.Name());
        _output_file = get(output_file);
    }

    if(tmp_dir) {
        _cli_options.push_back(tmp_dir.Name());
        _tmp_dir = get(tmp_dir);
    }

    if(crystal_file) {
        _cli_options.push_back(crystal_file.Name());
        _crystal_file = get(crystal_file);
    }

    if(defocus) {
        _cli_options.push_back(defocus.Name());
        _probe_defocus = get(defocus);
    }

    if(altern_para) {
        _cli_options.push_back(altern_para.Name());
        _alternative_parallelization = get(altern_para);
    }

    _uuid = sole::uuid4().str();
}


int main(int argc, const char **argv) {
    auto &mpi_env = mpi::Environment::getInstance();
    Params &p = Params::getInstance();

    initialize_input_file(argc, argv);

    p.setParamsFileName(p.outputFilename());

    if(mpi_env.isMaster()) {
        p.readParamsFromNCFile(p.outputFilename());
        p.setCell(IO::initCrystalFromNCFile(p.outputFilename()));
    }

    if(mpi_env.isMpi()) {
        p.broadcast(mpi::Environment::MASTER);
    }
    p.cell()->initScattering();

    // create the simulation instance and pass the parameters instance
    // to it.
    Simulation s;
    s.init();
    s.run();
}