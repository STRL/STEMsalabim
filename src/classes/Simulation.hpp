/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_SIMULATION_HPP
#define STEMSALABIM_SIMULATION_HPP

#include <memory>


#include "FPConfManager.hpp"
#include "GridManager.hpp"
#include "Slice.hpp"
#include "Params.hpp"
#include "IO.hpp"
#include "../utilities/mpi.hpp"
#include "../utilities/TaskQueue.hpp"
#include "../utilities/output.hpp"
#include "SimulationState.hpp"

namespace stemsalabim {

    /*!
     * The main simulation class. Carries out a *STEMsalabim* simulation.
     */

    class Simulation {
    public:

        /*!
         * Initializes the simulation, reads parameter and sets up all the member pointers etc.
         */
        void init();

        /*!
         * This is the main simulation loop. It contains the main loops (frozen phonon, defocus, pixels)
         * and handles the distribution of tasks to MPI workers and threads.
         */
        void run();

        /*!
         * Print a line to stdout, with some additional information such as MPI rank, defocus and configuration etc.
         */
        static void printLine(const SimulationState &st, const std::string &line);

        /*!
         * Print a line to stdout, with some additional information such as MPI rank, defocus and configuration etc.
         * Line is printed only when this is the master MPI process.
         */
        static void printMaster(const SimulationState &st, const std::string &line);

        /*!
         * Print a line to stdout, with some additional information such as MPI rank, defocus.
         * Line is printed only when this is the master MPI process.
         */
        static void printMaster(const std::string &line);


    private:
        enum STATUS_SIGNAL {
            START_SIMULATION, START_DEFOCUS, START_CONFIGURATION, PROGRESS, FINISH_SIMULATION
        };

        /// The FPConfManager instance of the frozen phonon simulation.  Takes care of displacing atoms etc.
        std::shared_ptr<FPConfManager> _fpman;

        /// The GridManager that manages all required sweeps and grids.
        std::shared_ptr<GridManager> _gridman;

        /// all IO functions, i.e., NC results file management.
        std::shared_ptr<IO> _io;

        /// The propagator can be represented as two 1D functions, one for each
        /// spatial direction.
        Wave _propagator;

        /// these buffers temporarily hold intensity data as continuous storage. This is to prevent
        /// memory fragmentation errors.
        std::shared_ptr<memory::buffer::number_buffer<float>> _adf_intensity_buffer;
        std::shared_ptr<memory::buffer::number_buffer<float>> _adf_plasmon_intensity_buffer;
        std::shared_ptr<memory::buffer::number_buffer<float>> _com_x_buffer;
        std::shared_ptr<memory::buffer::number_buffer<float>> _com_x_plasmon_buffer;
        std::shared_ptr<memory::buffer::number_buffer<float>> _com_y_buffer;
        std::shared_ptr<memory::buffer::number_buffer<float>> _com_y_plasmon_buffer;
        std::shared_ptr<memory::buffer::number_buffer<float>> _com_buffer;
        std::shared_ptr<memory::buffer::number_buffer<float>> _com_plasmon_buffer;
        std::shared_ptr<memory::buffer::number_buffer<float>> _cbed_intensity_buffer;

        /*!
         * Carries out a multi-slice simulation for a single probe position
         * and a specific defocus value. Returns a vector with the intensities, that is aligned by slices * angles, i.e.,
         * having M angles and N slices, [0, M[ are the angular intensity of slice 0, [M, 2M[ the second slice etc.
         */
        std::chrono::seconds calculatePixel(ScanPoint &point, double defocus, unsigned int idefocus, unsigned int iconf);

        /*!
         * The part of the multi-slice simulation that is carried out by the MPI master, i.e., rank 0.
         * Most importantly, the master orchestrates the distribution of tasks to the MPI slaves. Work
         * packages are generated, sent to slaves, received, deserialized and stored in the corresponding
         * result structures. However, additionally the master also carries out some multi-slice simulations
         * when more than one thread is available.
         */
        void multisliceMaster(const SimulationState &st, bool do_mpi_queue);

        /*!
         * This is the part of the multi-slice simulations that is carried out by the MPI slaves,
         * who wait for pixel indices to process, calculate the results, serialize them and submit
         * them to the MPI master. Each slave has its own little taskq queue that is worked.
         */
        void multisliceWorker(const SimulationState &st);

        /*!
         * Stores the ScanPoint from work_packages with ids defined in pix_ids, for defocus idefocus and iconf
         * in the temporary output file.
         */
        void storeResultData(unsigned int idefocus, unsigned int iconf, const std::vector<unsigned int> &pix_ids,
                             std::vector<ScanPoint> &work_packages);

        /*!
         * Generate the propagator function for propagation of the wave between the slices.
         */
        void generatePropagator();

        /*!
         * propagate and bandwidth limit a wave one slice.
         */
        void propagate(Wave &wave, bool do_backward_fft);

        /*
         * Return the value of the aperture function at the indices (x,y). In the usual case, this will give 0 for
         * when the beam is blanked and 1 when the beam passes the aperture, with the aperture size depending on the
         * values given for min_apert and max_apert.
         * If a bitmap aperture file was provided by the user, the value from this file will be returned instead.
         */
        double apertureValue(Params &p, double alpha_sq, unsigned int x, unsigned int y) const;

        /*!
         * Given an (empty) wave, shape the wave to represent a focused STEM electron beam with
         * given defocus and x and y coordinate.
         */
        void makeProbe(Wave &wave, const double defocus, double x, double y) const;

        /*!
         * Method to send HTTP status report when HTTP reporting is configured.
         */
        void postStatus(STATUS_SIGNAL signal, const SimulationState &st, float progress);

        /*!
         * Method to send HTTP status report when HTTP reporting is configured.
         */
        void postStatus(STATUS_SIGNAL signal, const SimulationState &st) {
            postStatus(signal, st, 0);
        }

        /*!
         * Initializes result store buffers and the serialization buffer for the message passing.
         */
        void initBuffers();

        /*!
         * Store all finished results up to now.
         */
        std::chrono::microseconds
        storeDirtyResults(const SimulationState &st, std::vector<ScanPoint> &work_packages, TaskQueue &scan_work);

//        /*!
//         * Return numberOfPositions random pairs of indices in the grid.
//         */
//        std::vector<std::vector<unsigned int>> getPositionsInsideSpecimen(int numberOfPositions);

    };
}
#endif //STEMSALABIM_SIMULATION_HPP
