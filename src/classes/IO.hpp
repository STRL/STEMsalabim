/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_IO_H
#define STEMSALABIM_IO_H


#include <memory>
#include <vector>
#include <tuple>

#include <netcdf.h>
#include "Params.hpp"
#include "../utilities/memory.hpp"
#include "../utilities/algorithms.hpp"
#include "../utilities/netcdf_utils.hpp"
#include "../utilities/Wave.hpp"
#include "../libatomic/Cell.hpp"

namespace stemsalabim {


    class Crystal;


    class GridManager;


    class ScanPoint;


    class FPConfManager;

    /*!
     * Class for writing the output NetCDF file of STEMsalabim.
     */

    class IO {
    public:
        /*!
         * Initialize NetCDF output file. Here, most of the hierarchical structure is written, including
         * variables and dimension. Also, compression and chunking is set up and some information about the simulation
         * is written to the file.
         */
        void initNCFile(const std::shared_ptr<GridManager> &gridman, const std::shared_ptr<atomic::Cell> &crystal);

        /*!
         * Initialize the memory buffers that are used when averaging results.
         */
        void initBuffers(const std::shared_ptr<GridManager> &gridman);

        /*!
         * Appends the atomic positions of the current FP configuration to the AMBER group.
         */
        void writeCrystal(std::shared_ptr<const FPConfManager> fpman, std::shared_ptr<const atomic::Cell> crystal, std::shared_ptr<const GridManager> gridman);

        /*!
         * Writes the scattering potentials to the output file
         */
        void writePotentials(const std::shared_ptr<FPConfManager> &fpman, const std::vector<std::vector<std::vector<float>>> & d);

        /*!
         * Write all simulation parameters to the output file.
         */
        void writeParams(const std::shared_ptr<GridManager> &gridman, const std::string & conf_file_string = "");

        /*!
         * Append to the ADF intensities variable.
         */
        void writeAdfIntensities(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf);
        void writeAdfIntensities(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar & v);

        /*!
         * Append to the ADF plasmon intensities variable.
         */
        void writeAdfPlasmonIntensities(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf);
        void writeAdfPlasmonIntensities(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar & v);


        /*!
         * Append to the Center of mass y variable.
         */
        void writeCOMx(unsigned int idefocus, unsigned int iconf,
                                 const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                                 std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf);
        void writeCOMx(unsigned int idefocus, unsigned int iconf,
                                 const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                                 std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar & v);

        /*!
         * Append to the Center of mass x plasmon variable.
         */
        void writeCOMxPlasmon(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf);
        void writeCOMxPlasmon(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar & v);

        /*!
         * Append to the Center of mass y variable.
         */
        void writeCOMy(unsigned int idefocus, unsigned int iconf,
                       const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                       std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf);
        void writeCOMy(unsigned int idefocus, unsigned int iconf,
                       const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                       std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar & v);

        /*!
         * Append to the Center of mass y plasmon variable.
         */
        void writeCOMyPlasmon(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf);
        void writeCOMyPlasmon(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar & v);

        /*!
        * Append to the Center of mass variable.
        */
        void writeCOM(unsigned int idefocus, unsigned int iconf,
                      const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                      std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf);
        void writeCOM(unsigned int idefocus, unsigned int iconf,
                      const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                      std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar & v);

        /*!
        * Append to the Center of mass variable.
        */
        void writeCOMPlasmon(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf);
        void writeCOMPlasmon(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar & v);

        /*!
         * Append to the CBED intensities variable.
         */
        void writeCBEDIntensities(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf);
        void writeCBEDIntensities(unsigned int idefocus, unsigned int iconf,
                const std::shared_ptr<GridManager> &gridman, const ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf, NCVar & v);

        /*!
         * Generates the temporary result file and writes the header.
         */
        static void generateTemporaryResultFile();

        /*!
         * Removes the temporary result file.
         */
        static void removeTemporaryResultFile();

        /*!
         * Write temporary results to a binary file (custom format). This is done in MPI mode.
         */
        static void writeTemporaryResult(unsigned int idefocus, unsigned int iconf,
                ScanPoint  &point,
                std::shared_ptr<memory::buffer::number_buffer<float>> &com_x_buf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &com_x_plasmon_buf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &com_y_buf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &com_y_plasmon_buf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &combuf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &com_plasmon_buf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &adf_intensity_buf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &adf_plasmon_intensity_buf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf);

        /*!
         * Copy written temporary results from the binary files to the NC output file. (MPI mode)
         */
        void copyFromTemporaryFile(const std::shared_ptr<GridManager> &gridman,
                std::shared_ptr<memory::buffer::number_buffer<float>> &com_x_buf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &com_x_plasmon_buffer,
                std::shared_ptr<memory::buffer::number_buffer<float>> &com_y_buf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &com_y_plasmon_buffer,
                std::shared_ptr<memory::buffer::number_buffer<float>> &combuf,
                std::shared_ptr<memory::buffer::number_buffer<float>> &com_plasmon_buffer,
                std::shared_ptr<memory::buffer::number_buffer<float>> &adf_intensity_buffer,
                std::shared_ptr<memory::buffer::number_buffer<float>> &adf_plasmon_intensity_buffer,
                std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf);

        /*!
         * Write the various grids to the output file.
         */
        void writeGrids(const std::shared_ptr<GridManager> &gridman);

        /*!
         * Write a Complex2D wave function to a text file. This is a utility function for debugging purposes.
         */
        static void dumpWave(std::string filename, const Wave &wave);

        static std::shared_ptr<atomic::Cell> initCrystalFromNCFile(const std::string & filename);
        static std::shared_ptr<atomic::Cell> initCrystalFromXYZFile(const std::string & filename);

        std::vector<std::vector<std::tuple<double, double, double>>> readDisplacementsFromNCFile();

        std::vector<std::vector<float>> readStoredPotential(const std::shared_ptr<FPConfManager> &fpman, unsigned int slice_id);

        bool rewriteNCFile() const {
            Params &p = Params::getInstance();
            return algorithms::absoluteFilePath(p.outputFilename()) != algorithms::absoluteFilePath(p.paramsFileName());
        }

        void writeRuntime() const;

    private:
        typedef std::vector<size_t> vs;

        std::mutex _io_lock;

        bool _created{false};

        float _running_weight{0};

        std::vector<float> _com_x_buffer;
        std::vector<float> _com_x_plasmon_buffer;
        std::vector<float> _com_y_buffer;
        std::vector<float> _com_y_plasmon_buffer;
        std::vector<float> _com_buffer;
        std::vector<float> _com_plasmon_buffer;
        std::vector<float> _adf_intensities_buffer;
        std::vector<float> _adf_plasmon_intensities_buffer;
        std::vector<float> _cbed_intensities_buffer;

    };
}

#endif //STEMSALABIM_IO_H
