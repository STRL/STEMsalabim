/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "GridManager.hpp"
#include "Slice.hpp"

using namespace std;
using namespace stemsalabim;


void GridManager::generateGrids() {
    Params &p = Params::getInstance();

    double density = Params::getInstance().samplingDensity();
    if(p.isFixedGrid()) {
        _sampling_x = p.fixedGridX();
        _sampling_y = p.fixedGridY();
    } else {
        _sampling_x = (unsigned int)algorithms::getFFTEfficientSize((int)ceil(density * _crystal->sizeX()));
        _sampling_y = (unsigned int)algorithms::getFFTEfficientSize((int)ceil(density * _crystal->sizeY()));
    }

    generateKSpace();

    double dz = p.sliceThickness();
    double slice_top_offset = -dz / 8.0;

    // we start slightly above the sample and need to get a little hacky in order to correctly
    // slice everything. Now, atomic coordinates with precision as bad as two decimals should be
    // taken care of.
    unsigned int c = 0;
    for(double z = slice_top_offset; z < _crystal->sizeZ(); z += dz, c++) {
        _slices.push_back(std::make_shared<Slice>(z, dz, c));
    }

    // below, we generate all the points that are calculated, either in ADF or CBED.
    // First, we determine the max range of coordinates in _relative_ units composed
    // of the ADF/CBED scan range. Then, the ScanPoint<> objects are created.
    double sx1 = min(get<0>(p.adfScanX()), get<0>(p.cbedScanX()));
    double sx2 = max(get<1>(p.adfScanX()), get<1>(p.cbedScanX()));
    double sy1 = min(get<0>(p.adfScanY()), get<0>(p.cbedScanY()));
    double sy2 = max(get<1>(p.adfScanY()), get<1>(p.cbedScanY()));

    auto scan_points_x = (unsigned int) round(p.probeDensity() * (sx2 - sx1) * _crystal->sizeX());
    auto scan_points_y = (unsigned int) round(p.probeDensity() * (sy2 - sy1) * _crystal->sizeY());

    auto points_x = algorithms::linSpace(sx1, sx2, scan_points_x, false);
    auto points_y = algorithms::linSpace(sy1, sy2, scan_points_y, false);

    for(auto &x: points_x) {
        if(p.adf() && get<0>(p.adfScanX()) <= x && get<1>(p.adfScanX()) >= x)
            _adf_x_grid.push_back(x * _crystal->sizeX());

        if(p.cbed() && get<0>(p.cbedScanX()) <= x && get<1>(p.cbedScanX()) >= x)
            _cbed_x_grid.push_back(x * _crystal->sizeX());
    }

    for(auto &y: points_y) {
        if(p.adf() && get<0>(p.adfScanY()) <= y && get<1>(p.adfScanY()) >= y)
            _adf_y_grid.push_back(y * _crystal->sizeY());

        if(p.cbed() && get<0>(p.cbedScanY()) <= y && get<1>(p.cbedScanY()) >= y)
            _cbed_y_grid.push_back(y * _crystal->sizeY());
    }

    unsigned int adf_row = 0, adf_col = 0, cbed_row = 0, cbed_col = 0;
    for(auto &x: points_x) {
        adf_col = 0;
        cbed_col = 0;

        bool is_scan_adf_x = p.adf() && get<0>(p.adfScanX()) <= x && get<1>(p.adfScanX()) >= x;
        bool is_scan_cbed_x = p.cbed() && get<0>(p.cbedScanX()) <= x && get<1>(p.cbedScanX()) >= x;

        for(auto &y: points_y) {
            bool is_scan_adf_y = p.adf() && get<0>(p.adfScanY()) <= y && get<1>(p.adfScanY()) >= y;
            bool is_scan_cbed_y = p.cbed() && get<0>(p.cbedScanY()) <= y && get<1>(p.cbedScanY()) >= y;

            if((is_scan_adf_x && is_scan_adf_y) || (is_scan_cbed_x && is_scan_cbed_y)) {

                _scan_points.emplace_back(x * _crystal->sizeX(),
                                          y * _crystal->sizeY(),
                                          adf_row,
                                          adf_col,
                                          cbed_row,
                                          cbed_col,
                                          _scan_points.size(),
                                          is_scan_adf_x && is_scan_adf_y,
                                          is_scan_adf_x && is_scan_adf_y,
                                          is_scan_cbed_x && is_scan_cbed_y);
            }

            if(is_scan_adf_y)
                adf_col++;

            if(is_scan_cbed_y)
                cbed_col++;
        }

        if(is_scan_adf_x)
            adf_row++;

        if(is_scan_cbed_x)
            cbed_row++;
    }

    double angle_min, angle_max;
    unsigned int num_angles;
    tie(angle_min, angle_max, num_angles) = p.adfDetectorAngles();
    _detector_grid = algorithms::adaptiveSpace(angle_min, angle_max, num_angles, p.adfDetectorIntervalExponent(), true);

    unsigned int nslices = _slices.size();
    unsigned int every_nslices_adf = p.adfSaveEveryNSlices();
    unsigned int every_nslices_cbed = p.cbedSaveEveryNSlices();
    unsigned int every_nslices_plasmon = p.plasmonSaveEveryNSlices();

    // determine which z coordinates (thicknesses) of slices are stored in the output file
    _adf_store_slice.resize(nslices);
    _plasmon_store_slice.resize(nslices);
    _cbed_store_slice.resize(nslices);
    for(const shared_ptr<Slice> &slice: _slices) {
        unsigned int snum = slice->id() + 1;

        _adf_store_slice[slice->id()] = false;
        _cbed_store_slice[slice->id()] = false;

        _slice_coords.push_back(slice->z());

        if(snum == nslices || (p.adfSaveEveryNSlices() > 0 && snum % every_nslices_adf == 0)) {
            _adf_slice_coords.push_back(slice->z());
            _adf_store_slice[slice->id()] = true;
        }

        if(snum == nslices || (p.cbedSaveEveryNSlices() > 0 && snum % every_nslices_cbed == 0)) {
            _cbed_slice_coords.push_back(slice->z());
            _cbed_store_slice[slice->id()] = true;
        }

        if(snum == nslices || (p.plasmonSaveEveryNSlices() > 0 && snum % every_nslices_plasmon == 0)) {
            _plasmon_slice_coords.push_back(slice->z());
            _plasmon_store_slice[slice->id()] = true;
        }
    }

    // generate the indices for the ADF detector array bins.
    // here, we iterate the k space and calculate the wave angles.
    // Then, we determine the corresponding detector angle and sum the intensity up.
    _adf_bin_index.resize(samplingX());
    for(unsigned int ix = 0; ix < samplingX(); ix++) {
        _adf_bin_index[ix].resize(samplingY());
        for(unsigned int iy = 0; iy < samplingY(); iy++) {

            double k2 = sqrt(pow(kx(ix), 2) + pow(ky(iy), 2)) * p.wavelength();
            int index = algorithms::getIndexOfAdaptiveSpace(k2 * 1e3,
                                                            get<0>(p.adfDetectorAngles()),
                                                            get<1>(p.adfDetectorAngles()),
                                                            get<2>(p.adfDetectorAngles()),
                                                            p.adfDetectorIntervalExponent(),
                                                            true);
            if(index >= 0 && index < (long) _detector_grid.size())
                _adf_bin_index[ix][iy] = index;
            else
                _adf_bin_index[ix][iy] = -1;
        }
    }

    // defocus grid
    double delta_defocus_max = p.deltaDefocusMax();
    double defocus_mean = p.meanDefocus();
    unsigned int num_defoci = p.numberOfDefoci();
    float fwhm_defocus_distribution = p.FWHMDefocusDistribution();

    stemsalabim::algorithms::set_defoci_and_defocus_weights(
            delta_defocus_max,
            defocus_mean,
            num_defoci,
            fwhm_defocus_distribution,
            _defoci,
            _defocus_weights);

    if(p.bandwidthLimiting()) {
        _bandwidth_limit_mask.init(samplingX(), samplingY());
        _bandwidth_limit_mask.setIsKSpace(true);
        for(unsigned int ix = 0; ix < samplingX(); ix++) {
            for(unsigned int iy = 0; iy < samplingY(); iy++) {
                if(pow(kx(ix), 2) + pow(ky(iy), 2) > pow(1.0 / (3.0 * max(1. / scalingFactorNanometerToGridPixelX(), 1. /
                        scalingFactorNanometerToGridPixelY())), 2)) {
                    _bandwidth_limit_mask(ix, iy) = 0.0;
                } else {
                    _bandwidth_limit_mask(ix, iy) = 1.0;
                }
            }
        }
    }

    // Generate the position shifts for the plasmon scattering potential B(x)
    // and store them in _positionsToSwitchPlsmPotBy.
    if(p.plasmonsEnabled()){
        generatePositionSwitchesForBx(points_x.size(), points_y.size());
    }

    if(p.apertureBitmapGiven()){
        setApertureBitmap();
    }

}


void stemsalabim::GridManager::generateKSpace() {
    unsigned int lx = samplingX();
    unsigned int ly = samplingY();

    _kx_space.resize(lx);
    _ky_space.resize(ly);
    _x_space.resize(lx);
    _y_space.resize(ly);

    for(int i = 0; i < (int) lx; ++i) {
        _x_space[i] = (i * _crystal->sizeX()) / ((int) lx - 1);
        //_x_space[i] = (i * _size_x) / ((int) lx);

        if(i > ceil(lx / 2.0))
            _kx_space[i] = (i - (int) lx) / _crystal->sizeX();
        else
            _kx_space[i] = i / _crystal->sizeX();
    }

    for(int i = 0; i < (int) ly; ++i) {
        _y_space[i] = (i * _crystal->sizeY()) / ((int) ly - 1);
        //_y_space[i] = (i * _size_y) / ((int) ly);

        if(i > ceil(ly / 2.0))
            _ky_space[i] = (i - (int) ly) / _crystal->sizeY();
        else
            _ky_space[i] = i / _crystal->sizeY();
    }
}


/*
 * Generate the position shifts that the plasmon scattering potential B(x) is switched by.
 */
void GridManager::generatePositionSwitchesForBx(unsigned int NposX,unsigned int NposY) {
    Params &prms = Params::getInstance();

    unsigned int Ndefocus, Nphonon, Nslice, NposPerSlice;
    NposPerSlice = prms.numberOfPotentialPositionsPerSlice();
    Nslice = _slices.size();
    Ndefocus = prms.numberOfDefoci();
    Nphonon = prms.numberOfConfigurations();

    vector<vector<vector<vector<vector<vector<unsigned int>>>>>> tempDim6;
    vector<vector<vector<vector<vector<unsigned int>>>>> tempDim5;
    vector<vector<vector<vector<unsigned int>>>> tempDim4;
    vector<vector<vector<unsigned int>>> tempDim3;
    vector<vector<unsigned int>> tempDim2;
    vector<unsigned int> tempDim1;

    tempDim1.resize(2);
    tempDim2.resize(NposPerSlice);
    tempDim3.resize(Nslice);
    tempDim4.resize(Nphonon);
    tempDim5.resize(Ndefocus);
    tempDim6.resize(NposY);
    _positionsToSwitchPlsmPotBy.resize(NposX);

    // Define random number generator for integers in the range [0, maxIndex]
    unsigned int maxIndexX = samplingX() - 1;
    unsigned int maxIndexY = samplingY() - 1;
    auto randX = std::bind(std::uniform_int_distribution<unsigned int>(0,maxIndexX), mt19937(prms.randomSeed()));
    auto randY = std::bind(std::uniform_int_distribution<unsigned int>(0,maxIndexY), mt19937(prms.randomSeed() + 666));

    for (unsigned int ix = 0; ix < NposX; ix++) {
        for (unsigned int iy = 0; iy < NposY; iy++) {
            for (unsigned int idefocus = 0; idefocus < Ndefocus; idefocus++) {
                for (unsigned int iPhonon = 0; iPhonon < Nphonon; iPhonon++) {
                    for (unsigned int iSlice = 0; iSlice < Nslice; iSlice++) {
                        for (unsigned int i = 0; i < NposPerSlice; i++) {
                            // save position shift in smallest vector
                            tempDim1[0] = randX();
                            tempDim1[1] = randY();

                            tempDim2[i] = tempDim1;
                        }
                        tempDim3[iSlice] = tempDim2;
                    }
                    tempDim4[iPhonon] = tempDim3;
                }
                tempDim5[idefocus] = tempDim4;
            }
            tempDim6[iy] = tempDim5;
        }
        _positionsToSwitchPlsmPotBy[ix] = tempDim6;
    }
}

void GridManager::setApertureBitmap() {
    Params &prms = Params::getInstance();

    int size_grid_x = samplingX();
    int size_grid_y = samplingY();
    _aperture_bitmap.resize(size_grid_x, vector<double>(size_grid_y));

    fstream bitmap_file;
    bitmap_file.open(prms.apertureBitmapFilename(), ios::in);
    int ix = 0;
    int iy = 0;
    string line;
    while(getline(bitmap_file, line)){
        std::stringstream ss(line);
        string s;
        ix = 0;
        while (getline(ss, s, ' ')){
            _aperture_bitmap[ix][iy] = stod(s);
            ix++;
        }
        iy++;
    }
    bitmap_file.close();
}