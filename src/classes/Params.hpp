/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_PARAMS_HPP
#define STEMSALABIM_PARAMS_HPP

#include <utility>
#include <string>
#include <tuple>
#include <libconfig.h++>
#include <random>
#include <cmath>
#include <algorithm>

#include "../libatomic/Cell.hpp"
#include "../utilities/output.hpp"

namespace stemsalabim {

    /*!
     * A singleton class containing all simulation parameters, which are collected from command line
     * arguments and the input parameter file. In addition, it generates (if required) a random seed
     * as well as the simulation UUID.
     */
    class Params {
    public:
        /*!
         * Return the instance of the class, which is created if called for the first time.
         */
        static Params &getInstance() {
            static Params instance;
            return instance;
        }

        /*!
         * Initialize the parameters that are read from the command line, including most importantly
         * the location of the parameter file.
         */
        void initFromCLI(int argc, const char **argv);

        /*!
         * Make the Params class know the cell...
         */
        void setCell(const std::shared_ptr<atomic::Cell> &cell) {
            _cell = cell;
        }

        /*!
         * Access to the cell associated with params class.
         */
        std::shared_ptr<atomic::Cell> cell() const {
            return _cell;
        }

        /*!
         * Return the generated universally unique identifier (UUID) of this simulation.
         */
        const std::string &uuid() const {
            return _uuid;
        }

        /*!
         * Return the command line arguments string, i.e., the command line call
         * minus the executable name.
         */
        const std::string &cliArguments() const {
            return _command_line_arguments;
        }

        /*!
         * Return the file path to the parameters file.
         */
        const std::string &paramsFileName() const {
            return _param_file;
        };

        /*!
         * Set the param file path.
         */
        void setParamsFileName(const std::string & path) {
            _param_file = path;
        };

        /*!
         * Read the simulation parameters from a string with the contents of the parameters file.
         * The file is not read in by the Param class itself to ensure that MPI processes other than the
         * master don't do IO.
         */
        void readParamsFromString(const std::string &prms);

        /*!
         * Read the simulation parameters from another NC file.
         */
        void readParamsFromNCFile(const std::string & path);
        void readParamsFromNCFile();

        /*!
         * broadcast parameters to other MPI procs.
         */
        void broadcast(int rank);

        /*!
         * Return the path to the crystal XYZ file.
         */
        const std::string &crystalFilename() const {
            return _crystal_file;
        }

        /*!
         * Return path to the output file name.
         */
        const std::string &outputFilename() const {
            return _output_file;
        }

        /*!
        * Return path to the output file name for storing potentials.
        * If potentials shall be stored in the usual nc file, return _output_file instead.
        */
        const std::string &outputFilenamePotentials() const {
            if (_store_potentials_in_external_file)
                return _external_potential_file;
            else
                return _output_file;
        }

        /*!
        * Return whether to store potentials in an external file.
        * If store_potentials_in_external_file==false and storedPotentials==True, the potentials will be stored in the
        * usual nc file.
        */
        const bool store_potentials_in_external_file() const{
            return _store_potentials_in_external_file;
        }

        /*!
         * Return path to the temporary directory. If empty string, use the
         * dir of the output file.
         */
        const std::string &tmpDir() const {
            return _tmp_dir;
        }

        /*!
         * Returns if output should be compressed.
         */
        bool outputCompress() const {
            return _output_compress;
        }

        /*!
         * Returns if potentials should be stored and read from the init file.
         */
        bool storedPotentials() const {
            return _stored_potentials;
        }

        /*!
         * Returns if simulation was initialized from NC file.
         */
        bool initFromNCFile() const {
            return _init_from_ncfile;
        }

        /*!
         * Return the title of the simulation.
         */
        const std::string &title() const {
            return _title;
        }

        /*!
         * Return the Slice thickness of the multi-slice algorithm.
         */
        double sliceThickness() const {
            return _slice_thickness;
        }

        /*!
         * Return the cutoff radius of the atomic potentials.
         */
        double maxPotentialRadius() const {
            return _max_potential_radius;
        }

        /*!
         * Return the beam energy of the microscope.
         */
        double beamEnergy() const {
            return _beam_energy;
        }

        /*!
         * Get the density of probe scan sampling in points/nm.
         */
        double probeDensity() const {
            return _probe_scan_density;
        }

        /*!
         * Return the max. numeric aperture of the electron probe.
         */
        double probeMaxAperture() const {
            return _probe_max_aperture;
        }

        /*!
         * Return the min. numeric aperture of the electron probe.
         */
        double probeMinAperture() const {
            return _probe_min_aperture;
        }

        /*!
         * Return the fifth order spherical aberration coefficient of the microscope.
         */
        double probeC5() const {
            return _probe_c5_coeff;
        }

        /*!
         * Return the third order spherical aberration coefficient of the microscope.
         */
        double probeSphericalAberrationCoeff() const {
            return _probe_spherical_aberration_coeff;
        }

        /*!
        * Get full width at half maximum of the Gaussian which determines the defocus weigths for defocus series for
         * simulations of chromatic aberration. It comes from equation (2) in [Beyer, A., et al., Journal of microscopy 262.2 (2016): 171-177.,
         * https://onlinelibrary.wiley.com/doi/10.1111/jmi.12284].
         * The default value of 7.5 is "for the JEOL JEM-2200 FS microscope with a CC of 1.5 mm and a dE of 0.42 eV follows a fwhm_defocus_distribution of 7.5 nm at 200 kV acceleration voltage "
        */
        double FWHMDefocusDistribution() const {
            return _fwhm_defocus_distribution;
        }

        /*!
         * Get the maximum value which will be added/subtracted to/from the mean defocus value to determine the defoci to use for defocus series.
         */
        double deltaDefocusMax() const {
            return _delta_defocus_max;
        }

        /*!
         * Get the center part of the defocus information tuple.
         */
        double meanDefocus() const {
            return _probe_defocus;
        }

        /*!
         * Get the number part of the defocus information tuple.
         */
        unsigned int numberOfDefoci() const {
            return _probe_num_defoci;
        }

        /*!
         * Return two-fold astigmatism coefficient.
         */
        double probeAstigmatismCa() const {
            return _probe_astigmatism_ca;
        }

        /*!
         * Return two-fold astigmatism angle coefficient.
         */
        double probeAstigmatismAngle() const {
            return _probe_astigmatism_angle;
        }

        /*!
         * Get the number of frozen phonon configurations.
         */
        unsigned int numberOfConfigurations() const {
            return _number_configurations;
        }

        /*!
         * Return whether frozen phonon should be enabled
         */
        bool fpEnabled() const {
            return _fp_enabled;
        }


        /*!
         * Get the (generated) random seed of this simulation.
         */
        unsigned int randomSeed() const {
            return _random_seed;
        }

        /*!
         * Get the density of real and phase space grating sampling in points/nm.
         */
        double samplingDensity() const {
            return _sample_density;
        }

        /*!
         * Get the ranom number generator.
         */
        const std::mt19937 &getRandomGenerator() const {
            return _rgen;
        }

        /*!
         * Return whether slicing is fixed, i.e., the z coordinates of the Atom s are not
         * varied in FP vibrations.
         */
        bool isFixedSlicing() const {
            return _fixed_slicing;
        }

        /*!
         * Return whether wave functions are bandwidth limited.
         */
        bool bandwidthLimiting() const {
            return _bandwidth_limiting;
        }

        /*!
         * Return whether wave functions are renormalized after each slice.
         */
        bool normalizeAlways() const {
            return _normalize_always;
        }

        /*!
         * Return whether the chunk size of the netcdf file is defined depending on the number of atoms,
         * or if the default scheme of netcdf shall be used.
         */
        bool chunking_enabled() const {
            return _chunking_enabled;
        }

        /*!
        * Return whether the probe wavefunction for every scanpoint will be saved as a text file.
        */
        bool save_probe_wavefunction() const {
            return _save_probe_wavefunction;
        }

        /*!
         * Return the package of each thread of slave MPI processes,
         * i.e., how many pixels this thread calculates before reporting the results back to the master.
         */
        unsigned int workPackageSize() const {
            return _work_package_size;
        }

        /*!
         * Print all quantities which together determine whether the parallelization scheme works out.
         */
        void printParallelizationParameters(int total_number_of_threads, int number_adf_scanpoints) {
            printf("Quantities which are important for parallelization:\n");
            printf("\tNumber of threads per node: %d\n", numberOfThreads());
            printf("\tTotal number of threads, i.e. total number of CPU cores: %d\n", total_number_of_threads);
            printf("\tTotal number of ADF scan points: %d\n", number_adf_scanpoints);
            printf("\tWork package size: %d\n", _work_package_size);
            printf("\n\n");
        }

        /*!
        * Determine whether work_package_size together with the other parallelization parameters gives a valid ensemble.
         * Class member parameters are given as arguments to make testing easier (prevent mocking).
        */
        bool workPackageSizeValid(int work_package_size, int number_adf_scanpoints, int mpi_env_size,
                int number_of_threads) {
            if((work_package_size < number_of_threads) || work_package_size * mpi_env_size > number_adf_scanpoints) {
                return false;
            } else return true;
        }

        /*!
        * Decrease _work_package_size until it reaches a useful value. Return at least 1.
         * Some content is redundant to workPackageSizeValid(),
         * but this function is used recursive.
        */
        int getWorkPackageSize(unsigned int number_adf_scanpoints, unsigned int mpi_env_size) {
            _iteration_counter++;
            if (_iteration_counter >1000){
                std::cerr << "\n The iteration counter for finding a useful value for the work package size has exceeded 1000! \n";
                exit(1);
            }

            Params &p = Params::getInstance();

            // End point of recursion
            if(_work_package_size == 1) {
                return 1;
            }

            // Package size is too small. Some cores on each node will stay idle
            if(_work_package_size < p.numberOfThreads()) {
                _work_package_size++;
                return getWorkPackageSize(number_adf_scanpoints, mpi_env_size);
            }

            // Package size is too big, some nodes would not get enough work
            if(_work_package_size * mpi_env_size > number_adf_scanpoints) {
                _work_package_size--;
                return getWorkPackageSize(number_adf_scanpoints, mpi_env_size);
            }
            return _work_package_size;
        }

        /*!
         * In case work_package_size together with the other parallelization parameters gives not a valid ensemble,
         * reset work_package_size to a more useful value.
         */
        void setWorkPackageSize(unsigned int number_adf_scanpoints, unsigned int mpi_env_size) {
            _work_package_size = getWorkPackageSize(number_adf_scanpoints, mpi_env_size);
        }

        /*!
         * Return the wavelength of the electron beam in angstroms.
         */
        double wavelength() const {
            return PLANCK_CONSTANT_SPEED_LIGHT / sqrt(_beam_energy * (2 * ELECTRON_REST_MASS + _beam_energy));
        }

        /*!
         * Return the \f$\gamma\f$ factor of the electron beam.
         */
        double gamma() const {
            return (1 + _beam_energy / ELECTRON_REST_MASS);
        }

        /*!
         * Return the number of threads per MPI process.
         */
        unsigned int numberOfThreads() const {
            return _num_threads;
        }

        /*!
         * Returns true if any ADF is to be saved at all.
         */
        bool adf() const {
            return _adf_enabled;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the STEM probe in x direction.
         */
        const std::tuple<double, double> &adfScanX() const {
            return _adf_scan_x;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the STEM probe in y direction.
         */
        const std::tuple<double, double> &adfScanY() const {
            return _adf_scan_y;
        }

        /*!
         * Get the detector angles interval and number, as a tuple containing
         * min detector angle, max detector angle, number of points.
         */
        const std::tuple<double, double, unsigned int> &adfDetectorAngles() const {
            return _adf_detector_angles;
        }

        /*!
         * Get the number of detector angles to calculate intensities for.
         */
        unsigned int adfNumberOfDetectorAngles() const {
            return std::get<2>(_adf_detector_angles);
        }

        /*!
         * Return the periodicity of slices after which intensities are written to the output file.
         * 0 - Only after the bottom of the sample
         * 1 - after each slice
         * N - after every Nth slice
         */
        unsigned int adfSaveEveryNSlices() const {
            return _adf_save_every_n_slices;
        }

        /*!
         * Get the exponent of the detector interval sizes.
         * 1 - linear interval (default)
         * > 1 - adaptive interval (smaller angles -> finer sampling)
         */
        float adfDetectorIntervalExponent() const {
            return _adf_detector_interval_exponent;
        }

        /*!
         * Return whether FP configurations should be averaged in the output file.
         */
        bool adfAverageConfigurations() const {
            return _adf_average_configurations;
        }

        bool isAlternativeParallelization() const {
            return _alternative_parallelization;
        }

        /*!
         * When some fixed grid size is given, return true
         */
        bool isFixedGrid() const {
            return _nkx > 0 && _nky > 0;
        }

        unsigned int fixedGridX() const {
            return _nkx;
        }

        unsigned int fixedGridY() const {
            return _nky;
        }

        /*!
         * Return whether different defoci simulations are averaged in the output file.
         */
        bool adfAverageDefoci() const {
            return _adf_average_defoci;
        }

        /*!
         * Returns true if any CBED is to be saved at all.
         */
        bool cbed() const {
            return _cbed_enabled;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the CBEDs in x direction.
         */
        const std::tuple<double, double> &cbedScanX() const {
            return _cbed_scan_x;
        }

        /*!
         * Get the scan boundaries in relative units (i.e., between 0 and 1)
         * of the CBEDs in y direction.
         */
        const std::tuple<double, double> &cbedScanY() const {
            return _cbed_scan_y;
        }

        /*!
         * Get the desired size of the CBED images to store. Will return a tuple (0,0) if
         * not set as a parameter.
         */
        const std::tuple<unsigned int, unsigned int> &cbedSize() const {
            return _cbed_size;
        }

        /*!
         * Get the desired size of the CBED images to store in X direction. Will return 0 if
         * not set as a parameter.
         */
        unsigned int cbedSizeX() const {
            return std::get<0>(_cbed_size);
        }

        /*!
         * Get the desired size of the CBED images to store in Y direction. Will return 0 if
         * not set as a parameter.
         */
        unsigned int cbedSizeY() const {
            return std::get<1>(_cbed_size);
        }

        /*!
         * Returns true if the desired CBED size is not 0,0, in which case the CBED
         * is to be rescaled before storage.
         */
        bool cbedRescale() const {
            return cbedSizeX() > 0 && cbedSizeY() > 0;
        }

        /*!
         * Return whether CBED FP configurations should be averaged in the output file.
         */
        bool cbedAverageConfigurations() const {
            return _cbed_average_configurations;
        }

        /*!
         * Return whether different CBED defoci simulations are averaged in the output file.
         */
        bool cbedAverageDefoci() const {
            return _cbed_average_defoci;
        }

        /*!
         * Return the periodicity of slices after which CBEDs are written to the output file.
         * 0 - Only after the bottom of the sample
         * 1 - after each slice
         * N - after every Nth slice
         */
        unsigned int cbedSaveEveryNSlices() const {
            return _cbed_save_every_n_slices;
        }

        /*!
         * Return the maximum angle which will be stored in the CBEDs.
         * A max angle of 0 mrad leads to the full CBED being stored.
         */
        double cbedMaxAngle() const {
            return _cbed_max_angle_mrad;
        }

        /*!
         * Return true if plasmon scattering is enabled.
         */
        bool plasmonsEnabled() const {
            return _plasmons_enabled;
        }


        /*!
        * Return the periodicity of slices after which CBEDs are written to the output file.
        * 0 - Only after the bottom of the sample
        * 1 - after each slice
        * N - after every Nth slice
        */
        unsigned int plasmonSaveEveryNSlices() const {
            return _plasmon_save_every_n_slices;
        }

        /*!
         * Plasmon mean free path in nm
         */
        float plasmonMeanFreePath() const {
            return _plasmons_mean_free_path;
        }

        /*!
         * Plasmon energy in eV
         */
        float plasmonEnergy() const {
            return _plasmons_energy;
        }

        /*!
        * Eenrgy loss energy in eV
        */
        float energyLoss() const {
            return _energy_loss;
        }
        /*!
         * Plasmon energy full width half maximum in eV
         */
        float plasmonFWHM() const {
            return _plasmons_fwhm;
        }

        // If _plasmons_enabled == False, make here sure no extra dimensions are created for plasmons in adf and cbed intensities.
        unsigned int numberOfPotentialPositionsPerSlice() const {
            if (_plasmons_enabled) {
                return _number_of_potential_positions;
            } else {
                return 0;
            }
        }

        /*!
         * Plasmon energy full width half maximum in eV
         */
        float plasmonsScatteringfunctionCutoffNanometer() const {
            return _plasmons_scatteringfunction_cutoff;
        }

        /*!
        * Return true if specimen tilt is enabled.
        */
        bool specimenTiltEnabled() const {
            return _specimen_tilt_enabled;
        }

        /*!
         * Return the specimen tilt angle for the x direction.
         */
        float specimenTiltAngleXmrad() const {
            return _specimen_tilt_angle_x_mrad;
        }

        /*!
         * Return the specimen tilt angle for the y direction.
         */
        float specimenTiltAngleYmrad() const {
            return _specimen_tilt_angle_y_mrad;
        }

        /*
         * Return whether or not to adjust the slice thickness to account for a longer beam path from slice to slice
         * due to specimen tilting.
         */
        bool adjustSliceThicknessForSpecimenTilt() const{
            return _adjust_slice_thickness_for_specimen_tilt;
        }

        /*!
        * Return true if beam tilt is enabled.
        */
        bool beamTiltEnabled() const {
            return _beam_tilt_enabled;
        }

        /*!
        * Return the beam tilt angle for the x direction as defined in cfg file.
        */
        float beamTiltAngleXmrad() const {
            return _beam_tilt_angle_x_mrad;
        }

        /*!
        * Return the beam tilt angle for the y direction as defined in cfg file.
        */
        float beamTiltAngleYmrad() const {
            return _beam_tilt_angle_y_mrad;
        }

        /*
         * Return whether or not to adjust the slice thickness to account for a longer beam path from slice to slice
         * due to beam tilting.
         */
        bool adjustSliceThicknessForBeamTilt() const{
            return _adjust_slice_thickness_for_beam_tilt;
        }

        /*
        * Return whether or not to adjust the slice thickness to account for a longer beam path from slice to slice
        * due to beam or specimen tilting.
        */
        bool adjustSliceThicknessForTilt() const{
            return (_adjust_slice_thickness_for_beam_tilt or _adjust_slice_thickness_for_specimen_tilt);
        }

        /*
         * Return the total tilt angle from the x and y tilt angles from beam or specimen tilt.
         */
        float totalTiltAngleRad(){
            float total_tilt_angle_x_mrad = 0;
            float total_tilt_angle_y_mrad = 0;

            if (_beam_tilt_enabled){
                total_tilt_angle_x_mrad += _beam_tilt_angle_x_mrad;
                total_tilt_angle_y_mrad += _beam_tilt_angle_y_mrad;
            }

            if(_specimen_tilt_enabled){
                total_tilt_angle_x_mrad += _specimen_tilt_angle_x_mrad;
                total_tilt_angle_y_mrad += _specimen_tilt_angle_y_mrad;
            }

            float total_tilt_angle_magnitude_mrad = sqrt(total_tilt_angle_x_mrad * total_tilt_angle_x_mrad + total_tilt_angle_y_mrad * total_tilt_angle_y_mrad);
            return total_tilt_angle_magnitude_mrad / 1000.0;
        }

        /*
         * Return if a bitmap for the aperture values was given by the user.
         */
        bool apertureBitmapGiven(){
            return _aperture_bitmap_given;
        }

        /*
         * Return the the aperture bitmap file name given by the user.
         */
        std::string apertureBitmapFilename(){
            return _aperture_bitmap_filename;
        }

        /// electron rest mass in keV. \f$m_0  c^2\f$ in keV
        constexpr static double ELECTRON_REST_MASS = 510.99906;

        /// product of planck's constant and speed of light in keV * nm.
        constexpr static double PLANCK_CONSTANT_SPEED_LIGHT = 1.239841875;

        /// pi. Approximately.
        constexpr static double pi = 3.14159265358979323846;

        /// pi. Approximately.
        constexpr static double e = 1.602176462e-19;

        std::string _param_file{""};
    private:
        Params() = default;

        std::string _command_line_arguments{""};

        std::string _uuid{""};

        double _sample_density{360};
        bool _output_compress{false};

        bool _init_from_ncfile{false};

        double _probe_max_aperture{24.0};
        double _probe_min_aperture{0.0};
        double _probe_c5_coeff{5e6};
        double _probe_spherical_aberration_coeff{2e3};
        double _probe_defocus{0};
        unsigned int _probe_num_defoci{1};
        double _delta_defocus_max{6};
        double _fwhm_defocus_distribution{7.5};
        double _probe_astigmatism_angle{0};
        double _probe_astigmatism_ca{0};
        double _probe_scan_density{40}; // in A^-1
        bool _aperture_bitmap_given{false};
        std::string _aperture_bitmap_filename{""};

        double _beam_energy{200.0};

        double _max_potential_radius{0.3};

        double _slice_thickness{0.2715};

        unsigned int _number_configurations{1};
        bool _fp_enabled{true};
        bool _fixed_slicing{true};

        std::string _crystal_file{""};
        std::string _output_file{""};
        std::string _external_potential_file{"stored_potentials.nc"};
        std::string _tmp_dir{"."};
        std::string _title{""};

        bool _bandwidth_limiting{true};
        bool _normalize_always{false};
        bool _chunking_enabled{true};
        bool _save_probe_wavefunction{false};

        bool _stored_potentials{false};
        bool _store_potentials_in_external_file{false};

        unsigned int _random_seed{0};
        std::mt19937 _rgen{0};
        unsigned _work_package_size{4};
        unsigned int _iteration_counter{0};
        unsigned _num_threads{1};
        bool _alternative_parallelization{false};

        // Settings related to ADF mode
        bool _adf_enabled{true};
        std::tuple<double, double> _adf_scan_x{std::make_tuple(0.0, 1.0)};
        std::tuple<double, double> _adf_scan_y{std::make_tuple(0.0, 1.0)};
        unsigned int _adf_save_every_n_slices{1};
        std::tuple<double, double, unsigned int> _adf_detector_angles{std::make_tuple(0.0, 300.0, 301)};
        float _adf_detector_interval_exponent{1.0};
        bool _adf_average_configurations{true};
        bool _adf_average_defoci{true};

        // settings related to CBED mode
        bool _cbed_enabled{false};
        std::tuple<double, double> _cbed_scan_x{std::make_tuple(0.0, 1.0)};
        std::tuple<double, double> _cbed_scan_y{std::make_tuple(0.0, 1.0)};
        std::tuple<unsigned int, unsigned int> _cbed_size{std::make_tuple(0, 0)};
        unsigned int _cbed_save_every_n_slices{0};
        bool _cbed_average_configurations{true};
        bool _cbed_average_defoci{true};
        double _cbed_max_angle_mrad{0.0};

        // plasmon scattering
        bool _plasmons_enabled{false};
        float _plasmons_mean_free_path{110};
        float _plasmons_energy{16.6};
        float _energy_loss{16.6};
        float _plasmons_fwhm{3.7};
        float _plasmons_scatteringfunction_cutoff{3};
        unsigned int _number_of_potential_positions{0};
        unsigned int _plasmon_save_every_n_slices{0};

        // specimen tilt
        bool _specimen_tilt_enabled{false};
        float _specimen_tilt_angle_x_mrad{0};
        float _specimen_tilt_angle_y_mrad{0};
        bool _adjust_slice_thickness_for_specimen_tilt{false};

        // beam tilt
        bool _beam_tilt_enabled{false};
        float _beam_tilt_angle_x_mrad{0};
        float _beam_tilt_angle_y_mrad{0};
        bool _adjust_slice_thickness_for_beam_tilt{false};

        libconfig::Config _cfg;

        std::vector<std::string> _cli_options;

        bool cliParamGiven(const std::string &p) {
            return std::find(_cli_options.begin(), _cli_options.end(), p) != _cli_options.end();
        }

        std::shared_ptr<atomic::Cell> _cell;

        unsigned int _nkx{0};
        unsigned int _nky{0};

    public:
        Params(Params const &) = delete;

        void operator=(Params const &) = delete;
    };

}

#endif //STEMSALABIM_PARAMS_HPP