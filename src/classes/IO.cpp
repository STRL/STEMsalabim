/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "IO.hpp"

#include <sys/stat.h>
#include <malloc.h>
#include <regex>

#include "config.h"

#include "Simulation.hpp"
#include "../libatomic/Scattering.hpp"

using namespace std;
using namespace stemsalabim;

void IO::initNCFile(const shared_ptr<GridManager> &gridman, const shared_ptr<atomic::Cell> &crystal) {
    Params &p = Params::getInstance();
    auto &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isSlave())
        output::error("This function should be called from the MPI master exclusively!\n");

    //try {

    if(_created) {
        return;
    }

    NCFile f(p.outputFilename(), true);

    // start with AMBER stuff
    {
        auto g = f.defineGroup("AMBER");

        auto atomDim = g.defineDim("atom", crystal->numberOfAtoms());
        auto eleDim = g.defineDim("elements", crystal->numberOfElements());
        auto spatialDim = g.defineDim("spatial", 3);
        auto cellSpatialDim = g.defineDim("cell_spatial", 3);
        auto cellAngularDim = g.defineDim("cell_angular", 3);
        auto labelDim = g.defineDim("label", 6);
        auto frameDim = g.defineDim("frame", gridman->defoci().size() * p.numberOfConfigurations());
        auto slicesDim = g.defineDim("slices", gridman->slices().size());
        auto gridxDim = g.defineDim("grid_x", gridman->samplingX());
        auto gridyDim = g.defineDim("grid_y", gridman->samplingY());

        auto spatialVar = g.defineVar<char>("spatial", vd({spatialDim}));
        auto cellSpatialVar = g.defineVar<char>("cell_spatial", vd({cellSpatialDim}));
        auto cellAngularVar = g.defineVar<char>("cell_angular", vd({cellAngularDim, labelDim}));
        auto coordinatesVar = g.defineVar<float>("coordinates", vd({frameDim, atomDim, spatialDim}));
        auto lCoordinatesVar = g.defineVar<float>("lattice_coordinates", vd({frameDim, atomDim, spatialDim}));
        auto cellLengthsVar = g.defineVar<float>("cell_lengths", vd({frameDim, cellSpatialDim}));
        auto cellAnglesVar = g.defineVar<float>("cell_angles", vd({frameDim, cellAngularDim}));
        auto radiusVar = g.defineVar<float>("radius", vd({frameDim, atomDim}));
        auto MSDVar = g.defineVar<float>("msd", vd({frameDim, atomDim}));
        auto sliceVar = g.defineVar<int>("slice", vd({frameDim, atomDim}));
        auto sliceCoordsVar = g.defineVar<float>("slice_coordinates", vd({slicesDim}));
        auto elementVar = g.defineVar<short>("element", vd({frameDim, atomDim}));
        auto systemLengthsVar = g.defineVar<float>("system_lengths", vd({cellSpatialDim}));
        auto systemAnglesVar = g.defineVar<float>("system_angles", vd({cellSpatialDim}));
        auto atomTypesVar = g.defineVar<char>("atom_types", vd({eleDim, labelDim}));


        if(p.chunking_enabled()) {
            size_t chunk_size = crystal->numberOfAtoms();
            coordinatesVar.chunking(vs({1, chunk_size, 3}));
            lCoordinatesVar.chunking(vs({1, chunk_size, 3}));
            cellAnglesVar.chunking(vs({1, 3}));
            cellLengthsVar.chunking(vs({1, 3}));
            elementVar.chunking(vs({1, chunk_size}));
            radiusVar.chunking(vs({1, chunk_size}));
            MSDVar.chunking(vs({1, chunk_size}));
            sliceVar.chunking(vs({1, chunk_size}));
        }

        if(p.outputCompress()) {
            coordinatesVar.deflate(1);
            lCoordinatesVar.deflate(1);
            cellAnglesVar.deflate(1);
            cellLengthsVar.deflate(1);
            elementVar.deflate(1);
            radiusVar.deflate(1);
            MSDVar.deflate(1);
            sliceVar.deflate(1);
        }
        // store potentials in external file
        if(p.storedPotentials() && p.store_potentials_in_external_file()) {
            NCFile f_potentials(p.outputFilenamePotentials(), p.store_potentials_in_external_file());
            auto g_potentials = f_potentials.defineGroup("AMBER");

            auto frameDim = g_potentials.defineDim("frame", gridman->defoci().size() * p.numberOfConfigurations());
            auto slicesDim = g_potentials.defineDim("slices", gridman->slices().size());
            auto gridxDim = g_potentials.defineDim("grid_x", gridman->samplingX());
            auto gridyDim = g_potentials.defineDim("grid_y", gridman->samplingY());

            g_potentials.defineVar<float>("slice_potentials", vd({frameDim, slicesDim, gridxDim, gridyDim}));
        }

        // store potentials in usual nc file
        if(p.storedPotentials() && (not p.store_potentials_in_external_file())) {
            g.defineVar<float>("slice_potentials", vd({frameDim, slicesDim, gridxDim, gridyDim}));
        }

        const char angles_labels[3][6] = {"alpha", "beta", "gamma"};
        vector<float> syslength = {(float) crystal->sizeX(), (float) crystal->sizeY(), (float) crystal->sizeZ()};
        spatialVar.put<char>("xyz");
        cellSpatialVar.put<char>("abc");
        cellAngularVar.put<char>(angles_labels);
        systemLengthsVar.put(syslength);
        systemAnglesVar.put(vector<float>(3, 0));

        // insert atom types
        unsigned long i = 0;
        for(const shared_ptr<atomic::Element> &el : crystal->elements()) {
            atomTypesVar.put(vs({i++, 0}), vs({1, el->symbol().size() + 1}), el->symbol().c_str());
        }

        coordinatesVar.att("unit", "nanometer");
        radiusVar.att("unit", "nanometer");
        cellLengthsVar.att("unit", "nanometer");
        cellAnglesVar.att("unit", "degree");

        // write some meta information to the netCDF file.
        g.att("Conventions", "AMBER");
        g.att("ConventionVersion", "1.0");
        g.att("program", PKG_NAME);
        g.att("programVersion", output::fmt("%s.%s.%s", PKG_VERSION_MAJOR, PKG_VERSION_MINOR, PKG_VERSION_PATCH));
        g.att("title", p.title());

    }

    // runtime stuff
    {
        auto runtimeGroup = f.defineGroup("runtime");

        // write attributes
        runtimeGroup.att("programVersionMajor", PKG_VERSION_MAJOR);
        runtimeGroup.att("programVersionMinor", PKG_VERSION_MINOR);
        runtimeGroup.att("programVersionPatch", PKG_VERSION_PATCH);
        runtimeGroup.att("gitCommit", GIT_COMMIT_HASH);
        runtimeGroup.att("title", p.title());
        runtimeGroup.att("UUID", p.uuid());
        runtimeGroup.att("time_start", output::currentTimeString());
    }

    // params group
    {
        auto paramsGroup = f.defineGroup("params");
        auto defocusDim = paramsGroup.defineDim("defocus", p.numberOfDefoci());

        paramsGroup.defineVar<float>("defocus", vd({defocusDim}));
        paramsGroup.defineVar<float>("defocus_weights", vd({defocusDim}));
    }

    if(p.adf()) {
        auto adf_points_x = (unsigned int) gridman->adfXGrid().size();
        auto adf_points_y = (unsigned int) gridman->adfYGrid().size();

        // define ADF group
        auto g = f.defineGroup("adf");

        // define ADF dimensions
        auto xDim = g.defineDim("adf_position_x", adf_points_x);
        auto yDim = g.defineDim("adf_position_y", adf_points_y);
        auto angleDim = g.defineDim("adf_detector_angle", p.adfNumberOfDetectorAngles());
        auto defocusDim = g.defineDim("adf_defocus", p.adfAverageDefoci() ? 1 : p.numberOfDefoci());
        auto fpDim = g.defineDim("adf_phonon", p.adfAverageConfigurations() ? 1 : p.numberOfConfigurations());
        auto sliceDim = g.defineDim("adf_slice", gridman->adfSliceCoords().size());
        auto coordinateDim = g.defineDim("coordinate_dim", 2);
        auto plasmonDim = g.defineDim("adf_plasmon_dimension", 1); // Empty dimension to keep backwards compatibility
        auto v = g.defineVar<float>("adf_intensities",vd({defocusDim, xDim, yDim, fpDim, sliceDim, plasmonDim, angleDim}));

        g.defineVar<float>("com_x",vd({defocusDim, xDim, yDim, fpDim, sliceDim, plasmonDim, angleDim}));
        g.defineVar<float>("com_y",vd({defocusDim, xDim, yDim, fpDim, sliceDim, plasmonDim, angleDim}));
        g.defineVar<float>("center_of_mass", vd({defocusDim, xDim, yDim, fpDim, sliceDim, plasmonDim, coordinateDim}));

        g.defineVar<double>("adf_probe_x_grid", vd({xDim}));
        g.defineVar<double>("adf_probe_y_grid", vd({yDim}));
        g.defineVar<double>("adf_detector_grid", vd({angleDim}));
        g.defineVar<double>("adf_slice_coords", vd({sliceDim}));

        if(p.outputCompress())
            v.deflate(1);

    }

    if(p.plasmonsEnabled()){
        auto adf_points_x = (unsigned int) gridman->adfXGrid().size();
        auto adf_points_y = (unsigned int) gridman->adfYGrid().size();

        auto plasmon_group = f.defineGroup("plasmon");

        auto xDim = plasmon_group.defineDim("adf_position_x", adf_points_x);
        auto yDim = plasmon_group.defineDim("adf_position_y", adf_points_y);
        auto angleDim = plasmon_group.defineDim("adf_detector_angle", p.adfNumberOfDetectorAngles());
        auto defocusDim = plasmon_group.defineDim("adf_defocus", p.adfAverageDefoci() ? 1 : p.numberOfDefoci());
        auto fpDim = plasmon_group.defineDim("adf_phonon", p.adfAverageConfigurations() ? 1 : p.numberOfConfigurations());
        auto sliceDim = plasmon_group.defineDim("adf_slice", gridman->plasmonSliceCoords().size());
        auto coordinateDim = plasmon_group.defineDim("coordinate_dim", 2);
        // if averaging over B-positions is done in ssb, this has to be taken into account here
        auto plasmonPotentialPositionsDim = plasmon_group.defineDim("plasmon_potential_positions",
                                                                    p.numberOfPotentialPositionsPerSlice());
        auto v = plasmon_group.defineVar<float>("adf_plasmon_intensities", vd({defocusDim, xDim, yDim, fpDim, sliceDim, plasmonPotentialPositionsDim, angleDim}));

        plasmon_group.defineVar<float>("com_x",vd({defocusDim, xDim, yDim, fpDim, sliceDim, plasmonPotentialPositionsDim, angleDim}));
        plasmon_group.defineVar<float>("com_y",vd({defocusDim, xDim, yDim, fpDim, sliceDim, plasmonPotentialPositionsDim, angleDim}));

        plasmon_group.defineVar<float>("center_of_mass", vd({defocusDim, xDim, yDim, fpDim, sliceDim, plasmonPotentialPositionsDim, coordinateDim}));

        plasmon_group.defineVar<double>("plasmon_slice_coords", vd({sliceDim}));

    }

    if(p.cbed()) {

        auto cbed_points_x = (unsigned int) gridman->cbedXGrid().size();
        auto cbed_points_y = (unsigned int) gridman->cbedYGrid().size();

        unsigned int k_sampling_x = gridman->storedCbedSizeX();
        unsigned int k_sampling_y = gridman->storedCbedSizeY();

        // define CBED group
        auto g = f.defineGroup("cbed");

        // define CBED dimensions
        auto xDim = g.defineDim("cbed_position_x", cbed_points_x);
        auto yDim = g.defineDim("cbed_position_y", cbed_points_y);
        auto kxDim = g.defineDim("cbed_k_x", k_sampling_x);
        auto kyDim = g.defineDim("cbed_k_y", k_sampling_y);
        auto defDim = g.defineDim("cbed_defocus", p.cbedAverageDefoci() ? 1 : p.numberOfDefoci());
        auto fpDim = g.defineDim("cbed_phonon", p.cbedAverageConfigurations() ? 1 : p.numberOfConfigurations());
        auto sliceDim = g.defineDim("cbed_slice", gridman->cbedSliceCoords().size());
        auto plasmonDim = g.defineDim("cbed_plasmon_dimension", 1 + p.numberOfPotentialPositionsPerSlice());

        auto v = g.defineVar<float>("cbed_intensities",
                                    vd({defDim, xDim, yDim, fpDim, sliceDim, plasmonDim, kxDim, kyDim}));
        g.defineVar<double>("cbed_probe_x_grid", vd({xDim}));
        g.defineVar<double>("cbed_probe_y_grid", vd({yDim}));
        g.defineVar<double>("cbed_x_grid", vd({kxDim}));
        g.defineVar<double>("cbed_y_grid", vd({kyDim}));
        g.defineVar<double>("cbed_slice_coords", vd({sliceDim}));

        if(p.outputCompress())
            v.deflate(1);
    }

    _created = true;

}

void IO::initBuffers(const shared_ptr<GridManager> &gridman) {
    Params &p = Params::getInstance();

    if(p.adf()) {
        _adf_intensities_buffer.resize(gridman->adfDetectorGrid().size() *
                                       gridman->adfSliceCoords().size() *
                                       1, 0);

        _com_x_buffer.resize(gridman->adfDetectorGrid().size() *
                                       gridman->adfSliceCoords().size(), 0);

        _com_y_buffer.resize(gridman->adfDetectorGrid().size() *
                             gridman->adfSliceCoords().size(), 0);

        _com_buffer.resize(2 * gridman->adfSliceCoords().size(), 0);

    }

    if(p.plasmonsEnabled()){
        _adf_plasmon_intensities_buffer.resize(gridman->adfDetectorGrid().size() *
                                       gridman->plasmonSliceCoords().size() * p.numberOfPotentialPositionsPerSlice(), 0);

        _com_x_plasmon_buffer.resize(gridman->adfDetectorGrid().size() *
                             gridman->plasmonSliceCoords().size() * p.numberOfPotentialPositionsPerSlice(), 0);

        _com_y_plasmon_buffer.resize(gridman->adfDetectorGrid().size() *
                                     gridman->plasmonSliceCoords().size() * p.numberOfPotentialPositionsPerSlice(), 0);

        _com_plasmon_buffer.resize(2 * gridman->plasmonSliceCoords().size() * p.numberOfPotentialPositionsPerSlice(), 0);
    }

    if(p.cbed()) {
        unsigned int k_sampling_x = gridman->storedCbedSizeX();
        unsigned int k_sampling_y = gridman->storedCbedSizeY();
        // resize the write buffer
        _cbed_intensities_buffer.resize(k_sampling_x *
                                        k_sampling_y *
                                        gridman->cbedSliceCoords().size() *
                                        (p.numberOfPotentialPositionsPerSlice() + 1), 0);
    }
}


void IO::writeParams(const shared_ptr<GridManager> &gridman, const string &conf_file_string) {
    Params &p = Params::getInstance();
    auto &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isSlave())
        output::error("This function should be called from the MPI master exclusively!\n");

    NCFile f(p.outputFilename());

    try {
        auto pg = f.group("params");

        {
            string prms_string;

            if(conf_file_string.empty()) {
                ifstream t(p.paramsFileName());
                prms_string = string((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
            } else {
                prms_string = conf_file_string;
            }

            pg.att("program_arguments", p.cliArguments());
            pg.att("config_file_contents", prms_string);
        }

        {
            auto g = pg.defineGroup("application");
            g.att("random_seed", p.randomSeed());
        }

        {
            auto g = pg.defineGroup("simulation");
            g.att("title", p.title());
            g.att("normalize_always", p.normalizeAlways());
            g.att("bandwidth_limiting", p.bandwidthLimiting());
            g.att("output_file", p.outputFilename());
            g.att("output_compress", p.outputCompress());
            g.att("chunking_enabled", p.chunking_enabled());
            g.att("save_probe_wavefunction", p.save_probe_wavefunction());
        }

        {
            auto g = pg.defineGroup("probe");
            g.att("c5", p.probeC5());
            g.att("cs", p.probeSphericalAberrationCoeff());
            g.att("astigmatism_ca", p.probeAstigmatismCa());
            g.att("defocus", p.meanDefocus());
            g.att("delta_defocus_max", p.deltaDefocusMax());
            g.att("fwhm_defocus_distribution", p.FWHMDefocusDistribution());
            g.att("num_defoci", p.numberOfDefoci());
            g.att("astigmatism_ca", p.probeAstigmatismCa());
            g.att("astigmatism_angle", p.probeAstigmatismAngle());
            g.att("min_apert", p.probeMinAperture());
            g.att("max_apert", p.probeMaxAperture());
            g.att("beam_energy", p.beamEnergy());
            g.att("scan_density", p.probeDensity());
            g.att("aperture_bitmap_filename", p.apertureBitmapFilename());
        }

        {
            auto g = pg.defineGroup("specimen");
            g.att("max_potential_radius", p.maxPotentialRadius());
            g.att("crystal_file", p.crystalFilename());
        }

        {
            auto g = pg.defineGroup("grating");
            g.att("density", p.samplingDensity());
            g.att("nx", gridman->samplingX());
            g.att("ny", gridman->samplingY());
            g.att("slice_thickness", p.sliceThickness());
        }

        {
            auto g = pg.defineGroup("adf");
            vector<double> adf_scan_x({get<0>(p.adfScanX()), get<1>(p.adfScanX())});
            vector<double> adf_scan_y({get<0>(p.adfScanY()), get<1>(p.adfScanY())});

            g.att("enabled", p.adf());
            g.att("x", adf_scan_x);
            g.att("y", adf_scan_y);
            g.att("detector_min_angle", get<0>(p.adfDetectorAngles()));
            g.att("detector_max_angle", get<1>(p.adfDetectorAngles()));
            g.att("detector_num_angles", get<2>(p.adfDetectorAngles()));
            g.att("detector_interval_exponent", p.adfDetectorIntervalExponent());
            g.att("average_configurations", p.adfAverageConfigurations());
            g.att("average_defoci", p.adfAverageDefoci());
            g.att("save_slices_every", p.adfSaveEveryNSlices());
        }

        {
            auto g = pg.defineGroup("cbed");
            vector<double> cbed_scan_x({get<0>(p.cbedScanX()), get<1>(p.cbedScanX())});
            vector<double> cbed_scan_y({get<0>(p.cbedScanY()), get<1>(p.cbedScanY())});
            vector<unsigned int> cbed_size({p.cbedSizeX(), p.cbedSizeY()});

            g.att("enabled", p.cbed());
            g.att("x", cbed_scan_x);
            g.att("y", cbed_scan_y);
            g.att("size", cbed_size);
            g.att("average_configurations", p.cbedAverageConfigurations());
            g.att("average_defoci", p.cbedAverageDefoci());
            g.att("save_slices_every", p.cbedSaveEveryNSlices());
            g.att("max_angle", p.cbedMaxAngle());
        }

        {
            auto g = pg.defineGroup("frozen_phonon");
            g.att("number_configurations", p.numberOfConfigurations());
            g.att("fixed_slicing", p.isFixedSlicing());
            g.att("enabled", p.fpEnabled());
        }

        {
            auto g = pg.defineGroup("plasmon_scattering");
            g.att("enabled", p.plasmonsEnabled());
            g.att("mean_free_path", p.plasmonMeanFreePath());
            g.att("plasmon_energy", p.plasmonEnergy());
            g.att("energy_loss", p.energyLoss());
            g.att("plasmon_fwhm", p.plasmonFWHM());
            g.att("plasmons_scatteringfunction_cutoff", p.plasmonsScatteringfunctionCutoffNanometer());
            g.att("number_of_potential_positions", p.numberOfPotentialPositionsPerSlice());
            g.att("save_slices_every", p.plasmonSaveEveryNSlices());
        }

        {
            auto g = pg.defineGroup("specimen_tilt");
            g.att("enabled", p.specimenTiltEnabled());
            g.att("specimen_tilt_angle_x_mrad", p.specimenTiltAngleXmrad());
            g.att("specimen_tilt_angle_y_mrad", p.specimenTiltAngleYmrad());
            g.att("adjust_slice_thickness", p.adjustSliceThicknessForSpecimenTilt());
        }

        {
            auto g = pg.defineGroup("beam_tilt");
            g.att("enabled", p.beamTiltEnabled());
            g.att("beam_tilt_angle_x_mrad", p.beamTiltAngleXmrad());
            g.att("beam_tilt_angle_y_mrad", p.beamTiltAngleYmrad());
            g.att("adjust_slice_thickness", p.adjustSliceThicknessForBeamTilt());
            int index_to_shift_by_x = int (gridman->pixelPerMradX() * p.beamTiltAngleXmrad());
            int index_to_shift_by_y = int (gridman->pixelPerMradY() * p.beamTiltAngleYmrad());
            g.att("index_to_shift_aperture_by_x", index_to_shift_by_x);
            g.att("index_to_shift_aperture_by_y", index_to_shift_by_y);
        }

    } catch(NcException &e) {
        output::error("%s\n", e.what());
    }
}

void IO::writePotentials(const shared_ptr<FPConfManager> &fpman, const vector<vector<vector<float>>> &d) {
    Params &p = Params::getInstance();
    auto &mpi_env = mpi::Environment::getInstance();
    NCFile f(p.outputFilenamePotentials());

    try {

        if(mpi_env.isSlave())
            output::error("This function should be called from the MPI master exclusively!\n");

        auto g = f.group("AMBER");
        auto v = g.var("slice_potentials");

        unsigned int iconf = fpman->currentConfiguration();

        for(unsigned long i = 0; i < d.size(); ++i) {
            for(unsigned long j = 0; j < d[i].size(); ++j) {
                v.put(vs({iconf, i, j, 0}), vs({1, 1, 1, d[i][j].size()}), d[i][j]);
            }
        }


    } catch(NcException &e) {
        output::error("%s\n", e.what());
    }
}

void IO::writeCrystal(shared_ptr<const FPConfManager> fpman, shared_ptr<const atomic::Cell> crystal,
        shared_ptr<const GridManager> gridman) {

    Params &p = Params::getInstance();
    NCFile f(p.outputFilename());
    auto &mpi_env = mpi::Environment::getInstance();

    try {

        if(mpi_env.isSlave())
            output::error("This function should be called from the MPI master exclusively!\n");

        auto g = f.group("AMBER");

        size_t n = crystal->numberOfAtoms();

        vector<float> cell_lengths(3);
        cell_lengths[0] = (float) crystal->sizeX();
        cell_lengths[1] = (float) crystal->sizeY();
        cell_lengths[2] = (float) crystal->sizeZ();

        vector<short> elements_data(n);
        vector<float> radii_data(n);
        vector<float> msd_data(n);
        vector<int> slice_data(n);
        vector<float> coords_data(n * 3);
        vector<float> lattice_coords_data(n * 3);

        unsigned int iconf = fpman->currentConfiguration();

        size_t i = 0;
        for(auto &atom: crystal->getAtoms()) {
            elements_data[i] = (short) distance(crystal->elements().begin(),
                                                crystal->elements().find(atom->getElement()));

            radii_data[i] = atom->getElement()->atomicRadius();
            msd_data[i] = (float) (atom->getMSD());

            if (atom->hasSlice()) {
                slice_data[i] = atom->getSlice();
            } else {
                double z = atom->getZ() + fpman->dz(i);
                slice_data[i] = gridman->sliceIndex(z);
            }

            coords_data[3 * i + 0] = (float) (atom->getX() + fpman->dx(i));
            coords_data[3 * i + 1] = (float) (atom->getY() + fpman->dy(i));
            coords_data[3 * i + 2] = (float) (atom->getZ() + fpman->dz(i));

            lattice_coords_data[3 * i + 0] = (float) atom->getX();
            lattice_coords_data[3 * i + 1] = (float) atom->getY();
            lattice_coords_data[3 * i + 2] = (float) atom->getZ();
            i++;
        }

        g.var("coordinates").put(vs({iconf, 0, 0}), vs({1, n, 3}), coords_data);
        g.var("lattice_coordinates").put(vs({iconf, 0, 0}), vs({1, n, 3}), lattice_coords_data);
        g.var("cell_lengths").put(vs({iconf, 0}), vs({1, 3}), cell_lengths);
        g.var("cell_angles").put(vs({iconf, 0}), vs({1, 3}), vector<float>({90.f, 90.f, 90.f}));
        g.var("element").put(vs({iconf, 0}), vs({1, n}), elements_data);
        g.var("radius").put(vs({iconf, 0}), vs({1, n}), radii_data);
        g.var("msd").put(vs({iconf, 0}), vs({1, n}), msd_data);
        g.var("slice").put(vs({iconf, 0}), vs({1, n}), slice_data);

    } catch(NcException &e) {
        output::error("%s\n", e.what());
    }
}

void
IO::writeCOM(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman, const ScanPoint &point,
             std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf) {
    unique_lock<mutex> qlck(_io_lock);

    Params &p = Params::getInstance();

    if(!point.adf || !point.hasCOM())
        return;

    NCFile f(p.outputFilename());
    auto g = f.group("adf");
    auto v = g.var("center_of_mass");

    try {
        IO::writeCOM(idefocus, iconf, gridman, point, ibuf, v);

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}

void
IO::writeCOM(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman, const ScanPoint &point,
             std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar &v) {

    Params &p = Params::getInstance();

    try {
        unsigned int n_slices_total = (unsigned int) gridman->adfSliceCoords().size();
        unsigned long lenght_plasmon_dimension = 1;

        unsigned int idefocus_store = p.adfAverageDefoci() ? 0 : idefocus;
        unsigned int iconf_store = p.adfAverageConfigurations() ? 0 : iconf;

        float w = 1.0;

        if(p.adfAverageDefoci())
            w *= gridman->defocusWeights()[idefocus];

        if(p.adfAverageConfigurations())
            w /= p.numberOfConfigurations();

        if(iconf > iconf_store || idefocus > idefocus_store) {

            v.get(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
                  vs({1, 1, 1, 1, n_slices_total, lenght_plasmon_dimension, 2}),
                  _com_buffer);

            for(size_t j = 0; j < _com_buffer.size(); ++j)
                _com_buffer[j] += w * ibuf->value(point.com, j);

        } else {

            for(size_t j = 0; j < _com_buffer.size(); ++j)
                _com_buffer[j] = w * ibuf->value(point.com, j);

        }

        v.put(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
              vs({1, 1, 1, 1, n_slices_total, lenght_plasmon_dimension, 2}),
              _com_buffer);

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}

void
IO::writeCOMPlasmon(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman, const ScanPoint &point,
        std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf) {
    unique_lock<mutex> qlck(_io_lock);

    Params &p = Params::getInstance();

    if(!point.adf || !point.hasCOMPlasmon())
        return;

    NCFile f(p.outputFilename());
    auto g = f.group("plasmon");
    auto v = g.var("center_of_mass");

    try {
        IO::writeCOMPlasmon(idefocus, iconf, gridman, point, ibuf, v);

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}

void
IO::writeCOMPlasmon(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman, const ScanPoint &point,
        std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar &v) {

    Params &p = Params::getInstance();

    try {
        unsigned int n_slices_total = (unsigned int) gridman->plasmonSliceCoords().size();

        unsigned int idefocus_store = p.adfAverageDefoci() ? 0 : idefocus;
        unsigned int iconf_store = p.adfAverageConfigurations() ? 0 : iconf;
        unsigned long nplasmons = p.numberOfPotentialPositionsPerSlice();

        float w = 1.0;

        if(p.adfAverageDefoci())
            w *= gridman->defocusWeights()[idefocus];

        if(p.adfAverageConfigurations())
            w /= p.numberOfConfigurations();

        if(iconf > iconf_store || idefocus > idefocus_store) {

            v.get(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
                  vs({1, 1, 1, 1, n_slices_total, nplasmons, 2}),
                  _com_plasmon_buffer);

            for(size_t j = 0; j < _com_plasmon_buffer.size(); ++j)
                _com_plasmon_buffer[j] += w * ibuf->value(point.com_plasmon, j);

        } else {

            for(size_t j = 0; j < _com_plasmon_buffer.size(); ++j)
                _com_plasmon_buffer[j] = w * ibuf->value(point.com_plasmon, j);

        }

        v.put(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
              vs({1, 1, 1, 1, n_slices_total, nplasmons, 2}),
              _com_plasmon_buffer);

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}

void IO::writeAdfIntensities(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf) {
    unique_lock<mutex> qlck(_io_lock);

    Params &p = Params::getInstance();

    if(!point.adf || !point.hasAdfIntensities())
        return;

    NCFile f(p.outputFilename());
    auto g = f.group("adf");
    auto v = g.var("adf_intensities");

    try {
        IO::writeAdfIntensities(idefocus, iconf, gridman, point, ibuf, v);

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}

void IO::writeAdfIntensities(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar &v) {

    Params &p = Params::getInstance();

    try {
        unsigned long nangles = gridman->adfDetectorGrid().size();
        unsigned int n_slices_total = (unsigned int) gridman->adfSliceCoords().size();
        unsigned long lenght_plasmon_dimension = 1;

        unsigned int idefocus_store = p.adfAverageDefoci() ? 0 : idefocus;
        unsigned int iconf_store = p.adfAverageConfigurations() ? 0 : iconf;

        float w = 1.0;

        if(p.adfAverageDefoci())
            w *= gridman->defocusWeights()[idefocus];

        if(p.adfAverageConfigurations())
            w /= p.numberOfConfigurations();

        if(iconf > iconf_store || idefocus > idefocus_store) {

            v.get(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
                  vs({1, 1, 1, 1, n_slices_total, lenght_plasmon_dimension, nangles}),
                  _adf_intensities_buffer);

            for(size_t j = 0; j < _adf_intensities_buffer.size(); ++j)
                _adf_intensities_buffer[j] += w * ibuf->value(point.adf_intensities, j);

        } else {
            for(size_t j = 0; j < _adf_intensities_buffer.size(); ++j)
                _adf_intensities_buffer[j] = w * ibuf->value(point.adf_intensities, j);
        }

        v.put(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
              vs({1, 1, 1, 1, n_slices_total, lenght_plasmon_dimension, nangles}),
              _adf_intensities_buffer);


    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}

void IO::writeAdfPlasmonIntensities(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf) {
    unique_lock<mutex> qlck(_io_lock);

    Params &p = Params::getInstance();

    if(!point.adf || !point.hasAdfIntensities())
        return;

    NCFile f(p.outputFilename());
    auto g = f.group("plasmon");
    auto v = g.var("adf_plasmon_intensities");

    try {
        IO::writeAdfPlasmonIntensities(idefocus, iconf, gridman, point, ibuf, v);

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}


void IO::writeAdfPlasmonIntensities(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar &v) {

    Params &p = Params::getInstance();

    try {
        unsigned long nangles = gridman->adfDetectorGrid().size();
        unsigned int n_slices_total = (unsigned int) gridman->plasmonSliceCoords().size();
        unsigned long nplasmons = p.numberOfPotentialPositionsPerSlice();

        unsigned int idefocus_store = p.adfAverageDefoci() ? 0 : idefocus;
        unsigned int iconf_store = p.adfAverageConfigurations() ? 0 : iconf;

        float w = 1.0;

        if(p.adfAverageDefoci())
            w *= gridman->defocusWeights()[idefocus];

        if(p.adfAverageConfigurations())
            w /= p.numberOfConfigurations();

        if(iconf > iconf_store || idefocus > idefocus_store) {

            v.get(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
                  vs({1, 1, 1, 1, n_slices_total, nplasmons, nangles}),
                  _adf_plasmon_intensities_buffer);

            for(size_t j = 0; j < _adf_plasmon_intensities_buffer.size(); ++j)
                _adf_plasmon_intensities_buffer[j] += w * ibuf->value(point.adf_plasmon_intensities, j);

        } else {
            for(size_t j = 0; j < _adf_plasmon_intensities_buffer.size(); ++j)
                _adf_plasmon_intensities_buffer[j] = w * ibuf->value(point.adf_plasmon_intensities, j);
        }

        v.put(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
              vs({1, 1, 1, 1, n_slices_total, nplasmons, nangles}),
              _adf_plasmon_intensities_buffer);


    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}


void IO::writeCBEDIntensities(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf) {
    unique_lock<mutex> qlck(_io_lock);

    Params &p = Params::getInstance();

    if(!point.cbed || !point.hasCbedIntensities())
        return;

    NCFile f(p.outputFilename());
    auto g = f.group("cbed");
    auto v = g.var("cbed_intensities");

    try {

        writeCBEDIntensities(idefocus, iconf, gridman, point, cbuf, v);


    } catch(NcException &e) {
        output::error("CBED %s\n", e.what());
    }
}

void IO::writeCBEDIntensities(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &cbuf, NCVar &v) {
    Params &p = Params::getInstance();

    unsigned long nx = gridman->storedCbedSizeX();
    unsigned long ny = gridman->storedCbedSizeY();
    auto n_slices_total = (unsigned int) gridman->cbedSliceCoords().size();

    unsigned int idefocus_store = p.cbedAverageDefoci() ? 0 : idefocus;
    unsigned int iconf_store = p.cbedAverageConfigurations() ? 0 : iconf;
    unsigned long nplasmons = (p.numberOfPotentialPositionsPerSlice() + 1);

    float w = 1.0;

    if(p.cbedAverageDefoci())
        w *= gridman->defocusWeights()[idefocus];

    if(p.cbedAverageConfigurations())
        w /= p.numberOfConfigurations();


    try {

        if(iconf > iconf_store || idefocus > idefocus_store) {

            v.get(vs({idefocus_store, point.cbed_row, point.cbed_col, iconf_store, 0, 0, 0, 0}),
                  vs({1, 1, 1, 1, n_slices_total, nplasmons, nx, ny}),
                  _cbed_intensities_buffer);

            for(size_t j = 0; j < _cbed_intensities_buffer.size(); ++j)
                _cbed_intensities_buffer[j] += w * cbuf->value(point.cbed_intensities, j);

        } else {

            for(size_t j = 0; j < _cbed_intensities_buffer.size(); ++j)
                _cbed_intensities_buffer[j] = w * cbuf->value(point.cbed_intensities, j);

        }

        v.put(vs({idefocus_store, point.cbed_row, point.cbed_col, iconf_store, 0, 0, 0, 0}),
              vs({1, 1, 1, 1, n_slices_total, nplasmons, nx, ny}),
              _cbed_intensities_buffer);

    } catch(NcException &e) {
        output::error("CBED %s\n", e.what());
    }
}

string getTempFileName() {
    Params &p = Params::getInstance();
    auto &mpi_env = mpi::Environment::getInstance();
    int mpi_rank = mpi_env.rank();

    return output::fmt("%s/%s_%d.bin", p.tmpDir(), p.uuid(), mpi_rank);
}

void IO::generateTemporaryResultFile() {
    auto &mpi_env = mpi::Environment::getInstance();
    int mpi_rank = mpi_env.rank();
    auto tmpfile = std::ofstream(getTempFileName(), std::ios::binary | std::ios::app);

    if(tmpfile.fail()) {
        output::error("Couldn't open temporary binary %s file for writing.\n", getTempFileName());
    }

    tmpfile.write(reinterpret_cast<const char *>(&mpi_rank), sizeof(mpi_rank));

    tmpfile.close();
}

void IO::removeTemporaryResultFile() {
    remove(getTempFileName().c_str());
}

void IO::writeTemporaryResult(unsigned int idefocus, unsigned int iconf, ScanPoint &point,
        shared_ptr<memory::buffer::number_buffer<float>> &com_x_buf,
        shared_ptr<memory::buffer::number_buffer<float>> &com_x_plasmon_buf,
        shared_ptr<memory::buffer::number_buffer<float>> &com_y_buf,
        shared_ptr<memory::buffer::number_buffer<float>> &com_y_plasmon_buf,
        shared_ptr<memory::buffer::number_buffer<float>> &combuf,
        shared_ptr<memory::buffer::number_buffer<float>> &com_plasmon_buf,
        shared_ptr<memory::buffer::number_buffer<float>> &adf_intensity_buf,
        shared_ptr<memory::buffer::number_buffer<float>> &adf_plasmon_intensity_buf,
        shared_ptr<memory::buffer::number_buffer<float>> &cbuf) {

    auto tmpfile = std::ofstream(getTempFileName(), std::ios::binary | std::ios::app);

    // the format is as follows:
    // int rank ( written in generateTemporaryResultFile)
    // for each pixel:
    //     unsigned int idefocus
    //     unsigned int iconf
    //     unsigned int index
    //     float[] adf_intensities
    //     float[] cbed_intensities

    if(tmpfile.fail()) {
        output::error("Couldn't open temporary binary %s file for writing.\n", getTempFileName());
    }

    if(tmpfile.tellp() == 0) {
        output::error("File %s shouldn't be empty! Has writeTemporaryResult() been called?\n", getTempFileName());
    }

    tmpfile.write(reinterpret_cast<const char *>(&idefocus), sizeof(idefocus));
    tmpfile.write(reinterpret_cast<const char *>(&iconf), sizeof(iconf));
    tmpfile.write(reinterpret_cast<const char *>(&point.index), sizeof(point.index));

    // write zero-loss ADF
    if(point.hasAdfIntensities()) {
        // write com_x
        tmpfile.write(reinterpret_cast<const char *>(com_x_buf->ptr(point.com_x)),
                      point.com_x.byte_size());
        point.clearCOM_x(com_x_buf);

        // write com_y
        tmpfile.write(reinterpret_cast<const char *>(com_y_buf->ptr(point.com_y)),
                      point.com_y.byte_size());
        point.clearCOM_y(com_y_buf);

        // write com
        tmpfile.write(reinterpret_cast<const char *>(combuf->ptr(point.com)), point.com.byte_size());
        point.clearCOM(combuf);

        // write ADF
        tmpfile.write(reinterpret_cast<const char *>(adf_intensity_buf->ptr(point.adf_intensities)),
                      point.adf_intensities.byte_size());
        point.clearAdfIntensities(adf_intensity_buf);
    }

    // write plasmon ADF
    if(point.hasAdfPlasmonIntensities()){
        // write com_x plasmon
        tmpfile.write(reinterpret_cast<const char *>(com_x_plasmon_buf->ptr(point.com_x_plasmon)),
                      point.com_x_plasmon.byte_size());
        point.clearCOM_x_plasmon(com_x_plasmon_buf);

        // write com_y plasmon
        tmpfile.write(reinterpret_cast<const char *>(com_y_plasmon_buf->ptr(point.com_y_plasmon)),
                      point.com_y_plasmon.byte_size());
        point.clearCOM_y_plasmon(com_y_plasmon_buf);

        tmpfile.write(reinterpret_cast<const char *>(com_plasmon_buf->ptr(point.com_plasmon)), point.com_plasmon.byte_size());
        point.clearCOMPlasmon(com_plasmon_buf);

        // write ADF plasmon
        tmpfile.write(reinterpret_cast<const char *>(adf_plasmon_intensity_buf->ptr(point.adf_plasmon_intensities)),
                      point.adf_plasmon_intensities.byte_size());
        point.clearAdfPlasmonIntensities(adf_plasmon_intensity_buf);
    }

    if(point.hasCbedIntensities()) {
        tmpfile.write(reinterpret_cast<const char *>(cbuf->ptr(point.cbed_intensities)),
                      point.cbed_intensities.byte_size());
        point.clearCBEDIntensities(cbuf);
    }

    tmpfile.close();

}

void IO::copyFromTemporaryFile(const shared_ptr<GridManager> &gridman,
        shared_ptr<memory::buffer::number_buffer<float>> &com_x_buf,
        shared_ptr<memory::buffer::number_buffer<float>> &com_x_plasmon_buffer,
        shared_ptr<memory::buffer::number_buffer<float>> &com_y_buf,
        shared_ptr<memory::buffer::number_buffer<float>> &com_y_plasmon_buffer,
        shared_ptr<memory::buffer::number_buffer<float>> &combuf,
        shared_ptr<memory::buffer::number_buffer<float>> &com_plasmon_buffer,
        shared_ptr<memory::buffer::number_buffer<float>> &adf_intensity_buffer,
        shared_ptr<memory::buffer::number_buffer<float>> &adf_plasmon_intensity_buffer,
        shared_ptr<memory::buffer::number_buffer<float>> &cbuf) {

    Params &prms = Params::getInstance();
    auto &mpi_env = mpi::Environment::getInstance();
    auto tmpfile = std::ifstream(getTempFileName(), std::ifstream::binary);

    tmpfile.seekg(0, std::ios::end);
    const std::streamoff length = tmpfile.tellg();
    tmpfile.seekg(0, std::ios::beg);

    // first, read rank (and check, if it is correct...)
    int mpi_rank_from_file;
    tmpfile.read(reinterpret_cast<char *>(&mpi_rank_from_file), sizeof(mpi_rank_from_file));

    if(mpi_rank_from_file != mpi_env.rank()) {
        output::error("Reading wrong temporary file!\n");
    }

    NCFile f(prms.outputFilename());

    NCVar adf_v, adf_plasmon_v, cv, com_x_v, com_x_plasmon_v, com_y_v, com_y_plasmon_v, comv, com_plasmon_v;

    try {
        if(prms.adf()) {
            adf_v = f.group("adf").var("adf_intensities");

            com_x_v = f.group("adf").var("com_x");
            com_y_v = f.group("adf").var("com_y");

            comv = f.group("adf").var("center_of_mass");
        }

        if(prms.plasmonsEnabled()){
            adf_plasmon_v = f.group(("plasmon")).var("adf_plasmon_intensities");

            com_x_plasmon_v = f.group(("plasmon")).var("com_x");
            com_y_plasmon_v = f.group(("plasmon")).var("com_y");

            com_plasmon_v = f.group(("plasmon")).var("center_of_mass");
        }

        if(prms.cbed()) {
            cv = f.group("cbed").var("cbed_intensities");
        }
    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }

    unsigned int idefocus;
    unsigned int iconf;
    unsigned int pix_idx;

    while(tmpfile.tellg() < length) {
        tmpfile.read(reinterpret_cast<char *>(&idefocus), sizeof(idefocus));
        tmpfile.read(reinterpret_cast<char *>(&iconf), sizeof(iconf));
        tmpfile.read(reinterpret_cast<char *>(&pix_idx), sizeof(pix_idx));

        ScanPoint p = gridman->scanPoints()[pix_idx];

        if(prms.adf() && p.adf) {
            auto &com_x_entry = com_x_buf->add_empty();
            tmpfile.read(reinterpret_cast<char *>(com_x_buf->ptr(com_x_entry)), com_x_entry.byte_size());
            p.storeCOMx(com_x_entry);
            writeCOMx(idefocus, iconf, gridman, p, com_x_buf, com_x_v);
            p.clearCOM_x(com_x_buf);

            auto &com_y_entry = com_y_buf->add_empty();
            tmpfile.read(reinterpret_cast<char *>(com_y_buf->ptr(com_y_entry)), com_y_entry.byte_size());
            p.storeCOMy(com_y_entry);
            writeCOMy(idefocus, iconf, gridman, p, com_y_buf, com_y_v);
            p.clearCOM_y(com_y_buf);

            auto &com_entry = combuf->add_empty();
            tmpfile.read(reinterpret_cast<char *>(combuf->ptr(com_entry)), com_entry.byte_size());
            p.storeCOM(com_entry);
            writeCOM(idefocus, iconf, gridman, p, combuf, comv);
            p.clearCOM(combuf);

            auto &entry = adf_intensity_buffer->add_empty();
            tmpfile.read(reinterpret_cast<char *>(adf_intensity_buffer->ptr(entry)), entry.byte_size());
            p.storeAdfIntensities(entry);
            writeAdfIntensities(idefocus, iconf, gridman, p, adf_intensity_buffer, adf_v);
            p.clearAdfIntensities(adf_intensity_buffer);
        }

        if(prms.plasmonsEnabled() && p.plasmon) {
            auto &com_x_plasmon_entry = com_x_plasmon_buffer->add_empty();
            tmpfile.read(reinterpret_cast<char *>(com_x_plasmon_buffer->ptr(com_x_plasmon_entry)), com_x_plasmon_entry.byte_size());
            p.storeCOMxPlasmon(com_x_plasmon_entry);
            writeCOMxPlasmon(idefocus, iconf, gridman, p, com_x_plasmon_buffer, com_x_plasmon_v);
            p.clearCOM_x_plasmon(com_x_plasmon_buffer);

            auto &com_y_plasmon_entry = com_y_plasmon_buffer->add_empty();
            tmpfile.read(reinterpret_cast<char *>(com_y_plasmon_buffer->ptr(com_y_plasmon_entry)), com_y_plasmon_entry.byte_size());
            p.storeCOMyPlasmon(com_y_plasmon_entry);
            writeCOMyPlasmon(idefocus, iconf, gridman, p, com_y_plasmon_buffer, com_y_plasmon_v);
            p.clearCOM_y_plasmon(com_y_plasmon_buffer);

            auto &com_plasmon_entry = com_plasmon_buffer->add_empty();
            tmpfile.read(reinterpret_cast<char *>(com_plasmon_buffer->ptr(com_plasmon_entry)), com_plasmon_entry.byte_size());
            p.storeCOMPlasmon(com_plasmon_entry);
            writeCOMPlasmon(idefocus, iconf, gridman, p, com_plasmon_buffer, com_plasmon_v);
            p.clearCOMPlasmon(com_plasmon_buffer);

            auto &adf_intensity_plasmon_entry = adf_plasmon_intensity_buffer->add_empty();
            tmpfile.read(reinterpret_cast<char *>(adf_plasmon_intensity_buffer->ptr(adf_intensity_plasmon_entry)), adf_intensity_plasmon_entry.byte_size());
            p.storeAdfPlasmonIntensities(adf_intensity_plasmon_entry);
            writeAdfPlasmonIntensities(idefocus, iconf, gridman, p, adf_plasmon_intensity_buffer, adf_plasmon_v);
            p.clearAdfPlasmonIntensities(adf_plasmon_intensity_buffer);

        }

            if(prms.cbed() && p.cbed) {
            auto &entry = cbuf->add_empty();
            tmpfile.read(reinterpret_cast<char *>(cbuf->ptr(entry)), entry.byte_size());
            p.storeCBEDIntensities(entry);
            writeCBEDIntensities(idefocus, iconf, gridman, p, cbuf, cv);
            p.clearCBEDIntensities(cbuf);
        }

    }

    tmpfile.close();

}


void IO::writeGrids(const shared_ptr<GridManager> &gridman) {
    Params &p = Params::getInstance();

    auto &mpi_env = mpi::Environment::getInstance();

    NCFile f(p.outputFilename());

    try {

        if(mpi_env.isSlave())
            output::error("This function should be called from the MPI master exclusively!\n");

        if(p.adf()) {
            auto g = f.group("adf");

            g.var("adf_probe_x_grid").put(vs({0}), vs({gridman->adfXGrid().size()}), gridman->adfXGrid());
            g.var("adf_probe_y_grid").put(vs({0}), vs({gridman->adfYGrid().size()}), gridman->adfYGrid());
            g.var("adf_detector_grid")
             .put(vs({0}), vs({gridman->adfDetectorGrid().size()}), gridman->adfDetectorGrid());
            g.var("adf_slice_coords").put(vs({0}), vs({gridman->adfSliceCoords().size()}), gridman->adfSliceCoords());
        }

        if(p.cbed()) {
            auto g = f.group("cbed");

            g.var("cbed_x_grid").put(vs({0}), vs({gridman->outputKXGrid().size()}), gridman->outputKXGrid());
            g.var("cbed_y_grid").put(vs({0}), vs({gridman->outputKYGrid().size()}), gridman->outputKYGrid());
            g.var("cbed_probe_x_grid").put(vs({0}), vs({gridman->cbedXGrid().size()}), gridman->cbedXGrid());
            g.var("cbed_probe_y_grid").put(vs({0}), vs({gridman->cbedYGrid().size()}), gridman->cbedYGrid());
            g.var("cbed_slice_coords")
             .put(vs({0}), vs({gridman->cbedSliceCoords().size()}), gridman->cbedSliceCoords());

        }

        if(p.plasmonsEnabled()){
            auto g = f.group("plasmon");
            g.var("plasmon_slice_coords").put(vs({0}), vs({gridman->plasmonSliceCoords().size()}), gridman->plasmonSliceCoords());
        }

        {
            auto g = f.group("params");
            g.var("defocus").put(vs({0}), vs({gridman->defoci().size()}), gridman->defoci());
            g.var("defocus_weights").put(vs({0}), vs({gridman->defocusWeights().size()}), gridman->defocusWeights());
        }

        {
            auto g = f.group("AMBER");
            g.var("slice_coordinates").put(vs({0}), vs({gridman->slices().size()}), gridman->sliceCoords());
        }

    } catch(NcException &e) {
        output::error("%s\n", e.what());
    }
}


void IO::writeRuntime() const {
    try {
        Params &p = Params::getInstance();
        auto &mpi_env = mpi::Environment::getInstance();
        NCFile f(p.outputFilename());

        if(!mpi_env.isMpi() || mpi_env.isMaster()) {
            f.group("runtime").att("time_stop", output::currentTimeString());
        }

    } catch(NcException &e) {
        output::error("%s\n", e.what());
    }
}


void IO::dumpWave(string filename, const Wave &wave) {
    FILE *f = fopen(filename.c_str(), "w");
    for(unsigned int ix = 0; ix < wave.lx(); ix++)
        for(unsigned int iy = 0; iy < wave.ly(); iy++)
            fprintf(f,
                    "%d %d %f %f %f\n",
                    ix,
                    iy,
                    wave(ix, iy).real(),
                    wave(ix, iy).imag(),
                    sqrt(pow(wave(ix, iy).real(), 2) + pow(wave(ix, iy).imag(), 2)));
    fclose(f);
}

bool exists(const std::string& name) {
    struct stat buffer;
    return (stat (name.c_str(), &buffer) == 0);
}

std::shared_ptr<stemsalabim::atomic::Cell> IO::initCrystalFromXYZFile(const std::string &filename) {

    if(!exists(filename))
        output::error("Crystal file %s can't be found!\n", filename);

    atomic::ElementProvider elp = atomic::ElementProvider::getInstance();

    // some useful variable declarations
    ifstream iss(filename);
    string line;
    vector<string> tokens;
    double x, y, z, msd, size_x, size_y, size_z;
    unsigned int slice = 0;

    // the first line is only the number of atoms.
    getline(iss, line);

    // from the second line, extract the system size
    // and convert from nm to angstrom
    getline(iss, line);

    //determine, if we have extended xyz or normal xyz format. In normal xyz format, this
    // line contains only 3 numbers, corresponding to cell size in x y and z direction. In
    // extended xyz, it is like this: Lattice="lx 0.0 0.0 0.0 ly 0.0 0.0 0.0 lz"
    if(line.find("Lattice=") != string::npos) {
        regex e("Lattice=\"([0-9\\seE+-\\.]+)\"", regex_constants::icase);
        smatch m;
        if(regex_search(line, m, e)) {
            tokens = algorithms::split(m[1], " \t");

            algorithms::trim(tokens[0]);
            algorithms::trim(tokens[4]);
            algorithms::trim(tokens[8]);

            size_x = std::stod(tokens[0]);
            size_y = std::stod(tokens[4]);
            size_z = std::stod(tokens[8]);
        } else {
            output::error("Wrong syntax of extended XYZ file line 2.");
        }
    } else {
        tokens = algorithms::split(line, " \t");

        algorithms::trim(tokens[0]);
        algorithms::trim(tokens[1]);
        algorithms::trim(tokens[2]);

        size_x = std::stod(tokens[0]);
        size_y = std::stod(tokens[1]);
        size_z = std::stod(tokens[2]);

    }

    auto cell = std::make_shared<stemsalabim::atomic::Cell>();
    cell->setLengths(size_x, size_y, size_z);

    // the rest of the lines is atoms with positions
    size_t atom_id = 0;
    while(getline(iss, line)) {
        if(line.length() == 0)
            continue;

        tokens = algorithms::split(line, "\t ");

        if(tokens.size() != 5 && tokens.size() != 6) {
            output::error("Init file needs to have 5 or 6 columns!\n");
        }

        bool with_slices = (tokens.size() == 6);

        algorithms::trim(tokens[0]);
        algorithms::trim(tokens[1]);
        algorithms::trim(tokens[2]);
        algorithms::trim(tokens[3]);
        algorithms::trim(tokens[4]);

        if(with_slices) {
            algorithms::trim(tokens[5]);
            slice = std::stoi(tokens[5]);
        }

        // x y z are columns 1,2,3 and the mean square displacement is column 4. Convert to angstrom
        x = std::stod(tokens[1]);
        y = std::stod(tokens[2]);
        z = std::stod(tokens[3]);
        msd = std::stod(tokens[4]);

        // set the site's occupant
        if(with_slices) {
            cell->addAtom(std::make_shared<atomic::Atom>(x, y, z, msd, elp.elementBySymbol(tokens[0]), atom_id, slice));
        } else {
            cell->addAtom(std::make_shared<atomic::Atom>(x, y, z, msd, elp.elementBySymbol(tokens[0]), atom_id));
        }

        cell->addElement(elp.elementBySymbol(tokens[0]));

        atom_id++;
    }

    return cell;

}

std::shared_ptr<stemsalabim::atomic::Cell> IO::initCrystalFromNCFile(const std::string &filename) {
    atomic::ElementProvider elp = atomic::ElementProvider::getInstance();


    vector<short> element_ids;
    vector<float> lattice_coords;
    vector<float> msds;
    vector<float> system_lengths;
    vector<string> atom_types;

    vector<int> slice_ids;

    vector<size_t> e_len;
    vector<size_t> a_len;

    NCFile f(filename, false, true);

    auto g = f.group("AMBER");
    auto lv = g.var("lattice_coordinates");
    auto ev = g.var("element");
    auto av = g.var("atom_types");
    auto mv = g.var("msd");

    auto slice_ids_variable = g.var("slice");

    e_len = ev.len();
    a_len = av.len();

    atom_types.resize(a_len[0]);
    char buff[100];
    for(size_t i = 0; i < a_len[0]; ++i) {
        av.get(vs({i, 0}), vs({1, a_len[1]}), buff);
        atom_types[i] = buff;
    }

    element_ids = ev.get<short>(vs({0, 0}), vs({1, e_len[1]}));
    lattice_coords = lv.get<float>(vs({0, 0, 0}), vs({1, e_len[1], 3}));
    msds = mv.get<float>(vs({0, 0}), vs({1, e_len[1]}));
    system_lengths = g.var("system_lengths").get<float>();

    slice_ids = slice_ids_variable.get<int>(vs({0, 0}), vs({1, e_len[1]}));

    auto cell = std::make_shared<stemsalabim::atomic::Cell>();

    for(const auto &at: atom_types) {
        cell->addElement(elp.elementBySymbol(at));
    }

    cell->setLengths(system_lengths[0], system_lengths[1], system_lengths[2]);
    for(size_t i_atom = 0; i_atom < e_len[1]; i_atom++) {
        cell->addAtom(make_shared<atomic::Atom>(lattice_coords[3 * i_atom + 0],
                                                lattice_coords[3 * i_atom + 1],
                                                lattice_coords[3 * i_atom + 2],
                                                msds[i_atom],
                                                elp.elementBySymbol(atom_types[element_ids[i_atom]]),
                                                i_atom,
                                                slice_ids[i_atom]));
    }


    return cell;
}

vector<vector<tuple<double, double, double>>> IO::readDisplacementsFromNCFile() {
    Params &p = Params::getInstance();
    auto &mpi_env = mpi::Environment::getInstance();

    vector<vector<tuple<double, double, double>>> displacements;

    if(mpi_env.isMaster()) {
        NCFile f(p.paramsFileName(), false, true);

        auto g = f.group("AMBER");
        auto cv = g.var("coordinates");
        auto lv = g.var("lattice_coordinates");

        auto len = cv.len();

        displacements.resize(len[0]);

        for(size_t i = 0; i < len[0]; ++i) {
            displacements[i].resize(len[1]);

            auto coords = cv.get<float>(vs({i, 0, 0}), vs({1, len[1], 3}));
            auto lattice_coords = lv.get<float>(vs({i, 0, 0}), vs({1, len[1], 3}));

            for(size_t i_atom = 0; i_atom < len[1]; i_atom++) {
                auto dx = coords[3 * i_atom + 0] - lattice_coords[3 * i_atom + 0];
                auto dy = coords[3 * i_atom + 1] - lattice_coords[3 * i_atom + 1];
                auto dz = coords[3 * i_atom + 2] - lattice_coords[3 * i_atom + 2];

                displacements[i][i_atom] = make_tuple(dx, dy, dz);
            }
        }
    }

    return displacements;
}

vector<vector<float>> IO::readStoredPotential(const std::shared_ptr<FPConfManager> &fpman, unsigned int slice_id) {
    Params &p = Params::getInstance();

    vector<float> flattened_potential;
    vector<size_t> len;

    auto &mpi_env = mpi::Environment::getInstance();

    if(mpi_env.isMaster()) {
        NCFile f(p.outputFilenamePotentials(), false, true);

        auto g = f.group("AMBER");
        auto v = g.var("slice_potentials");
        len = v.len();

        unsigned int iconf = fpman->currentConfiguration();

        flattened_potential = v.get<float>(vs({iconf, slice_id, 0, 0}), vs({1, 1, len[2], len[3]}));
    }

    if(mpi_env.isMpi()) {
        mpi_env.broadcast(len, 0);
        mpi_env.broadcast(flattened_potential, 0);
    }

    vector<vector<float>> pot(len[2], vector<float>(len[3]));

    for(size_t i = 0; i < len[2]; ++i) {
        for(size_t j = 0; j < len[3]; ++j) {
            pot[i][j] = flattened_potential[i * len[3] + j];
        }
    }

    return pot;

}

void IO::writeCOMx(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
                             const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf) {
    unique_lock<mutex> qlck(_io_lock);

    Params &p = Params::getInstance();

    if(!point.adf || !point.hasCOM_x())
        return;

    NCFile f(p.outputFilename());
    auto g = f.group("adf");
    auto v = g.var("com_x");

    try {
        IO::writeCOMx(idefocus, iconf, gridman, point, ibuf, v);

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}

void IO::writeCOMx(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
                             const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar &v) {

    Params &p = Params::getInstance();

    try {
        unsigned long nangles = gridman->adfDetectorGrid().size();
        unsigned int n_slices_total = (unsigned int) gridman->adfSliceCoords().size();
        unsigned long lenght_plasmon_dimension = 1;

        unsigned int idefocus_store = p.adfAverageDefoci() ? 0 : idefocus;
        unsigned int iconf_store = p.adfAverageConfigurations() ? 0 : iconf;

        float w = 1.0;

        if(p.adfAverageDefoci())
            w *= gridman->defocusWeights()[idefocus];

        if(p.adfAverageConfigurations())
            w /= p.numberOfConfigurations();

        if(iconf > iconf_store || idefocus > idefocus_store) {

            v.get(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
                  vs({1, 1, 1, 1, n_slices_total, lenght_plasmon_dimension, nangles}),
                  _com_x_buffer);

            for(size_t j = 0; j < _com_x_buffer.size(); ++j)
                _com_x_buffer[j] += w * ibuf->value(point.com_x, j);

        } else {
            for(size_t j = 0; j < _com_x_buffer.size(); ++j)
                _com_x_buffer[j] = w * ibuf->value(point.com_x, j);
        }

        v.put(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
              vs({1, 1, 1, 1, n_slices_total, lenght_plasmon_dimension, nangles}),
              _com_x_buffer);


    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}
void IO::writeCOMxPlasmon(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf) {
    unique_lock<mutex> qlck(_io_lock);

    Params &p = Params::getInstance();

    if(!point.adf || !point.hasCOM_x_plasmon())
        return;

    NCFile f(p.outputFilename());
    auto g = f.group("plasmon");
    auto v = g.var("com_x");

    try {
        IO::writeCOMxPlasmon(idefocus, iconf, gridman, point, ibuf, v);

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}

void IO::writeCOMxPlasmon(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar &v) {

    Params &p = Params::getInstance();

    try {
        unsigned long nangles = gridman->adfDetectorGrid().size();
        unsigned int n_slices_total = (unsigned int) gridman->plasmonSliceCoords().size();
        unsigned long nplasmons = p.numberOfPotentialPositionsPerSlice();

        unsigned int idefocus_store = p.adfAverageDefoci() ? 0 : idefocus;
        unsigned int iconf_store = p.adfAverageConfigurations() ? 0 : iconf;

        float w = 1.0;

        if(p.adfAverageDefoci())
            w *= gridman->defocusWeights()[idefocus];

        if(p.adfAverageConfigurations())
            w /= p.numberOfConfigurations();

        if(iconf > iconf_store || idefocus > idefocus_store) {

            v.get(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
                  vs({1, 1, 1, 1, n_slices_total, nplasmons, nangles}),
                  _com_x_plasmon_buffer);

            for(size_t j = 0; j < _com_x_plasmon_buffer.size(); ++j)
                _com_x_plasmon_buffer[j] += w * ibuf->value(point.com_x_plasmon, j);

        } else {
            for(size_t j = 0; j < _com_x_plasmon_buffer.size(); ++j)
                _com_x_plasmon_buffer[j] = w * ibuf->value(point.com_x_plasmon, j);
        }

        v.put(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
              vs({1, 1, 1, 1, n_slices_total, nplasmons, nangles}),
              _com_x_plasmon_buffer);


    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}



void IO::writeCOMy(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
                   const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf) {
    unique_lock<mutex> qlck(_io_lock);

    Params &p = Params::getInstance();

    if(!point.adf || !point.hasCOM_y())
        return;

    NCFile f(p.outputFilename());
    auto g = f.group("adf");
    auto v = g.var("com_y");

    try {
        IO::writeCOMy(idefocus, iconf, gridman, point, ibuf, v);

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}


void IO::writeCOMy(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
                   const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar &v) {

    Params &p = Params::getInstance();

    try {
        unsigned long nangles = gridman->adfDetectorGrid().size();
        unsigned int n_slices_total = (unsigned int) gridman->adfSliceCoords().size();
        unsigned long lenght_plasmon_dimension = 1;

        unsigned int idefocus_store = p.adfAverageDefoci() ? 0 : idefocus;
        unsigned int iconf_store = p.adfAverageConfigurations() ? 0 : iconf;

        float w = 1.0;

        if(p.adfAverageDefoci())
            w *= gridman->defocusWeights()[idefocus];

        if(p.adfAverageConfigurations())
            w /= p.numberOfConfigurations();

        if(iconf > iconf_store || idefocus > idefocus_store) {

            v.get(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
                  vs({1, 1, 1, 1, n_slices_total, lenght_plasmon_dimension, nangles}),
                  _com_y_buffer);

            for(size_t j = 0; j < _com_y_buffer.size(); ++j)
                _com_y_buffer[j] += w * ibuf->value(point.com_y, j);

        } else {
            for(size_t j = 0; j < _com_y_buffer.size(); ++j)
                _com_y_buffer[j] = w * ibuf->value(point.com_y, j);
        }

        v.put(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
              vs({1, 1, 1, 1, n_slices_total, lenght_plasmon_dimension, nangles}),
              _com_y_buffer);


    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}

void IO::writeCOMyPlasmon(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf) {
    unique_lock<mutex> qlck(_io_lock);

    Params &p = Params::getInstance();

    if(!point.adf || !point.hasCOM_y_plasmon())
        return;

    NCFile f(p.outputFilename());
    auto g = f.group("plasmon");
    auto v = g.var("com_y");

    try {
        IO::writeCOMyPlasmon(idefocus, iconf, gridman, point, ibuf, v);

    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}

void IO::writeCOMyPlasmon(unsigned int idefocus, unsigned int iconf, const shared_ptr<GridManager> &gridman,
        const ScanPoint &point, std::shared_ptr<memory::buffer::number_buffer<float>> &ibuf, NCVar &v) {

    Params &p = Params::getInstance();

    try {
        unsigned long nangles = gridman->adfDetectorGrid().size();
        unsigned int n_slices_total = (unsigned int) gridman->plasmonSliceCoords().size();
        unsigned long nplasmons = p.numberOfPotentialPositionsPerSlice();

        unsigned int idefocus_store = p.adfAverageDefoci() ? 0 : idefocus;
        unsigned int iconf_store = p.adfAverageConfigurations() ? 0 : iconf;

        float w = 1.0;

        if(p.adfAverageDefoci())
            w *= gridman->defocusWeights()[idefocus];

        if(p.adfAverageConfigurations())
            w /= p.numberOfConfigurations();

        if(iconf > iconf_store || idefocus > idefocus_store) {

            v.get(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
                  vs({1, 1, 1, 1, n_slices_total, nplasmons, nangles}),
                  _com_y_plasmon_buffer);

            for(size_t j = 0; j < _com_y_plasmon_buffer.size(); ++j)
                _com_y_plasmon_buffer[j] += w * ibuf->value(point.com_y_plasmon, j);

        } else {
            for(size_t j = 0; j < _com_y_plasmon_buffer.size(); ++j)
                _com_y_plasmon_buffer[j] = w * ibuf->value(point.com_y_plasmon, j);
        }

        v.put(vs({idefocus_store, point.adf_row, point.adf_col, iconf_store, 0, 0, 0}),
              vs({1, 1, 1, 1, n_slices_total, nplasmons, nangles}),
              _com_y_plasmon_buffer);


    } catch(NcException &e) {
        output::error("STEM %s\n", e.what());
    }
}


