/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include <chrono>

#include "Slice.hpp"

#include "Simulation.hpp"
#include "../libatomic/Scattering.hpp"
#include "../utilities/output.hpp"

using namespace std;
using namespace stemsalabim;


void Slice::calculateTransmissionFunction(const shared_ptr<GridManager> &gridman,
        const std::vector<std::vector<float>> &vz) {

    Params &p = Params::getInstance();

    unsigned int samples_x = gridman->samplingX();
    unsigned int samples_y = gridman->samplingY();

    _transmission_function.init(samples_x, samples_y, complex<float>(1.0, 0.0));

    // interaction parameter, convert to 1/(eV * A) from 1/(keV * A)
    double energy = p.beamEnergy();
    double wavelength = p.wavelength();
    double sigma = 2 *
                   Params::pi /
                   (wavelength * energy) *
                   (Params::ELECTRON_REST_MASS + energy) /
                   (2 * Params::ELECTRON_REST_MASS + energy) /
                   1000.;

    // finish calculation
    for(unsigned int current_x = 0; current_x < samples_x; ++current_x) {
        for(unsigned int current_y = 0; current_y < samples_y; ++current_y) {
            _transmission_function(current_x, current_y) = polar(1.0, sigma * vz[current_x][current_y]);
        }
    }

    // bandwidth limiting
    if(p.bandwidthLimiting()) {
        _transmission_function.forwardFFT();
        _transmission_function *= gridman->getBandwidthMask();
        _transmission_function.backwardFFT();
    }

}

vector<vector<float>>
Slice::calculatePotential(const shared_ptr<FPConfManager> &fpman, const shared_ptr<GridManager> &gridman) {

    Params &p = Params::getInstance();

    unsigned int samples_x = gridman->samplingX();
    unsigned int samples_y = gridman->samplingY();

    // see autoslic.cpp line 903ff of Kirklands code

    // first of all, define some scales, as we have the real inv_scale (Angstroms) and
    // the pixel inv_scale
    double scale_x = 1. / gridman->scalingFactorNanometerToGridPixelX();
    double scale_y = 1. / gridman->scalingFactorNanometerToGridPixelY();

    // define a maximal radius above which an atomic potential is zero
    // it is ok to have the same number for x and y, presumably, as the error
    // is very small when the densities are not equal.
    int pix_max = (int) ceil(p.maxPotentialRadius() * max(gridman->samplingX(), gridman->samplingY()));

    // Scattering calculations
    vector<vector<float>> vz(samples_x, vector<float>(samples_y, 0));

    atomic::Scattering &sf = atomic::Scattering::getInstance();

    for(auto &a: _atoms) {
        auto &pot_cache = sf.getPotential(a->getElement());

        // now add up all the potentials
        double dx = fpman->dx(a->getId());
        double dy = fpman->dy(a->getId());

        // find pixel coordinates of atom
        int pix_atom_x = (int) round((a->getX() + dx) / scale_x);
        int pix_atom_y = (int) round((a->getY() + dy) / scale_y);

        int x;
        int y;

        double dist_x;
        double dist_y;
        double distance;

        for(int current_x = pix_atom_x - pix_max; current_x <= pix_atom_x + pix_max; current_x++) {

            x = current_x;

            // periodic boundary conditions
            while(x < 0)
                x += samples_x;
            while(x >= (int) samples_x)
                x -= samples_x;

            for(int current_y = pix_atom_y - pix_max; current_y <= pix_atom_y + pix_max; current_y++) {

                y = current_y;

                // periodic boundary conditions
                while(y < 0)
                    y += samples_y;
                while(y >= (int) samples_y)
                    y -= samples_y;

                dist_x = current_x * scale_x - (a->getX() + dx);
                dist_y = current_y * scale_y - (a->getY() + dy);
                distance = sqrt(pow(dist_x, 2) + pow(dist_y, 2));

                int cache_index = sf.getIndex(distance);
                if(cache_index >= 0)
                    vz[x][y] += pot_cache[cache_index];
            }
        }
    }

    return vz;

}
