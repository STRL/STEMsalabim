/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#include "Params.hpp"

#include <thread>

#include "cereal/types/memory.hpp"
#include "cereal/archives/binary.hpp"
#include "cereal/types/vector.hpp"
#include "cereal/types/set.hpp"

#include "../utilities/netcdf_utils.hpp"
#include "../utilities/output.hpp"

using namespace libconfig;
using namespace std;
using namespace stemsalabim;


void Params::readParamsFromString(const string &prms) {

    try {
        _cfg.readString(prms);
#if LIBCONFIGXX_VER_MAJOR == 1 && LIBCONFIGXX_VER_MINOR < 7
        _cfg.setOptions(Setting::OptionAutoConvert & ~Setting::OptionOpenBraceOnSeparateLine);
#else
        _cfg.setOptions(Config::OptionAutoConvert & ~Config::OptionOpenBraceOnSeparateLine);
#endif
    } catch(const ParseException &pex) {
        output::error("Parse error at %s:%d - %s", pex.getFile(), pex.getLine(), pex.getError());
    }

    _cfg.lookupValue("application.random_seed", _random_seed);


    if(!cliParamGiven("title"))
        _cfg.lookupValue("simulation.title", _title);

    if(!cliParamGiven("output-file"))
        _cfg.lookupValue("simulation.output_file", _output_file);
    if(!cliParamGiven("tmp-dir"))
        _cfg.lookupValue("simulation.tmp_dir", _tmp_dir);
    _cfg.lookupValue("simulation.output_compress", _output_compress);

    _cfg.lookupValue("simulation.normalize_always", _normalize_always);
    _cfg.lookupValue("simulation.bandwidth_limiting", _bandwidth_limiting);
    _cfg.lookupValue("simulation.chunking_enabled", _chunking_enabled);
    _cfg.lookupValue("simulation.save_probe_wavefunction", _save_probe_wavefunction);

    _cfg.lookupValue("probe.c5", _probe_c5_coeff);
    _cfg.lookupValue("probe.cs", _probe_spherical_aberration_coeff);
    _cfg.lookupValue("probe.astigmatism_ca", _probe_astigmatism_ca);
    _cfg.lookupValue("probe.astigmatism_angle", _probe_astigmatism_angle);
    _cfg.lookupValue("probe.min_apert", _probe_min_aperture);
    _cfg.lookupValue("probe.max_apert", _probe_max_aperture);
    _cfg.lookupValue("probe.beam_energy", _beam_energy);
    _cfg.lookupValue("probe.scan_density", _probe_scan_density);
    _cfg.lookupValue("probe.aperture_bitmap_file", _aperture_bitmap_filename);

     if (not _aperture_bitmap_filename.empty())
         _aperture_bitmap_given = true;

    if(!cliParamGiven("defocus"))
        _cfg.lookupValue("probe.defocus", _probe_defocus);

    _cfg.lookupValue("probe.num_defoci", _probe_num_defoci);

    // Check both parameters in cfg file to keep backwards compatibility and not constitute an API break.
    _cfg.lookupValue("probe.fwhm_defoci", _delta_defocus_max);
    _cfg.lookupValue("probe.delta_defocus_max", _delta_defocus_max);

    _cfg.lookupValue("probe.fwhm_defocus_distribution", _fwhm_defocus_distribution);

    _cfg.lookupValue("specimen.max_potential_radius", _max_potential_radius);

    if(!cliParamGiven("crystal-file"))
        _cfg.lookupValue("specimen.crystal_file", _crystal_file);

    _cfg.lookupValue("grating.density", _sample_density);
    _cfg.lookupValue("grating.slice_thickness", _slice_thickness);

    if(!cliParamGiven("num-configurations"))
        _cfg.lookupValue("frozen_phonon.number_configurations", _number_configurations);
    _cfg.lookupValue("frozen_phonon.fixed_slicing", _fixed_slicing);
    _cfg.lookupValue("frozen_phonon.enabled", _fp_enabled);

    // CBED stuff
    if(_cfg.exists("cbed")) {
        _cfg.lookupValue("cbed.enabled", _cbed_enabled);
        _cfg.lookupValue("cbed.x.[0]", get<0>(_cbed_scan_x));
        _cfg.lookupValue("cbed.x.[1]", get<1>(_cbed_scan_x));
        _cfg.lookupValue("cbed.y.[0]", get<0>(_cbed_scan_y));
        _cfg.lookupValue("cbed.y.[1]", get<1>(_cbed_scan_y));
        _cfg.lookupValue("cbed.size.[0]", get<0>(_cbed_size));
        _cfg.lookupValue("cbed.size.[1]", get<1>(_cbed_size));
        _cfg.lookupValue("cbed.save_slices_every", _cbed_save_every_n_slices);
        _cfg.lookupValue("cbed.average_configurations", _cbed_average_configurations);
        _cfg.lookupValue("cbed.average_defoci", _cbed_average_defoci);
        _cfg.lookupValue("cbed.max_angle", _cbed_max_angle_mrad);

        // when we calculate a single defocus only, set average_defoci to false.
        if(_cbed_average_defoci && _probe_num_defoci == 1)
            _cbed_average_defoci = false;

        // when defoci are averaged, configurations need to be averaged as well.
        if(_cbed_average_defoci)
            _cbed_average_configurations = true;
    }

    // ADF stuff
    if(_cfg.exists("adf")) {
        _cfg.lookupValue("adf.enabled", _adf_enabled);
        _cfg.lookupValue("adf.x.[0]", get<0>(_adf_scan_x));
        _cfg.lookupValue("adf.x.[1]", get<1>(_adf_scan_x));
        _cfg.lookupValue("adf.y.[0]", get<0>(_adf_scan_y));
        _cfg.lookupValue("adf.y.[1]", get<1>(_adf_scan_y));

        _cfg.lookupValue("adf.detector_min_angle", get<0>(_adf_detector_angles));
        _cfg.lookupValue("adf.detector_max_angle", get<1>(_adf_detector_angles));
        _cfg.lookupValue("adf.detector_num_angles", get<2>(_adf_detector_angles));
        _cfg.lookupValue("adf.detector_interval_exponent", _adf_detector_interval_exponent);

        _cfg.lookupValue("adf.average_configurations", _adf_average_configurations);
        _cfg.lookupValue("adf.average_defoci", _adf_average_defoci);
        _cfg.lookupValue("adf.save_slices_every", _adf_save_every_n_slices);
    }

    if(_cfg.exists("plasmon_scattering")) {
        _cfg.lookupValue("plasmon_scattering.enabled", _plasmons_enabled);
        _cfg.lookupValue("plasmon_scattering.mean_free_path", _plasmons_mean_free_path);
        _cfg.lookupValue("plasmon_scattering.plasmon_energy", _plasmons_energy);
        _cfg.lookupValue("plasmon_scattering.energy_loss", _energy_loss);
        _cfg.lookupValue("plasmon_scattering.plasmon_fwhm", _plasmons_fwhm);
        _cfg.lookupValue("plasmon_scattering.plasmons_scatteringfunction_cutoff", _plasmons_scatteringfunction_cutoff);
        _cfg.lookupValue("plasmon_scattering.number_of_potential_positionsPerSlice", _number_of_potential_positions);
        _cfg.lookupValue("plasmon_scattering.save_slices_every", _plasmon_save_every_n_slices);
    }
    if(_cfg.exists("specimen_tilt")) {
        _cfg.lookupValue("specimen_tilt.enabled", _specimen_tilt_enabled);
        if (_specimen_tilt_enabled){
            _cfg.lookupValue("specimen_tilt.specimen_tilt_angle_x", _specimen_tilt_angle_x_mrad);
            _cfg.lookupValue("specimen_tilt.specimen_tilt_angle_y", _specimen_tilt_angle_y_mrad);
            _cfg.lookupValue("specimen_tilt.adjust_slice_thickness", _adjust_slice_thickness_for_specimen_tilt);
            // keep old names for specimen tilt angles to assure backwards compatibility
            _cfg.lookupValue("specimen_tilt.tilt_angle_x", _specimen_tilt_angle_x_mrad);
            _cfg.lookupValue("specimen_tilt.tilt_angle_y", _specimen_tilt_angle_y_mrad);
        }
    }
    if(_cfg.exists("beam_tilt")) {
        _cfg.lookupValue("beam_tilt.enabled", _beam_tilt_enabled);
        if(_beam_tilt_enabled){
            _cfg.lookupValue("beam_tilt.beam_tilt_angle_x", _beam_tilt_angle_x_mrad);
            _cfg.lookupValue("beam_tilt.beam_tilt_angle_y", _beam_tilt_angle_y_mrad);
            _cfg.lookupValue("beam_tilt.adjust_slice_thickness", _adjust_slice_thickness_for_beam_tilt);
        }
    }

    if(_tmp_dir.empty()) {
        // use the path of the output file directory for tmp
        int found = _output_file.find_last_of("/\\");
        if(found < 0)
            _tmp_dir = ".";
        else
            _tmp_dir = _output_file.substr(0, found);
    }

    if(!_fp_enabled && _number_configurations > 1)
        _number_configurations = 1;

    if(_random_seed == 0)
        _random_seed = (unsigned int) chrono::system_clock::now().time_since_epoch().count();
    _rgen.seed(_random_seed);

}

void Params::broadcast(int rank) {
    auto &mpi_env = mpi::Environment::getInstance();

    // communicate the cell
    {
        string serialized_cell;
        stringstream ss;
        if(mpi_env.rank() == rank) {
            cereal::BinaryOutputArchive oarchive(ss);
            oarchive(_cell);
            serialized_cell = ss.str();
        }

        mpi_env.broadcast(serialized_cell, rank);

        if(mpi_env.rank() != rank) {
            ss.str(serialized_cell);
            cereal::BinaryInputArchive iarchive(ss);
            iarchive(_cell);
        }
    }

    mpi_env.broadcast(_init_from_ncfile, rank);

    mpi_env.broadcast(_num_threads, rank);
    mpi_env.broadcast(_work_package_size, rank);
    mpi_env.broadcast(_random_seed, rank);

    mpi_env.broadcast(_title, rank);

    mpi_env.broadcast(_output_file, rank);
    mpi_env.broadcast(_tmp_dir, rank);
    mpi_env.broadcast(_output_compress, rank);

    mpi_env.broadcast(_normalize_always, rank);
    mpi_env.broadcast(_bandwidth_limiting, rank);
    mpi_env.broadcast(_chunking_enabled, rank);



    mpi_env.broadcast(_probe_c5_coeff, rank);
    mpi_env.broadcast(_probe_spherical_aberration_coeff, rank);
    mpi_env.broadcast(_probe_astigmatism_ca, rank);
    mpi_env.broadcast(_probe_astigmatism_angle, rank);
    mpi_env.broadcast(_probe_min_aperture, rank);
    mpi_env.broadcast(_probe_max_aperture, rank);
    mpi_env.broadcast(_beam_energy, rank);
    mpi_env.broadcast(_probe_scan_density, rank);
    mpi_env.broadcast(_aperture_bitmap_filename, rank);

    mpi_env.broadcast(_probe_defocus, rank);

    mpi_env.broadcast(_probe_num_defoci, rank);
    mpi_env.broadcast(_delta_defocus_max, rank);
    mpi_env.broadcast(_fwhm_defocus_distribution, rank);

    mpi_env.broadcast(_max_potential_radius, rank);

    mpi_env.broadcast(_crystal_file, rank);

    mpi_env.broadcast(_sample_density, rank);
    mpi_env.broadcast(_nkx, rank);
    mpi_env.broadcast(_nky, rank);
    mpi_env.broadcast(_slice_thickness, rank);

    mpi_env.broadcast(_number_configurations, rank);
    mpi_env.broadcast(_alternative_parallelization, rank);
    mpi_env.broadcast(_fixed_slicing, rank);
    mpi_env.broadcast(_fp_enabled, rank);

    // CBED stuff
    mpi_env.broadcast(get<0>(_cbed_scan_x), rank);
    mpi_env.broadcast(get<1>(_cbed_scan_x), rank);
    mpi_env.broadcast(get<0>(_cbed_scan_y), rank);
    mpi_env.broadcast(get<1>(_cbed_scan_y), rank);
    mpi_env.broadcast(get<0>(_cbed_size), rank);
    mpi_env.broadcast(get<1>(_cbed_size), rank);
    mpi_env.broadcast(_cbed_enabled, rank);
    mpi_env.broadcast(_cbed_save_every_n_slices, rank);
    mpi_env.broadcast(_cbed_average_configurations, rank);
    mpi_env.broadcast(_cbed_average_defoci, rank);
    mpi_env.broadcast(_cbed_max_angle_mrad, rank);

    mpi_env.broadcast(_adf_enabled, rank);
    mpi_env.broadcast(get<0>(_adf_scan_x), rank);
    mpi_env.broadcast(get<1>(_adf_scan_x), rank);
    mpi_env.broadcast(get<0>(_adf_scan_y), rank);
    mpi_env.broadcast(get<1>(_adf_scan_y), rank);

    mpi_env.broadcast(get<0>(_adf_detector_angles), rank);
    mpi_env.broadcast(get<1>(_adf_detector_angles), rank);
    mpi_env.broadcast(get<2>(_adf_detector_angles), rank);
    mpi_env.broadcast(_adf_detector_interval_exponent, rank);

    mpi_env.broadcast(_adf_average_configurations, rank);
    mpi_env.broadcast(_adf_average_defoci, rank);
    mpi_env.broadcast(_adf_save_every_n_slices, rank);

    mpi_env.broadcast(_plasmons_enabled, rank);
    mpi_env.broadcast(_plasmons_mean_free_path, rank);
    mpi_env.broadcast(_plasmons_energy, rank);
    mpi_env.broadcast(_energy_loss, rank);
    mpi_env.broadcast(_plasmons_fwhm, rank);
    mpi_env.broadcast(_plasmons_scatteringfunction_cutoff, rank);
    mpi_env.broadcast(_plasmon_save_every_n_slices, rank);
    mpi_env.broadcast(_number_of_potential_positions, rank);

    mpi_env.broadcast(_specimen_tilt_enabled, rank);
    mpi_env.broadcast(_specimen_tilt_angle_x_mrad, rank);
    mpi_env.broadcast(_specimen_tilt_angle_y_mrad, rank);
    mpi_env.broadcast(_adjust_slice_thickness_for_specimen_tilt, rank);

    mpi_env.broadcast(_beam_tilt_enabled, rank);
    mpi_env.broadcast(_beam_tilt_angle_x_mrad, rank);
    mpi_env.broadcast(_beam_tilt_angle_y_mrad, rank);
    mpi_env.broadcast(_adjust_slice_thickness_for_beam_tilt, rank);
}


void Params::readParamsFromNCFile() {
    readParamsFromNCFile(_param_file);
}

void Params::readParamsFromNCFile(const std::string & path) {

    NCFile f(path, false, true);
    auto g = f.group("params");

    _init_from_ncfile = true;

    {
        auto gg = g.group("application");
        _random_seed = gg.att<unsigned int>("random_seed");
    }

    {
        auto gg = g.group("simulation");

        if(!cliParamGiven("output-file"))
            _output_file = _param_file;

        _title = gg.att("title");
        _output_compress = gg.is("output_compress");
        _normalize_always = gg.is("normalize_always");
        _bandwidth_limiting = gg.is("bandwidth_limiting");
        _chunking_enabled = gg.is("chunking_enabled");

    }

    {
        auto gg = g.group("probe");

        _probe_c5_coeff = gg.att<double>("c5");
        _probe_spherical_aberration_coeff = gg.att<double>("cs");
        _probe_astigmatism_ca = gg.att<double>("astigmatism_ca");
        _probe_astigmatism_angle = gg.att<double>("astigmatism_angle");
        _probe_min_aperture = gg.att<double>("min_apert");
        _probe_max_aperture = gg.att<double>("max_apert");
        _beam_energy = gg.att<double>("beam_energy");
        _probe_scan_density = gg.att<double>("scan_density");
        _aperture_bitmap_filename = gg.att("aperture_bitmap_filename");
        if (not _aperture_bitmap_filename.empty())
            _aperture_bitmap_given = true;

        if(!cliParamGiven("defocus"))
            _probe_defocus = gg.att<double>("defocus");

        _probe_num_defoci = gg.att<unsigned int>("num_defoci");
        _delta_defocus_max = gg.att<double>("delta_defocus_max");
        _fwhm_defocus_distribution = gg.att<double>("fwhm_defocus_distribution");

    }

    {
        auto gg = g.group("specimen");

        _max_potential_radius = gg.att<double>("max_potential_radius");

        if(!cliParamGiven("crystal-file"))
            _crystal_file = gg.att("crystal_file");

    }

    {
        auto gg = g.group("grating");

        _sample_density = gg.att<double>("density");
        _slice_thickness = gg.att<double>("slice_thickness");
        _nkx = gg.att<unsigned int>("nx");
        _nky = gg.att<unsigned int>("ny");
    }

    {
        auto gg = g.group("frozen_phonon");

        if(!cliParamGiven("num-configurations"))
            _number_configurations = gg.att<unsigned int>("number_configurations");
        _fixed_slicing = gg.is("fixed_slicing");
        _fp_enabled = gg.is("enabled");

    }

    {
        auto gg = g.group("adf");

        _adf_enabled = gg.is("enabled");
        auto adf_scan_x = gg.attList<double>("x");
        auto adf_scan_y = gg.attList<double>("y");

        _adf_scan_x = make_tuple(adf_scan_x[0], adf_scan_x[1]);
        _adf_scan_y = make_tuple(adf_scan_y[0], adf_scan_y[1]);

        get<0>(_adf_detector_angles) = gg.att<double>("detector_min_angle");
        get<1>(_adf_detector_angles) = gg.att<double>("detector_max_angle");
        get<2>(_adf_detector_angles) = gg.att<unsigned int>("detector_num_angles");
        _adf_detector_interval_exponent = gg.att<float>("detector_interval_exponent");

        _adf_average_configurations = gg.is("average_configurations");
        _adf_average_defoci = gg.is("average_defoci");
        _adf_save_every_n_slices = gg.att<unsigned int>("save_slices_every");

        // when we calculate a single defocus only, set average_defoci to false.
        if(_adf_average_defoci && _probe_num_defoci == 1)
            _adf_average_defoci = false;
    }

    {
        auto gg = g.group("cbed");

        _cbed_enabled = gg.is("enabled");
        auto cbed_scan_x = gg.attList<double>("x");
        auto cbed_scan_y = gg.attList<double>("y");
        auto cbed_size = gg.attList<unsigned int>("size");

        _cbed_scan_x = make_tuple(cbed_scan_x[0], cbed_scan_x[1]);
        _cbed_scan_y = make_tuple(cbed_scan_y[0], cbed_scan_y[1]);
        _cbed_size = make_tuple(cbed_size[0], cbed_size[1]);

        _cbed_average_configurations = gg.is("average_configurations");
        _cbed_average_defoci = gg.is("average_defoci");
        _cbed_save_every_n_slices = gg.att<unsigned int>("save_slices_every");
        _cbed_max_angle_mrad = gg.att<double>("max_angle");

        if(!_fp_enabled && _number_configurations > 1) {
            _number_configurations = 1;
        }

    }

    {
        auto gg = g.group("plasmon_scattering");

        _plasmons_enabled = gg.is("enabled");
        _plasmons_mean_free_path = gg.att<float>("mean_free_path");
        _plasmons_energy = gg.att<float>("plasmon_energy");
        _energy_loss = gg.att<float>("energy_loss");
        _plasmons_fwhm = gg.att<float>("plasmon_fwhm");
        _plasmons_scatteringfunction_cutoff = gg.att<float>("plasmons_scatteringfunction_cutoff");
        _number_of_potential_positions = gg.att<unsigned int>("number_of_potential_positions");
        _plasmon_save_every_n_slices = gg.att<unsigned int>("save_slices_every");
    }

    {
        auto gg = g.group("specimen_tilt");
        _specimen_tilt_enabled = gg.is("enabled");
        _specimen_tilt_angle_x_mrad = gg.att<float>("specimen_tilt_angle_x_mrad");
        _specimen_tilt_angle_y_mrad = gg.att<float>("specimen_tilt_angle_y_mrad");
        _adjust_slice_thickness_for_specimen_tilt = gg.att<bool>("adjust_slice_thickness");
    }

    {
        auto gg = g.group("beam_tilt");
        _beam_tilt_enabled = gg.is("enabled");
        _beam_tilt_angle_x_mrad = gg.att<float>("beam_tilt_angle_x_mrad");
        _beam_tilt_angle_y_mrad = gg.att<float>("beam_tilt_angle_y_mrad");
        _adjust_slice_thickness_for_beam_tilt = gg.att<bool>("adjust_slice_thickness");
    }

}
