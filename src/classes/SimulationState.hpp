/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_SIMULATIONSTATE_HPP
#define STEMSALABIM_SIMULATIONSTATE_HPP

#include <vector>
#include <chrono>
#include <utility>

#include "../utilities/mpi.hpp"
#include "GridManager.hpp"
#include "FPConfManager.hpp"

namespace stemsalabim {

    /*!
     * Utility class to manage the current simulation state, i.e., current configuration, defocus etc.
     * It is supposed to be managed by SimulationStateManager
     */
    class SimulationState {
    public:
        /*!
         * Constructor to set the state
         */
        SimulationState(unsigned int idefocus, unsigned int iconf, unsigned int ndefoci, unsigned int nconfs,
                float defocus, unsigned int iteration)
                : _idefocus(idefocus)
                , _iconf(iconf)
                , _ndefoci(ndefoci)
                , _nconfs(nconfs)
                , _defocus(defocus)
                , _iteration(iteration) {}

        /*!
         * We provide a default constructor so that the class can be used for functions that
         * need a SimulationState but don't use it.
         */
        SimulationState() = default;

        /*!
         * Accessor method
         */
        inline unsigned int iconf() const {
            return _iconf;
        }

        /*!
         * Accessor method
         */
        inline unsigned int idefocus() const {
            return _idefocus;
        }

        /*!
         * Accessor method
         */
        inline float defocus() const {
            return _defocus;
        }

        /*!
         * Accessor method
         */
        inline unsigned int iteration() const {
            return _iteration;
        }

        /*!
         * Start the state. This only sets an internal timer for duration estimation.
         */
        inline void start() {
            _start_time = std::chrono::high_resolution_clock::now();
        };

        /*!
         * Get the estimated microseconds for completion of a configuration.
         */
        inline std::chrono::microseconds etaConf(float progress) const {
            return std::chrono::microseconds((long) round(algorithms::getTimeSince(_start_time).count() / progress));
        }

        /*!
         * Get the estimated microseconds for completion of a defocus.
         */
        inline std::chrono::microseconds etaDefocus(float progress) const {
            return etaConf(progress) * _nconfs;
        }

        /*!
         * Get the estimated microseconds for completion of the hole simulation.
         */
        inline std::chrono::microseconds etaSimulation(float progress) const {
            return etaConf(progress) * _nconfs * _ndefoci;
        }


    private:
        unsigned int _idefocus{0};
        unsigned int _iconf{0};
        unsigned int _ndefoci{0};
        unsigned int _nconfs{0};
        float _defocus{0};
        unsigned int _iteration{0};
        std::chrono::high_resolution_clock::time_point _start_time{};

    };

/*!
 * Class to manage the simulation states. Essentially wraps a vector of SimulationState that may be iterated.
 */

    class SimulationStateManager {
    public:
        /*!
         * Constructor. Prepares a vector of all the SimulationState objects that the simulation will go through.
         */
        explicit SimulationStateManager(std::shared_ptr<GridManager> gridman, bool parallel)
                : _gridman(std::move(gridman))
                , _parallel(parallel) {
            Params &prms = Params::getInstance();

            if(_parallel) {
                auto &mpi_env = mpi::Environment::getInstance();
                _rank = mpi_env.rank();
                _states.resize((unsigned long long)mpi_env.size());
            } else {
                _rank = 0;
                _states.resize(1);
            }

            unsigned int rank_index = 0;
            for(unsigned int idefocus = 0; idefocus < prms.numberOfDefoci(); idefocus++) {
                for(unsigned int iconf = 0; iconf < prms.numberOfConfigurations(); iconf++) {
                    unsigned int iteration = idefocus * prms.numberOfConfigurations() + iconf;
                    if(rank_index >= _states.size())
                        rank_index = 0;
                    _states[rank_index].push_back(SimulationState(idefocus,
                                                             iconf,
                                                             prms.numberOfDefoci(),
                                                             prms.numberOfConfigurations(),
                                                             _gridman->defoci()[idefocus],
                                                             iteration));
                    rank_index++;
                }
            }
        }

        typedef typename std::vector<SimulationState>::iterator iterator;
        typedef typename std::vector<SimulationState>::const_iterator const_iterator;

        iterator begin() { return _states[_rank].begin(); }

        const_iterator begin() const { return _states[_rank].begin(); }

        iterator end() { return _states[_rank].end(); }

        const_iterator end() const { return _states[_rank].end(); }

    private:

        /// The GridManager that manages all required sweeps and grids.
        std::shared_ptr<GridManager> _gridman;

        /// container of the SimulationState s
        std::vector<std::vector<SimulationState>> _states;

        /// whether or not the configurations are calculated in parallel
        bool _parallel{false};

        /// The index (rank if parallel) in _states that is calculated on this proc.
        int _rank;
    };
}

#endif //STEMSALABIM_SIMULATIONSTATE_HPP
