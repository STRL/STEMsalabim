/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_SLICE_HPP
#define STEMSALABIM_SLICE_HPP

#include <vector>
#include <memory>

#include "../utilities/Wave.hpp"
#include "../libatomic/Atom.hpp"

namespace stemsalabim {

    // TODO: We need a few forward declarations here, as Atom, Crystal, and Slice are somewhat
    // TODO: circularly dependent on each other. This should be changed at some point.

    class FPConfManager;

    class GridManager;

    /*!
     * Class that represents a Slice in the multi-slice algorithm. Its main task is to wrap a transmission
     * function that is mapped to this Slice. Also, it keeps a list of Atom s that belong to this Slice.
     */

    class Slice {
    public:

        /*!
         * Construct a Slice object with initial values for class variables.
         */
        Slice(double z, double dz, unsigned int id)
                : _z(z)
                , _thickness(dz)
                , _id(id) {}

        /*!
         * Construct a transmission function for this slice.
         */
        void calculateTransmissionFunction(const std::shared_ptr<GridManager> &gridman,
                const std::vector<std::vector<float>> &pot);

        void calculateTransmissionFunction(const std::shared_ptr<FPConfManager> &fpman,
                const std::shared_ptr<GridManager> &gridman) {
            calculateTransmissionFunction(gridman, calculatePotential(fpman, gridman));
        };

        /*!
         * Construct a coulomb potential for this slice.
         */
        std::vector<std::vector<float>>
        calculatePotential(const std::shared_ptr<FPConfManager> &fpman, const std::shared_ptr<GridManager> &gridman);

        /*!
         * Clear the cached transmission function.
         */
        void clearTransmissionFunction() {
            _transmission_function.clear();
        }

        /*!
         * Transmit a Complex2D wave through the transmission function.
         */
        void transmit(Wave &wave) const {
            wave *= _transmission_function;
        }

        /*!
         * Transmit a Complex2D wave through the transmission function.
         * @param wave The electronic wave to transmit
         */
        const Wave & transmissionFunction() const {
            return _transmission_function;
        }

        /*!
         * Add an atom to this slice.
         */
        void addAtom(std::shared_ptr<atomic::Atom> &atom) {
            _atoms.push_back(atom);
        }

        /*!
         * Add field to this slice.
         */
        void addField(double x, double y, double field) {
            _fields.push_back(std::make_tuple(x, y, field));
        }

        /*!
         * Get the z coordinate (the upper boundary with respect to electron beam direction) of
         * this Slice.
         */
        double z() const {
            return _z;
        }

        /*!
         * Get the slice thickness.
         */
        double thickness() const {
            return _thickness;
        }

        /*!
         * Get the number of Atom objects belonging to this slice.
         */
        unsigned long numberOfAtoms() const {
            return _atoms.size();
        }

        /*!
         * Get the ID of this slice, i.e., 0 based index.
         */
        unsigned int id() const {
            return _id;
        }

        /*!
         * Clear the Atom objects list.
         */
        void clearAtoms() {
            _atoms.clear();
        }

        /*!
         * Clear the Atom objects list.
         */
        void clearFields() {
            _fields.clear();
        }

        /*!
         * Get a vector of pointers to the Atom objects belonging to this Slice.
         */
        const std::vector<std::shared_ptr<atomic::Atom>> &atoms() const {
            return _atoms;
        }

    private:

        double _z;
        double _thickness;
        unsigned int _id;
        std::vector<std::shared_ptr<atomic::Atom>> _atoms;
        std::vector<std::tuple<double, double, double>> _fields;
        Wave _transmission_function;
    };
}

#endif //STEMSALABIM_SLICE_HPP
