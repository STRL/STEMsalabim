/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_GRIDMANAGER_HPP
#define STEMSALABIM_GRIDMANAGER_HPP

#include <utility>
#include "Params.hpp"
#include "Slice.hpp"
#include "../utilities/Wave.hpp"
#include "../utilities/memory.hpp"
#include "../utilities/algorithms.hpp"

namespace stemsalabim {

    /*!
     * Wrapper class that contains information about a single STEM scan point,
     * along with information where the related intensities are stored after
     * successful calculation.
     */

    class ScanPoint {

    public:
        typedef typename std::shared_ptr<memory::buffer::number_buffer<float>> ptr_buf_t;

        /*!
         * Empty constructor needed for serialization
         */
        ScanPoint() = default;

        /*!
         * Constructpr
         */
        ScanPoint(double x, double y, unsigned int adf_row, unsigned int adf_col, unsigned int cbed_row,
                unsigned int cbed_col, unsigned int index, bool adf, bool plasmon, bool cbed)
                : adf_row(adf_row)
                , adf_col(adf_col)
                , cbed_row(cbed_row)
                , cbed_col(cbed_col)
                , x(x)
                , y(y)
                , index(index)
                , adf(adf)
                , plasmon(plasmon)
                , cbed(cbed) {}

        /*!
         * Stores intensities in the member vector.
         */
        void storeAdfIntensities(const std::vector<float> &in, ptr_buf_t &buf) {
            adf_intensities = buf->add(in);
            _adf_intensities_stored = true;
        }

        /*!
        * Stores ADF plasmon intensities in the member vector.
        */
        void storeAdfPlasmonIntensities(const std::vector<float> &in, ptr_buf_t &buf) {
            adf_plasmon_intensities = buf->add(in);
            _adf_plasmon_intensities_stored = true;
        }

        /*!
        * Stores ADF plasmon intensities in the member vector given a pre-created buffer entry.
        */
        void storeAdfPlasmonIntensities(memory::buffer::entry<float> & buf_entry) {
            adf_plasmon_intensities = buf_entry;
            _adf_plasmon_intensities_stored = true;
        }

        /*!
         * Stores COM in the member vector.
         */
        void storeCOM(const std::vector<float> &in, ptr_buf_t &buf) {
            com = buf->add(in);
            _com_stored = true;
        }

        /*!
        * Stores COM in the member vector given a pre-created buffer entry.
        */
        void storeCOM(memory::buffer::entry<float> & buf_entry) {
            com = buf_entry;
            _com_stored = true;
        }

        /*!
         * Stores COM plasmon in the member vector.
         */
        void storeCOMPlasmon(const std::vector<float> &in, ptr_buf_t &buf) {
            com_plasmon = buf->add(in);
            _com_plasmon_stored = true;
        }

        /*!
        * Stores COM plasmon in the member vector given a pre-created buffer entry.
        */
        void storeCOMPlasmon(memory::buffer::entry<float> & buf_entry) {
            com_plasmon = buf_entry;
            _com_plasmon_stored = true;
        }

        /*!
        * Stores intensities in the member vector given a pre-created buffer entry.
        */
        void storeAdfIntensities(memory::buffer::entry<float> & buf_entry) {
            adf_intensities = buf_entry;
            _adf_intensities_stored = true;
        }

        /*!
         * Stores COM-x in the member vector.
         */
        void storeCOMx(const std::vector<float> &n_x, ptr_buf_t &buf) {
            com_x = buf->add(n_x);

            _com_x_stored = true;
        }
        /*!
         * Stores COM-x in the member vector given a pre-created buffer entry.
         */
        void storeCOMx(memory::buffer::entry<float> & buf_entry) {
            com_x = buf_entry;
            _com_x_stored = true;
        }

        /*!
         * Stores COM-x plasmon in the member vector.
         */
        void storeCOMxPlasmon(const std::vector<float> &n_x, ptr_buf_t &buf) {
            com_x_plasmon = buf->add(n_x);

            _com_x_plasmon_stored = true;
        }

        /*!
        * Stores COM-x plasmon in the member vector given a pre-created buffer entry.
        */
        void storeCOMxPlasmon(memory::buffer::entry<float> & buf_entry) {
            com_x_plasmon = buf_entry;
            _com_x_plasmon_stored = true;
        }

        /*!
         * Stores COM-y in the member vector.
         */
        void storeCOMy(const std::vector<float> &n_y, ptr_buf_t &buf) {
            com_y = buf->add(n_y);

            _com_y_stored = true;
        }

        /*!
         * Stores COM-y in the member vector given a pre-created buffer entry.
         */
        void storeCOMy(memory::buffer::entry<float> & buf_entry) {
            com_y = buf_entry;
            _com_y_stored = true;
        }

        /*!
        * Stores COM-y plasmon in the member vector.
        */
        void storeCOMyPlasmon(const std::vector<float> &n_x, ptr_buf_t &buf) {
            com_y_plasmon = buf->add(n_x);

            _com_y_plasmon_stored = true;
        }

        /*!
        * Stores COM-y plasmon in the member vector given a pre-created buffer entry.
        */
        void storeCOMyPlasmon(memory::buffer::entry<float> & buf_entry) {
            com_y_plasmon = buf_entry;
            _com_y_plasmon_stored = true;
        }

        /*!
         * Stores CBED intensities in the member vector.
         */
        void storeCBEDIntensities(const std::vector<float> &in, ptr_buf_t &buf) {
            cbed_intensities = buf->add(in);
            _cbed_intensities_stored = true;
        }

        /*!
         * Stores CBED intensities in the member vector  given a pre-created buffer entry.
         */
        void storeCBEDIntensities(memory::buffer::entry<float> & buf_entry) {
            cbed_intensities = buf_entry;
            _cbed_intensities_stored = true;
        }

        /*!
         * Clear COM in the member vector.
         */
        void clearCOM(ptr_buf_t &buf) {
            buf->remove(com);
            _com_stored = false;
        }

        /*!
        * Clear COM plasmon in the member vector.
        */
        void clearCOMPlasmon(ptr_buf_t &buf) {
            buf->remove(com_plasmon);
            _com_plasmon_stored = false;
        }

        /*!
         * Clear intensities in the member vector.
         */
        void clearAdfIntensities(ptr_buf_t &buf) {
            buf->remove(adf_intensities);
            _adf_intensities_stored = false;
        }

        /*!
         * Clear adf plasmon intensities in the member vector.
         */
        void clearAdfPlasmonIntensities(ptr_buf_t &buf) {
            buf->remove(adf_plasmon_intensities);
            _adf_plasmon_intensities_stored = false;
        }

        /*!
        * Clear COM-x in the member vector.
        */
        void clearCOM_x(ptr_buf_t &buf) {
            buf->remove(com_x);
            _com_x_stored = false;
        }

        /*!
        * Clear COM-x-plasmon in the member vector.
        */
        void clearCOM_x_plasmon(ptr_buf_t &buf) {
            buf->remove(com_x_plasmon);
            _com_x_plasmon_stored = false;
        }

        /*!
        * Clear COM-y-plasmon in the member vector.
        */
        void clearCOM_y_plasmon(ptr_buf_t &buf) {
            buf->remove(com_y_plasmon);
            _com_y_plasmon_stored = false;
        }

        /*!
        * Clear COM-y in the member vector.
        */
        void clearCOM_y(ptr_buf_t &buf) {
            buf->remove(com_y);
            _com_y_stored = false;
        }

        /*!
         * Clear CBED intensities in the member vector.
         */
        void clearCBEDIntensities(ptr_buf_t &buf) {
            buf->remove(cbed_intensities);
            _cbed_intensities_stored = false;
        }

        /*!
         * Check if CBED intensities are stored in this struct.
         */
        bool hasCbedIntensities() const {
            return _cbed_intensities_stored;
        }

        /*!
         * Check if intensities are stored in this struct.
         */
        bool hasAdfIntensities() const {
            return _adf_intensities_stored;
        }

        /*!
        * Check if ADF plasmon intensities are stored in this struct.
        */
        bool hasAdfPlasmonIntensities() const {
            return _adf_plasmon_intensities_stored;
        }

        /*!
         * Check if COM-x data are stored in this struct.
         */
        bool hasCOM_x() const {
            return _com_x_stored;
        }

        /*!
        * Check if COM-x plasmon data are stored in this struct.
        */
        bool hasCOM_x_plasmon() const {
            return _com_x_plasmon_stored;
        }

        /*!
        * Check if COM-y data are stored in this struct.
        */
        bool hasCOM_y() const {
            return _com_y_stored;
        }

        /*!
        * Check if COM-y plasmon data are stored in this struct.
        */
        bool hasCOM_y_plasmon() const {
            return _com_y_plasmon_stored;
        }

        /*!
        * Check if COM data are stored in this struct.
        */
        bool hasCOM() const {
            return _com_stored;
        }

        /*!
        * Check if COM data are stored in this struct.
        */
        bool hasCOMPlasmon() const {
            return _com_plasmon_stored;
        }

        // members are public, so we don't need getters/setters.

        unsigned int adf_row;
        unsigned int adf_col;
        unsigned int cbed_row;
        unsigned int cbed_col;
        double x;
        double y;
        unsigned int index;
        bool adf{false};
        bool plasmon{false};
        bool cbed{false};

        bool _com_x_stored{false};
        bool _com_x_plasmon_stored{false};
        bool _com_y_stored{false};
        bool _com_y_plasmon_stored{false};
        bool _com_stored{false};
        bool _com_plasmon_stored{false};
        bool _adf_intensities_stored{false};
        bool _adf_plasmon_intensities_stored{false};
        bool _cbed_intensities_stored{false};

        memory::buffer::entry<float> com_x;
        memory::buffer::entry<float> com_x_plasmon;
        memory::buffer::entry<float> com_y;
        memory::buffer::entry<float> com_y_plasmon;
        memory::buffer::entry<float> com;
        memory::buffer::entry<float> com_plasmon;
        memory::buffer::entry<float> adf_intensities;
        memory::buffer::entry<float> adf_plasmon_intensities;
        memory::buffer::entry<float> cbed_intensities;

    };

    /*!
     * Management class for the various parameter sweeps/grids required in the simulation.
     */

    class GridManager {

    public:
        explicit GridManager(std::shared_ptr<const atomic::Cell> crystal)
                : _crystal(std::move(crystal)) {}

        /*!
         * Generate the STEM scan points, work packages, slice coordinates, detector grid, and defocus values.
         * Must be called before usage of the class.
         */
        void generateGrids();

        /*!
         * Get the discretized detector k points.
         */
        const std::vector<double> adfDetectorGrid() const {
            return _detector_grid;
        }

        /*!
         * Get the z coordinates of the Slice objects of the multi-slice simulation.
         */
        const std::vector<double> adfSliceCoords() const {
            return _adf_slice_coords;
        }

        /*!
         * Get the z coordinates of the Slice objects of the CBED output multi-slice simulation.
         */
        const std::vector<double> cbedSliceCoords() const {
            return _cbed_slice_coords;
        }

        /*!
        * Get the z coordinates of the plasmon Slice objects of the multi-slice simulation.
        */
        const std::vector<double> plasmonSliceCoords() const {
            return _plasmon_slice_coords;
        }

        /*!
         * Get the z coordinates of the Slice objects
         */
        const std::vector<float> sliceCoords() const {
            return _slice_coords;
        }

        /*!
         * Return all ScanPoint objects for all the pixels that are calculated by the simulation.
         */
        const std::vector<ScanPoint> scanPoints() const {
            return _scan_points;
        }

        /*!
         * Get the real space coordinates of the discrete scan points in x direction.
         */
        const std::vector<double> &adfXGrid() const {
            return _adf_x_grid;
        }

        /*!
         * Get the real space coordinates of the discrete scan points in y direction.
         */
        const std::vector<double> &adfYGrid() const {
            return _adf_y_grid;
        }

        /*!
         * Get the total number of all scan points,
         * i.e. both ADF and CBED.
         */
        const int getTotalTotalNumberScanpoints() {
            return _scan_points.size();
        }

        /*!
         * Get the real space coordinates of the discrete CBED points in x direction.
         */
        const std::vector<double> &cbedXGrid() const {
            return _cbed_x_grid;
        }

        /*!
         * Get the real space coordinates of the discrete CBED points in y direction.
         */
        const std::vector<double> &cbedYGrid() const {
            return _cbed_y_grid;
        }

        /*!
         * Whether the slice with specified id is stored in the adf output or not.
         */
        bool adfStoreSlice(unsigned int id) const {
            return _adf_store_slice[id];
        }

        /*!
         * Whether the slice with specified id is stored in the cbed output or not.
         */
        bool cbedStoreSlice(unsigned int id) const {
            return _cbed_store_slice[id];
        }

        /*!
 * Whether the slice with specified id is stored in the cbed output or not.
 */
        bool plasmonStoreSlice(unsigned int id) const {
            return _plasmon_store_slice[id];
        }

        /*!
         * Index of the ADF bin (detector angle array) of k space indices kx and ky.
         */
        int adfBinIndex(unsigned int ix, unsigned int iy) const {
            return _adf_bin_index[ix][iy];
        }

        /*!
         * Get the defocus values that are simulated.
         */
        const std::vector<float> &defoci() const {
            return _defoci;
        }

        /*!
         * Generates the frequency space
         */
        void generateKSpace();

        /*!
         * Get the frequency / k value in x direction at grid point position i
         */
        double kx(unsigned int i) const {
            return _kx_space[i];
        }

        /*!
         * Get the frequency / k value in y direction at grid point position i
         */
        double ky(unsigned int i) const {
            return _ky_space[i];
        }

        /*!
         * Get the density of pixel per mrad in the reciprocal space, i.e. also in the CBEDs for the x direction.
         */
        double pixelPerMradX() const {
            Params &p = Params::getInstance();

            return 1.0 / (kx(1) * p.wavelength() * 1000);
        }

        /*!
        * Get the density of pixel per mrad in the reciprocal space, i.e. also in the CBEDs, for the x direction.
        */
        double MradPerPixelX() const {
            Params &p = Params::getInstance();

            return kx(1) * p.wavelength() * 1000;
        }

        /*!
        * Get the density of pixel per mrad in the reciprocal space, i.e. also in the CBEDs, for the y direction.
        */
        double MradPerPixelY() const {
            Params &p = Params::getInstance();

            return ky(1) * p.wavelength() * 1000;
        }

        /*!
         * Get the density of pixel per mrad in the reciprocal space, i.e. also in the CBEDs for the y direction.
         */
        double pixelPerMradY() const {
            Params &p = Params::getInstance();

            return 1.0 / (ky(1) * p.wavelength() * 1000);
        }


        /*
         * Return the index of the k-space grid best fitting the x beam tilt given by user.
         */
        int getBestFittingIndexshiftForBeamTiltX() const {
            Params &p = Params::getInstance();

            if (p.beamTiltAngleXmrad()==0)
                return 0;

            int index_to_shift_by_x = int (pixelPerMradX() * p.beamTiltAngleXmrad());

            float tilt_angle_effective_at_index = MradPerPixelX() * index_to_shift_by_x;
            float diff_to_input_value = p.beamTiltAngleXmrad() - tilt_angle_effective_at_index;
            float diff_to_input_value_temp;
            int new_index=index_to_shift_by_x;

            for (int ix=-1; ix<=1; ix++){
                tilt_angle_effective_at_index = MradPerPixelX() * (index_to_shift_by_x + ix);
                diff_to_input_value_temp = p.beamTiltAngleXmrad() - tilt_angle_effective_at_index;
                if (diff_to_input_value_temp < diff_to_input_value)
                    new_index = index_to_shift_by_x + ix;
            }
            index_to_shift_by_x = new_index;

            return index_to_shift_by_x;
        }

        /*
         * Return the index of the k-space grid best fitting the y beam tilt given by user.
         */
        int getBestFittingIndexshiftForBeamTiltY() const {
            Params &p = Params::getInstance();

            if (p.beamTiltAngleYmrad()==0)
                return 0;

            int index_to_shift_by_y = int (pixelPerMradY() * p.beamTiltAngleYmrad());

            float tilt_angle_effective_at_index = MradPerPixelY() * index_to_shift_by_y;
            float diff_to_input_value = p.beamTiltAngleYmrad() - tilt_angle_effective_at_index;
            float diff_to_input_value_temp;
            int new_index=index_to_shift_by_y;

            for (int iy=-1; iy <= 1; iy++){
                tilt_angle_effective_at_index = MradPerPixelY() * (index_to_shift_by_y + iy);
                diff_to_input_value_temp = p.beamTiltAngleYmrad() - tilt_angle_effective_at_index;
                if (diff_to_input_value_temp < diff_to_input_value)
                    new_index = index_to_shift_by_y + iy;
            }
            index_to_shift_by_y = new_index;

            return index_to_shift_by_y;
        }

        /*!
         * Get the frequencies / k values in x direction
         */
        const std::vector<double> &kx() const {
            return _kx_space;
        }

        /*!
         * Get the frequencies / k values in y direction
         */
        const std::vector<double> &ky() const {
            return _ky_space;
        }

        /*!
         * Get the real space value in x direction at grid point position i
         */
        double x(unsigned int i) const {
            return _x_space[i];
        }

        /*!
         * Get the real space value in y direction at grid point position i
         */
        double y(unsigned int i) const {
            return _y_space[i];
        }


        // We need to calculate the scales in x and y direction explicitely, because the
        // grating should be an even number.

        /*!
         * Get the x scale to translate integer grid points into a real space distance
         */
        double scalingFactorNanometerToGridPixelX() const {
            return samplingX() / _crystal->sizeX();
        }

        /*!
         * Get the y scale to translate integer grid points into a real space distance
         */
        double scalingFactorNanometerToGridPixelY() const {
            return samplingY() / _crystal->sizeY();
        }

        /*!
         * Get the grid sampling in x direction. It is rounded to an even integer because
         * of better behaving fourier transforms.
         */
        unsigned int samplingX() const {
            return _sampling_x;
            //return (unsigned int) algorithms::round_even(Params::getInstance().samplingDensity() * _crystal->sizeX());
        }

        /*!
         * Get the grid sampling in y direction. It is rounded to an even integer because
         * of better behaving fourier transforms.
         */
        unsigned int samplingY() const {
            return _sampling_y;
            //return (unsigned int) algorithms::round_even(Params::getInstance().samplingDensity() * _crystal->sizeY());
        }

        float densityX() const {
            return _sampling_x / (float)_crystal->sizeX();
        }

        float densityY() const {
            return _sampling_y / (float)_crystal->sizeY();
        }


        /*!
         * Return a wave object to multiply other waves with, that
         * will bandwidth limit the wave if requested.
         */
        const Wave & getBandwidthMask() const {
            return _bandwidth_limit_mask;
        }


        /*!
         * Calculates and returns the k grid values that are actually stored in the output file in kx direction.
         * Units: mrad
         */
        std::vector<double> outputKXGrid() const {
            Params &p = Params::getInstance();

            int bandwidth_lx = storedCbedSizeX(true);
            int final_lx = storedCbedSizeX();

            std::vector<double> tmp((unsigned long)bandwidth_lx);
            std::vector<double> outp((unsigned long)final_lx);

            for(int i = 0; i < bandwidth_lx; i++) {
                int ii = i;

                if(i > bandwidth_lx / 2)
                    ii = samplingX() - bandwidth_lx + i;

                // add to k array and rescale to mrad
                tmp[i] = _kx_space[ii] * p.wavelength() * 1e3;
            }

            if(final_lx == bandwidth_lx)
                return tmp;

            double min_kx = *std::min_element(std::begin(tmp), std::end(tmp));
            double max_kx = *std::max_element(std::begin(tmp), std::end(tmp));

            // if CBED is to be rescaled in X direction, calculate the scaled k values
            // here.
            for(int i = 0; i < final_lx; ++i) {
                if(i > final_lx / 2)
                    outp[i] = (i - final_lx) * (max_kx - min_kx) / storedCbedSizeY();
                else
                    outp[i] = i * (max_kx - min_kx) / storedCbedSizeY();
            }

            return outp;
        }

        /*!
         * Calculates and returns the k grid values that are actually stored in the output file in ky direction.
         * Units: mrad
         */
        std::vector<double> outputKYGrid() const {
            Params &p = Params::getInstance();

            int bandwidth_ly = storedCbedSizeY(true);
            int final_ly = storedCbedSizeY();

            std::vector<double> tmp((unsigned long)bandwidth_ly);
            std::vector<double> outp((unsigned long)storedCbedSizeX());

            for(int i = 0; i < bandwidth_ly; i++) {
                int ii = i;

                if(i > bandwidth_ly / 2)
                    ii = samplingY() - bandwidth_ly + i;

                // add to k array and rescale to mrad
                tmp[i] = _ky_space[ii] * p.wavelength() * 1e3;
            }

            if(final_ly == bandwidth_ly)
                return tmp;

            double min_ky = *std::min_element(std::begin(tmp), std::end(tmp));
            double max_ky = *std::max_element(std::begin(tmp), std::end(tmp));

            // if CBED is to be rescaled in X direction, calculate the scaled k values
            // here.
            for(int i = 0; i < final_ly; ++i) {
                if(i > final_ly / 2)
                    outp[i] = (i - final_ly) * (max_ky - min_ky) / storedCbedSizeY();
                else
                    outp[i] = i * (max_ky - min_ky) / storedCbedSizeY();
            }

            return outp;
        }

        /*!
         * Get the relative weights of the defoci when averaging. The weights are
         * determined simulating a Gaussian profile around the center defocus. This
         * is a simulated chromatic aberration.
         */
        const std::vector<float> &defocusWeights() const {
            return _defocus_weights;
        }

        /*!
         * Get the size of the stored CBED, taking into account bandwidth limiting
         * and rescaling in X direction.
         */
        unsigned int storedCbedSizeX(bool ignore_resize = false) const {
            Params &p = Params::getInstance();

            if (p.cbedMaxAngle()){
                int max_angle_cbed_pixel = p.cbedMaxAngle() * pixelPerMradX() * 2;
                return max_angle_cbed_pixel;
            }

            if(p.cbedRescale() && !ignore_resize)
                return p.cbedSizeX();

            if(!p.bandwidthLimiting())
                return samplingX();

            return (unsigned int) ceil(samplingX() * 2. / 3.);
        }

        /*!
         * Get the size of the stored CBED, taking into account bandwidth limiting
         * and rescaling in Y direction.
         */
        unsigned int storedCbedSizeY(bool ignore_resize = false) const {
            Params &p = Params::getInstance();

            if (p.cbedMaxAngle()){
                int max_angle_cbed_pixel = p.cbedMaxAngle() * pixelPerMradY() * 2;
                return max_angle_cbed_pixel;
            }

            if(p.cbedRescale() && !ignore_resize)
                return p.cbedSizeY();

            if(!p.bandwidthLimiting())
                return samplingY();

            return (unsigned int) ceil(samplingY() * 2. / 3.);
        }


        /*!
         * Get the number of slices of the multi-slice simulation
         */
        unsigned int numberOfSlices() const {
            return (unsigned int) _slices.size();
        }

        /*!
         * Get a vector of pointers to the Slice objects of the multi-slice algorithm
         */
        const std::vector<std::shared_ptr<Slice>> &slices() const {
            return _slices;
        }

        /*!
         * Get a vector of pointers to the Slice objects of the multi-slice algorithm. const version
         */
        std::vector<std::shared_ptr<Slice>> &slices() {
            return _slices;
        }


        /*
         * Get the positions that the plasmon scattering potential B(x) will be switched by
         * in real space for one slice for one specific set of indices.
         */

        std::vector<std::vector<unsigned int>> positionsToSwitchPlsmPotBy(
                unsigned int ix,
                unsigned int iy,
                unsigned int idef,
                unsigned int iphon,
                unsigned int islice){

            return _positionsToSwitchPlsmPotBy[ix][iy][idef][iphon][islice];
        }



        /*!
         * For a given z coordinate, get the index of the slice that contains this z coordinate.
         * Negative z coordinates always result in slice 0, z coordinates that are larger than the
         * cell size in z direction result in the last slice's index.
         */
        unsigned int sliceIndex(double z) const {
            Params &p = Params::getInstance();

            double dz = p.sliceThickness();
            double slice_top_offset = -dz / 8.0;
            auto slice_index = (int) floor((z - slice_top_offset) / dz);

            if(slice_index < 0)
                slice_index = 0;

            if(slice_index >= (int) numberOfSlices())
                slice_index = (int) numberOfSlices() - 1;

            return (unsigned int) slice_index;
        }

        double ApertureBitmapAtIndex(int x, int y){
            return _aperture_bitmap[x][y];
        }




    private:
        std::shared_ptr<const atomic::Cell> _crystal;

        std::vector<double> _detector_grid;
        std::vector<double> _adf_slice_coords;
        std::vector<double> _cbed_slice_coords;
        std::vector<double> _plasmon_slice_coords;
        std::vector<float> _slice_coords;
        std::vector<float> _defoci;
        std::vector<float> _defocus_weights;
        std::vector<ScanPoint> _scan_points;

        std::vector<double> _kx_space;
        std::vector<double> _ky_space;
        std::vector<double> _x_space;
        std::vector<double> _y_space;

        std::vector<std::shared_ptr<Slice>> _slices;

        std::vector<bool> _adf_store_slice;
        std::vector<bool> _cbed_store_slice;
        std::vector<bool> _plasmon_store_slice;

        std::vector<std::vector<int>> _adf_bin_index;

        std::vector<double> _adf_x_grid;
        std::vector<double> _adf_y_grid;
        std::vector<double> _cbed_x_grid;
        std::vector<double> _cbed_y_grid;

        unsigned int _sampling_x;
        unsigned int _sampling_y;

        Wave _bandwidth_limit_mask;

        std::vector<std::vector<double>> _aperture_bitmap;

        // Multidimensional vector which contains the position shift for B(x),
        // applied as as index shifts in the real space grid.
        // One dimension each for x, y, defocus, phonon-config, slice, (number of positions per slice)
        // and finally the shift in x and y direction.
        std::vector<std::vector<std::vector<std::vector<std::vector<std::vector<std::vector<unsigned int> > > > > > >_positionsToSwitchPlsmPotBy;


        void generatePositionSwitchesForBx(unsigned int NposX,unsigned int NposY);

        /*!
        * From a text file, read the values to fill _aperture_bitmap with.
         * It is assumed that the text file has a number of columns and rows equal to the grid size in x and y
         * direction, respectively.
        */
        void setApertureBitmap();


        };

}

#endif //STEMSALABIM_GRIDMANAGER_HPP
