/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/


#include <utility>

#ifndef STEMSALABIM_FROZENPHONONCONFIGURATIONMANAGER_HPP
#define STEMSALABIM_FROZENPHONONCONFIGURATIONMANAGER_HPP

#include "GridManager.hpp"

namespace stemsalabim {

    /*!
     * Management class of Frozen Phonon configurations.
     * It is responsible for generating and returning atomic displacements based on
     * the current configuration index.
     */

    class FPConfManager {

    public:

        template<class Archive>
        void serialize(Archive &ar) {
            ar(_displacements, _total_configurations, _current_configuration);
        }

        /*!
         * Constructor, sets member variables.
         */
        explicit FPConfManager(std::shared_ptr<const atomic::Cell> crystal)
                : _crystal(std::move(crystal))
                , _total_configurations(Params::getInstance().numberOfConfigurations() *
                                        Params::getInstance().numberOfDefoci())
                , _current_configuration(0) {}

        void setCrystal(std::shared_ptr<const atomic::Cell> crystal) {
            _crystal = std::move(crystal);
        }

        /*!
         * In this function the atomic displacements are generated based on their mean
         * square displacement parameters.
         */
        void generateDisplacements() {
            // atomic displacements are cached for all configurations

            Params &p = Params::getInstance();
            bool fixed_slicing = p.isFixedSlicing();

            std::normal_distribution<double> distribution(0.0, 1.0);
            auto rgen = p.getRandomGenerator();

            _displacements.resize(_total_configurations);

            for(size_t j = 0; j < _total_configurations; j++) {

                _displacements[j].resize(_crystal->numberOfAtoms());

                for(size_t i = 0; i < _crystal->numberOfAtoms(); i++) {
                    if(p.fpEnabled()) {

                        std::get<0>(_displacements[j][i]) = sqrt(_crystal->getAtoms()[i]->getMSD()) * distribution(rgen);
                        std::get<1>(_displacements[j][i]) = sqrt(_crystal->getAtoms()[i]->getMSD()) * distribution(rgen);
                        if(fixed_slicing) {
                            std::get<2>(_displacements[j][i]) = 0;
                        } else {
                            std::get<2>(_displacements[j][i]) = sqrt(_crystal->getAtoms()[i]->getMSD()) * distribution(rgen);
                        }
                    } else {
                        std::get<0>(_displacements[j][i]) = 0.0;
                        std::get<1>(_displacements[j][i]) = 0.0;
                        std::get<2>(_displacements[j][i]) = 0.0;
                    }
                }
            }
        };

        /*!
         * Set the current FP configuration index.
         */
        void setConfiguration(unsigned int iconf) {
            _current_configuration = iconf;
        }

        /*!
         * Assign atoms to slices. This function is moved to FPConfManager as the slice
         * depends on the z coordinate of the atoms, which may be subject to displacements.
         */
        void assignAtomsToSlices(const std::shared_ptr<GridManager> &gridman) {

            for(auto &slic: gridman->slices())
                slic->clearAtoms();

            size_t i = 0;
            for(auto a: _crystal->getAtoms()) {
                unsigned int slice_index;
                if(a->hasSlice())
                    slice_index = a->getSlice();
                else {
                    double z = a->getZ() + dz(i);
                    slice_index = gridman->sliceIndex(z);
                }

                gridman->slices()[slice_index]->addAtom(a);

                i++;
            }
        };

        /*!
         * Get atomic displacement in x direction for the atom with id atom_id.
         */
        double dx(unsigned long atom_id) const {
            return std::get<0>(_displacements[_current_configuration][atom_id]);
        }

        /*!
         * Get atomic displacement in y direction for the atom with id atom_id.
         */
        double dy(unsigned long atom_id) const {
            return std::get<1>(_displacements[_current_configuration][atom_id]);
        }

        /*!
         * Get atomic displacement in z direction for the atom with id atom_id.
         */
        double dz(unsigned long atom_id) const {
            return std::get<2>(_displacements[_current_configuration][atom_id]);
        }

        /*!
         * Get atomic displacement in x direction for the atom with id atom_id and configuration index iconf.
         */
        double dx(unsigned int iconf, unsigned long atom_id) const {
            return std::get<0>(_displacements[iconf][atom_id]);
        }

        /*!
         * Get atomic displacement in y direction for the atom with id atom_id and configuration index iconf.
         */
        double dy(unsigned int iconf, unsigned long atom_id) const {
            return std::get<1>(_displacements[iconf][atom_id]);
        }

        /*!
         * Get atomic displacement in z direction for the atom with id atom_id and configuration index iconf.
         */
        double dz(unsigned int iconf, unsigned long atom_id) const {
            return std::get<2>(_displacements[iconf][atom_id]);
        }

        /*!
         * Return the number of FP configurations.
         */
        unsigned int numberOfConfigurations() const {
            return _total_configurations;
        }

        /*!
         * Get the current FP configuration.
         */
        unsigned int currentConfiguration() const {
            return _current_configuration;
        }

        void setDisplacements(const std::vector<std::vector<std::tuple<double, double, double>>> & d) {
            _displacements = d;
        }

    private:
        std::shared_ptr<const atomic::Cell> _crystal;
        std::vector<std::vector<std::tuple<double, double, double>>> _displacements;

        unsigned int _total_configurations;
        unsigned int _current_configuration;
    };

}

#endif //STEMSALABIM_FROZENPHONONCONFIGURATIONMANAGER_HPP
