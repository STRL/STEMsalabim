/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

// This namespace includes some algorithms functions for various purposes.

#ifndef ALGORITHMS_HPP_
#define ALGORITHMS_HPP_

/*! @file algorithms.hpp */

#include <vector>
#include <string>
#include <random>
#include <cstring>
#include <functional>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <chrono>
#include <iterator>
#include <cmath>

/*!
 * Globale STEMsalabim namespace.
 */
namespace stemsalabim {
    /*!
     * Contains various algorithmic helper functions used in STEMsalabim.
     */
    namespace algorithms {

        template<typename T>
        void ignore(T &&) {}

        /*!
         * Round a number to the next higher even integer. For example, 3.3 would
         * be rounded to 4, as would 2.9.
         */
        template<typename numeric_t>
        int round_even(numeric_t val) {
            int res = (int) std::round(val);
            return res + res % 2;
        }

        /*!
         * Split a string by a (char) delimiter.
         */
        inline std::vector<std::string>
        split(const std::string &str, const std::string &delimiters = " ", bool trimEmpty = true) {
            std::string::size_type pos, lastPos = 0, length = str.length();

            std::vector<std::string> tokens;

            using value_type = typename std::vector<std::string>::value_type;
            using size_type  = typename std::vector<std::string>::size_type;

            while(lastPos < length + 1) {
                pos = str.find_first_of(delimiters, lastPos);
                if(pos == std::string::npos) {
                    pos = length;
                }

                if(pos != lastPos || !trimEmpty)
                    tokens.emplace_back(value_type(str.data() + lastPos, (size_type) pos - lastPos));

                lastPos = pos + 1;
            }

            return tokens;
        }

        /*!
         * Concatenate a vector of strings using a custom separator.
         */
        inline std::string join(const std::vector<std::string> &elements, const char *const separator) {
            switch(elements.size()) {
                case 0:
                    return "";
                case 1:
                    return elements[0];
                default:
                    std::ostringstream os;
                    std::copy(elements.begin(), elements.end() - 1, std::ostream_iterator<std::string>(os, separator));
                    os << *elements.rbegin();
                    return os.str();
            }
        }

        /*!
         * Trim a string (remove spaces) in-place from the left
         */
        static inline void ltrim(std::string &s) {
            s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        }

        /*!
         * Trim a string (remove spaces) in-place from the right.
         */
        static inline void rtrim(std::string &s) {
            s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(),
                    s.end());
        }

        /*!
         * Trim a string (remove spaces) in-place from both sides.
         */
        static inline void trim(std::string &s) {
            ltrim(s);
            rtrim(s);
        }

        /*!
         * Trim a string (remove spaces) by copying from the left.
         */
        static inline std::string ltrimmed(std::string s) {
            ltrim(s);
            return s;
        }

        /*!
         * Trim a string (remove spaces) by copying from the right.
         */
        static inline std::string rtrimmed(std::string s) {
            rtrim(s);
            return s;
        }

        /*!
         * Trim a string (remove spaces) by copying from both sides.
         */
        static inline std::string trimmed(std::string s) {
            trim(s);
            return s;
        }

        /*!
         * Get the closest entry in a vector to a given numeric value.
         */
        inline double closestValue(std::vector<double> v, double val) {
            std::vector<double>::iterator low, up;

            up = upper_bound(v.begin(), v.end(), val);
            low = up - 1;

            if((val - *low) < (*up - val))
                return *low;
            else
                return *up;
        }

        /*!
         * Return distributed numbers between min and max. The interval may not be linear, when
         * exponent != 1. Returns num_steps numbers calculated as
         * \f$ret[i] = x_{min} + \left(\frac{i}{N}\right)^p * (x_{max} - x_{min})\f$
         */
        inline std::vector<double>
        adaptiveSpace(double min, double max, unsigned int num_steps, float exponent, bool include_max) {
            std::vector<double> ret(num_steps);

            unsigned int offset = 0;

            if(include_max)
                offset = 1;

            for(unsigned int c = 0; c < num_steps; c++)
                ret[c] = min + pow((double) c / (num_steps - offset), exponent) * (max - min);

            return ret;
        }

        /*!
         * Return distributed numbers between min and max with equal distances.
         */
        inline std::vector<double> linSpace(double min, double max, unsigned int num_steps, bool include_max) {
            return adaptiveSpace(min, max, num_steps, 1.0, include_max);
        }

        inline int getIndexOfAdaptiveSpace(double val, double min, double max, unsigned int num_steps, float exponent,
                bool include_max) {

            if(val - min < 0)
                return -1;

            unsigned int offset = 0;

            if(include_max)
                offset = 1;

            double last_value = (double) pow((num_steps - 1) * (max - min) / (num_steps - offset), exponent);

            return (int) floor(pow((val - min) / (max - min) * last_value, 1. / exponent) /
                               (max - min) *
                               (num_steps - offset));
        }


        /*!
         * Get elapsed seconds since time t.
         */
        inline std::chrono::microseconds getTimeSince(std::chrono::high_resolution_clock::time_point t) {
            return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t);
        }

        /*!
         * Utility function that clamps a pixel value to [0, length[.
         */
        inline unsigned int clamp(int px, unsigned int length) {
            return px >= (int) length ? length - 1 : (px < 0 ? 0 : (unsigned int) px);
        }

        /*!
         * Carry out cubic interpolation.
         */

//            float cubicInterpolate(float A, float B, float C, float D, float t) {
//                float a = -A / 2.0 + (3.0 * B) / 2.0 - (3.0 * C) / 2.0 + D / 2.0;
//                float b = A - (5.0 * B) / 2.0 + 2.0 * C - D / 2.0;
//                float c = -A / 2.0 + C / 2.0;
//                float d = B;
//
//                return a * t * t * t + b * t * t + c * t + d;
//            }

        /*!
         * Rescale an image (represented as a flattened 1D vector of intensities) to a new size using
         * bicubic interpolation. This algorithm is unfavourable as it turned out, but I'll leave the code
         * here in case it is used one day.
         */

//            std::vector<float>
//            bicubicRescale(const std::vector<float> &input, unsigned int dest_width, unsigned int dest_height,
//                    unsigned int src_width, unsigned int src_height) {
//                std::vector<float> out(dest_width * dest_height, 0);
//
//                const float tx = float(src_width) / dest_width;
//                const float ty = float(src_height) / dest_height;
//
//                for(unsigned int i = 0; i < dest_width; ++i) {
//                    for(unsigned int j = 0; j < dest_height; ++j) {
//
//                        const int x = int(tx * i);
//                        const int y = int(ty * j);
//                        const float dx = tx * i - x;
//                        const float dy = ty * j - y;
//
//                        // 1st row
//                        float p00 = input[clamp(x - 1, src_width) * src_height + clamp(y - 1, src_height)];
//                        float p10 = input[clamp(x + 0, src_width) * src_height + clamp(y - 1, src_height)];
//                        float p20 = input[clamp(x + 1, src_width) * src_height + clamp(y - 1, src_height)];
//                        float p30 = input[clamp(x + 2, src_width) * src_height + clamp(y - 1, src_height)];
//
//                        // 2nd row
//                        float p01 = input[clamp(x - 1, src_width) * src_height + clamp(y + 0, src_height)];
//                        float p11 = input[clamp(x + 0, src_width) * src_height + clamp(y + 0, src_height)];
//                        float p21 = input[clamp(x + 1, src_width) * src_height + clamp(y + 0, src_height)];
//                        float p31 = input[clamp(x + 2, src_width) * src_height + clamp(y + 0, src_height)];
//
//                        // 3rd row
//                        float p02 = input[clamp(x - 1, src_width) * src_height + clamp(y + 1, src_height)];
//                        float p12 = input[clamp(x + 0, src_width) * src_height + clamp(y + 1, src_height)];
//                        float p22 = input[clamp(x + 1, src_width) * src_height + clamp(y + 1, src_height)];
//                        float p32 = input[clamp(x + 2, src_width) * src_height + clamp(y + 1, src_height)];
//
//                        // 4th row
//                        float p03 = input[clamp(x - 1, src_width) * src_height + clamp(y + 2, src_height)];
//                        float p13 = input[clamp(x + 0, src_width) * src_height + clamp(y + 2, src_height)];
//                        float p23 = input[clamp(x + 1, src_width) * src_height + clamp(y + 2, src_height)];
//                        float p33 = input[clamp(x + 2, src_width) * src_height + clamp(y + 2, src_height)];
//
//                        // interpolate bi-cubically!
//                        float col0 = cubicInterpolate(p00, p10, p20, p30, dx);
//                        float col1 = cubicInterpolate(p01, p11, p21, p31, dx);
//                        float col2 = cubicInterpolate(p02, p12, p22, p32, dx);
//                        float col3 = cubicInterpolate(p03, p13, p23, p33, dx);
//
//                        // as bicubic interpolation may result in values outside of the original boundaries,
//                        // we clip to 0 here. This results in artefacts and is the reason why this kind of
//                        // interpolation is not really good for our purposes.
//                        out[i * dest_height + j] = std::max(cubicInterpolate(col0, col1, col2, col3, dy), (float) 0.0);
//                    }
//                }
//
//                return out;
//            }

        /*!
         * Rescale an image (represented as a flattened 1D vector of intensities) to a new size using
         * bilinear interpolation.
         */

        inline std::vector<float>
        bilinearRescale(const std::vector<float> &input, unsigned int dest_width, unsigned int dest_height,
                unsigned int src_width, unsigned int src_height) {
            std::vector<float> out(dest_width * dest_height, 0);

            const float tx = float(src_width) / dest_width;
            const float ty = float(src_height) / dest_height;

            for(unsigned int i = 0; i < dest_width; ++i) {
                for(unsigned int j = 0; j < dest_height; ++j) {

                    const int x = int(tx * i);
                    const int y = int(ty * j);
                    const float dx = tx * i - x;
                    const float dy = ty * j - y;

                    // 1st row
                    float p00 = input[clamp(x + 0, src_width) * src_height + clamp(y + 0, src_height)];
                    float p01 = input[clamp(x + 0, src_width) * src_height + clamp(y + 1, src_height)];
                    float p10 = input[clamp(x + 1, src_width) * src_height + clamp(y + 0, src_height)];
                    float p11 = input[clamp(x + 1, src_width) * src_height + clamp(y + 1, src_height)];

                    // formula from here: https://en.wikipedia.org/wiki/Bilinear_interpolation
                    out[i * dest_height + j] = p00 * (1.0 - dx) * (1.0 - dy) +
                                               p10 * dx * (1.0 - dy) +
                                               p01 * (1.0 - dx) * dy +
                                               p11 * dx * dy;
                }
            }

            return out;
        }

        /*!
         * Sign function
         */
        template<typename T>
        int sgn(T val) {
            return (T(0) < val) - (val < T(0));
        }

        /*!
         * Read an ASCII file to a std::string
         */
        inline std::string readTextFile(const std::string &filename) {
            std::ifstream t(filename);
            return std::string((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
        }

        inline std::string absoluteFilePath(const std::string & filename) {
            bool delete_after = false;
            if( access( filename.c_str(), F_OK ) == -1 ) {
                FILE *file;
                file = fopen(filename.c_str(), "w");
                fclose(file);
                delete_after = true;
            }

            char *real_path = realpath(filename.c_str(), NULL);
            std::string absf = real_path;
            free(real_path);

            if(delete_after)
                remove(filename.c_str());

            return absf;
        }


        inline bool checkFFTEfficient(int n) {
            if(n==1)
                return true;
            else {
                if(n % 2 == 0)
                    return checkFFTEfficient(n / 2);
                if(n % 3 == 0)
                    return checkFFTEfficient(n / 3);
                if(n % 5 == 0)
                    return checkFFTEfficient(n / 5);
                if(n % 7 == 0)
                    return checkFFTEfficient(n / 7);
            }
            return false;
        }

        inline int getFFTEfficientSize(int N) {
            while(!checkFFTEfficient(N)) {
                N++;
            }
            return N;
        }

        inline double unwrap(double x, void *p) {
            auto fp = static_cast<std::function<double(double)> *>(p);
            return (*fp)(x);
        }

        /*!
        * Check if two vectors are equal within a given tolerance. Also their size has to be equal to return true.
        */
        template <class T>
        bool is_almost_equal(std::vector<T> a, std::vector<T> b, T tolerance = 0.001) {
            if (a.size() != b.size()) return false;
            for (int i = 0; i < a.size(); i++){
                if (std::abs(a[i] - b[i]) > tolerance) return false;
            }

            return true;
        }
        /*!
          * Set defoci and defocus weigths for defocus series for simulations of chromatic aberration.
          */
        inline void set_defoci_and_defocus_weights(
                double delta_defocus_max,
                double mean,
                unsigned int num_defoci,
                float fwhm_defocus_distribution,
                std::vector<float> & defoci,
                std::vector<float> & defocus_weights) {
            if(num_defoci == 1) {
                defoci.push_back(mean);
                defocus_weights.push_back(1.0);
            } else {
                float sigma = fwhm_defocus_distribution / (2 * sqrt(2 * log(2)));
                float running_sum = 0;

                for(float d = mean - delta_defocus_max;
                    d <= mean + delta_defocus_max;
                    d += 2 * delta_defocus_max / (num_defoci - 1)) {

                    float val = (float) exp(-pow(d, 2) / (2 * pow(sigma, 2)));

                    running_sum += val;

                    defocus_weights.push_back(val);
                    defoci.push_back(d);
                }

                for(auto &d: defocus_weights)
                    d /= running_sum;
            }

        }

    inline void test(){

    }

    }

}

#endif // ALGORITHMS_HPP_