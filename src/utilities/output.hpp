/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

// This namespace contains only static methods to print out stuff to stdout.

#ifndef OUTPUT_HPP_
#define OUTPUT_HPP_

#include <cstdio>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <set>
#include <map>
#include <iomanip>

#include <chrono>
#include <execinfo.h>
#include <unistd.h>

#ifdef __GNUG__
#if __GNUG__ > 6
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
#endif
#endif
#include "../3rdparty/tinyformat/tinyformat.h"
#ifdef __GNUG__
#if __GNUG__ > 6
#pragma GCC diagnostic pop
#endif
#endif

#include "mpi.hpp"
#include "../classes/Params.hpp"

/*! @file output.hpp */


namespace stemsalabim {
    /*!
     * Utility functions and classes command line output.
     */
    namespace output {

        inline std::string currentTimeString() {
            auto now = std::chrono::system_clock::now();
            auto in_time_t = std::chrono::system_clock::to_time_t(now);

            std::stringstream ss;
            ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
            return ss.str();
        }

        inline std::string currentTimeStringWithMillies() {

            using namespace std::chrono;

            system_clock::time_point now = system_clock::now();
            system_clock::duration tp = now.time_since_epoch();

            tp -= duration_cast<seconds>(tp);
            time_t tt = system_clock::to_time_t(now);
            tm t = *localtime(&tt);

            return tfm::format("%04u-%02u-%02u %02u:%02u:%02u.%03u", t.tm_year + 1900,
                        t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec,
                        static_cast<unsigned>(tp / milliseconds(1)));

        }

        inline void print(const std::string &str) {
            std::cout << currentTimeStringWithMillies() << " " << str;
        }

        template<typename... ParamTypes>
        void print(const std::string &str_format, const ParamTypes &... parameters) {
            std::string fmt = currentTimeStringWithMillies() + " " + str_format;
            tfm::format(std::cout, fmt.c_str(), parameters...);
        }

        template<typename... ParamTypes>
        void nakedprint(const std::string &str_format, const ParamTypes &... parameters) {
            tfm::format(std::cout, str_format.c_str(), parameters...);
        }

        template<typename... ParamTypes>
        std::string fmt(const std::string &str_format, ParamTypes... parameters) {
            return tfm::format(str_format.c_str(), parameters...);
        }

        inline void error(const std::string &str) {
            std::cerr << " -- Error --\n";
            std::cerr << str << "\n";
            exit(1);
        }

        template<typename... ParamTypes>
        void error(const std::string &str_format, ParamTypes... parameters) {
            std::cerr << " -- Error --\n";
            tfm::format(std::cerr, str_format.c_str(), parameters...);
            exit(1);
        }

        inline void stderr(const std::string &str) {
            std::cerr << currentTimeStringWithMillies() << " " << str << "\n";
        }

        template<typename... ParamTypes>
        void stderr(const std::string &str_format, ParamTypes... parameters) {
            std::string fmt = currentTimeStringWithMillies() + " " + str_format;
            tfm::format(std::cerr, fmt.c_str(), parameters...);
        }

        inline std::string humantime(std::chrono::microseconds us) {
            using namespace std;
            using namespace std::chrono;

            std::ostringstream os;

            typedef duration<int, ratio<86400>> days;

            char fill = os.fill();
            os.fill('0');
            auto d = duration_cast<days>(us);
            us -= d;
            auto h = duration_cast<hours>(us);
            us -= h;
            auto m = duration_cast<minutes>(us);
            us -= m;
            auto s = duration_cast<seconds>(us);
            us -= s;
            auto ms = duration_cast<milliseconds>(us);
            us -= ms;

            // we display durations only how it makes sense, i.e., for a duration of ms it is
            // unnecessary to display days, hours etc.

            // more than 1 day, display days.
            if(d.count())
                os << setw(2) << d.count() << "d:";

            // more than 1 day or more than one hour, display hours.
            if(d.count() || h.count())
                os << setw(2) << h.count() << "h:";

            // more than 1 day or more than one hour or more than 1 minute, display minutes
            if(d.count() || h.count() || m.count())
                os << setw(2) << m.count() << "m";

            // from now on, only display anything when duration is less than 1d
            if(!d.count()) {

                // display colon if less than one day, but something has been printed already.
                if(os.tellp())
                    os << ":";

                // display seconds
                if(os.tellp() || s.count())
                    os << setw(2) << s.count() << "s";

                if(!h.count() && !m.count()) {

                    if(os.tellp())
                        os << ":";

                    if(os.tellp() || ms.count())
                        os << setw(3) << ms.count() << "ms";

                    if(!s.count()) {
                        if(os.tellp())
                            os << ":";

                        if(os.tellp() || us.count())
                            os << setw(3) << us.count() << "us";

                    }
                }
            }

            os.fill(fill);

            return os.str();
        }


    }
}
#endif // OUTPUT_HPP_