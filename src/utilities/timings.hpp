/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_TIMINGS_HPP
#define STEMSALABIM_TIMINGS_HPP

#include <memory>
#include <vector>
#include <iostream>
#include <chrono>
#include <map>

/** @file */

namespace stemsalabim { namespace timings {

    class Timer {
    public:
        Timer()
                : _created(std::chrono::high_resolution_clock::now()) {

        };

        void lap() {

        }

    private:
        std::chrono::high_resolution_clock::time_point _created{};
    };

    /*!
     * Singleton implementation of the Timings class.
     */
    class Timings {
    public:

        /*!
         * Return an instance of the class. Objects can only be created here
         */
        static Timings &getInstance() {
            static Timings instance;
            return instance;
        }

    private:
        Timings() = default;

        std::map<std::string, Timer> _timers;

    public:
        Timings(Timings const &) = delete;

        void operator=(Timings const &)  = delete;
    };
}}

#endif //STEMSALABIM_TIMINGS_HPP
