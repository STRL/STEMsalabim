/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_NETCDF_UTILS_HPP
#define STEMSALABIM_NETCDF_UTILS_HPP

#include <utility>
#include <cstring>
#include <netcdf.h>
#include <assert.h>
#include <algorithm>
#include <numeric>

/** @file */

namespace stemsalabim {

    class NcException : public std::exception {
    public:
        NcException(const char *message, const char *location, int line) {
            std::string loc = std::string(location);
            std::string m = std::string(message);
            msg = std::string(loc + ":" + std::to_string(line) + " " + m);
        }

        virtual const char *what() const throw() {
            return msg.c_str();
        }

    private:
        std::string msg{""};
    };

    typedef std::initializer_list<int> dl;
    typedef std::vector<size_t> vs;

    /// Check nc call and throw exception on error
    inline void chk_nc(const char *location, int line, int retCode) {
        if(retCode == NC_NOERR)
            return;

        const char *msg = nullptr;
        if(NC_ISSYSERR(retCode)) {
            msg = strerror(retCode);
            msg = msg ? msg : "NetCDF: Unknown system error";
        } else {
            msg = nc_strerror(retCode);
        }

        throw NcException(msg, location, line);
    }

#define CHK(code) chk_nc(__FILE__, __LINE__, code)


    /// Translate C++ primitive type into corresponding MPI type
    template<typename T>
    class nc_type_def {};

#define NC_MAKETYPE(_nctype_, _cxxtype_)      \
        template <>                                            \
        class nc_type_def<_cxxtype_> {                            \
            public:                                            \
                typedef _cxxtype_ value_type;                  \
                operator nc_type () { return _nctype_; } \
        }

    NC_MAKETYPE(NC_CHAR, char);

    NC_MAKETYPE(NC_STRING, char *);

    NC_MAKETYPE(NC_SHORT, signed short int);

    NC_MAKETYPE(NC_INT, signed int);

    NC_MAKETYPE(NC_LONG, signed long int);

    NC_MAKETYPE(NC_BYTE, signed char);

    NC_MAKETYPE(NC_UINT, unsigned int);

    NC_MAKETYPE(NC_FLOAT, float);

    NC_MAKETYPE(NC_DOUBLE, double);

    //NC_MAKETYPE(NC_BOOL,bool);

#undef NC_MAKETYPE
    // @formatter:on

    /**
     * A simple wrapper around the NetCDF library.
     * Uses singleton pattern again.
     */

    class NCVar {
    public:
        NCVar(int id, int parent, std::string name, int type)
                : _id(id)
                , _parentid(parent)
                , _name(std::move(name))
                , _type(type) {}

        NCVar() = default;

        void att(const std::string &name, const char *val) {
            CHK(nc_put_att_text(_parentid, _id, name.c_str(), strlen(val), val));
        }

        void att(const std::string &name, const std::string &val) {
            CHK(nc_put_att_text(_parentid, _id, name.c_str(), val.length(), val.c_str()));
        }

        void att(const std::string &name, bool val) {
            unsigned short v = val ? (unsigned short) 1 : (unsigned short) 0;
            CHK(nc_put_att_ushort(_parentid, _id, name.c_str(), NC_USHORT, 1, &v));
        }

        template<typename T>
        void att(const std::string &name, const T &val) {
            CHK(nc_put_att(_parentid, _id, name.c_str(), nc_type_def<T>(), 1, &val));
        }

        template<typename T>
        void att(const std::string &name, const std::vector<T> &val) {
            CHK(nc_put_att(_parentid, _id, name.c_str(), nc_type_def<T>(), val.size(), val.data()));
        }

        std::string att(const std::string &name) {
            char buffer[20000];
            CHK(nc_get_att_text(_parentid, _id, name.c_str(), buffer));
            return std::string(buffer);
        }

        bool is(const std::string &name) {
            return att<unsigned short>(name) != 0;
        }

        template<typename T>
        T att(const std::string &name) {
            T val;
            CHK(nc_get_att(_parentid, _id, name.c_str(), &val));
            return val;
        }

        template<typename T>
        std::vector<T> attList(const std::string &name) {
            std::vector<T> vals;
            size_t len;
            CHK(nc_inq_attlen(_parentid, _id, name.c_str(), &len));
            vals.resize(len);
            CHK(nc_get_att(_parentid, _id, name.c_str(), vals.data()));
            return vals;
        }

        template<typename T>
        std::vector<T> get() {
            std::vector<T> dat;
            auto l = len();
            auto data_len = std::accumulate(l.begin(), l.end(), 1, std::multiplies<>());
            dat.resize(data_len);
            CHK(nc_get_vara(_parentid, _id, std::vector<size_t>(l.size(), 0).data(), l.data(), dat.data()));
            return dat;
        }

        template<typename T>
        std::vector<T> get(const std::vector<size_t> &index, const std::vector<size_t> &count) {
            std::vector<T> dat;
            auto data_len = std::accumulate(count.begin(), count.end(), 1, std::multiplies<>());
            dat.resize(data_len);
            CHK(nc_get_vara(_parentid, _id, index.data(), count.data(), dat.data()));
            return dat;
        }

        template<typename T>
        void get(const std::vector<size_t> &index, const std::vector<size_t> &count, std::vector<T> &vals) {
            CHK(nc_get_vara(_parentid, _id, index.data(), count.data(), vals.data()));
        }

        template<typename T>
        void get(const std::vector<size_t> &index, const std::vector<size_t> &count, T *vals) {
            CHK(nc_get_vara(_parentid, _id, index.data(), count.data(), vals));
        }


        template<typename T>
        void put(const std::vector<T> &vals) {
            auto l = len();
            assert(nc_type_def<T>() == _type);
            CHK(nc_put_var(_parentid, _id, vals.data()));
        }

        template<typename T>
        void put(const T *vals) {
            auto l = len();
            assert(nc_type_def<T>() == _type);
            CHK(nc_put_var(_parentid, _id, vals));
        }

        template<typename T>
        void put(const void *vals) {
            auto l = len();
            assert(nc_type_def<T>() == _type);
            CHK(nc_put_var(_parentid, _id, vals));
        }

        template<typename T>
        void put(const std::vector<size_t> &index, const std::vector<size_t> &count, const std::vector<T> &vals) {
            nc_type_def<T>();
            CHK(nc_put_vara(_parentid, _id, index.data(), count.data(), vals.data()));
        }

        template<typename T>
        void put(const std::vector<size_t> &index, const std::vector<size_t> &count, const T *vals) {
            nc_type_def<T>();
            CHK(nc_put_vara(_parentid, _id, index.data(), count.data(), vals));
        }

        void chunking(const std::vector<size_t> &chunks) {
            auto l = len();
            assert(l.size() == chunks.size());
            CHK(nc_def_var_chunking(_parentid, _id, NC_CHUNKED, chunks.data()));
        }

        void deflate(int deflate_level) {
            CHK(nc_def_var_deflate(_parentid, _id, NC_NOSHUFFLE, 1, deflate_level));
        }

        std::vector<size_t> len() {
            int ndims;
            CHK(nc_inq_varndims(_parentid, _id, &ndims));
            std::vector<int> dimids(ndims);
            std::vector<size_t> lengths(ndims);
            CHK(nc_inq_vardimid(_parentid, _id, dimids.data()));
            for(int i = 0; i < ndims; ++i) {
                CHK(nc_inq_dimlen(_parentid, dimids[i], &lengths[i]));
            }
            return lengths;
        }
    private:
        int _id;
        int _parentid;
        std::string _name;
        nc_type _type;

    };

    class NCDim {
    public:
        NCDim(int id, int parent, std::string name, size_t size)
                : _id(id)
                , _parentid(parent)
                , _name(std::move(name))
                , _size(size) {}

        int id() const {
            return _id;
        }

    private:
        int _id;
        int _parentid;
        std::string _name;
        size_t _size;
    };

    class NCGroup {
    public:
        NCGroup(int id, int parent, std::string name)
                : _id(id)
                , _parentid(parent)
                , _name(std::move(name)) {}

        NCGroup defineGroup(const std::string &name) {
            int id;
            CHK(nc_def_grp(_id, name.c_str(), &id));
            return NCGroup(id, _id, name);
        }

        NCGroup group(const std::string &name) {
            int id;
            CHK(nc_inq_grp_ncid(_id, name.c_str(), &id));
            return NCGroup(id, _id, name);
        }

        NCDim defineDim(const std::string &name, size_t size = NC_UNLIMITED) {
            int id;
            CHK(nc_def_dim(_id, name.c_str(), size, &id));
            return NCDim(id, _id, name, size);
        }

        NCDim dim(const std::string &name) {
            int id;
            size_t len;
            CHK(nc_inq_dimid(_id, name.c_str(), &id));
            CHK(nc_inq_dimlen(_id, id, &len));
            return NCDim(id, _id, name, len);
        }

        size_t getDimensionLength(const std::string &name) {
            int id;
            size_t len;
            CHK(nc_inq_dimid(_id, name.c_str(), &id));
            CHK(nc_inq_dimlen(_id, id, &len));
            return len;
        }

        template<typename T>
        NCVar defineVar(const std::string &name, const std::vector<NCDim> &dims) {
            int id;
            std::vector<int> dimids(dims.size());
            std::transform(dims.begin(), dims.end(), dimids.begin(), [](const NCDim &d) -> int {
                return d.id();
            });
            CHK(nc_def_var(_id, name.c_str(), nc_type_def<T>(), (int) dims.size(), dimids.data(), &id));
            return NCVar(id, _id, name, nc_type_def<T>());
        }

        NCVar var(const std::string &name) {
            int id;
            nc_type type;
            CHK(nc_inq_varid(_id, name.c_str(), &id));
            CHK(nc_inq_vartype(_id, id, &type));
            return NCVar(id, _id, name, type);
        }

        int id() const {
            return _id;
        }

        void att(const std::string &name, const char *val) {
            CHK(nc_put_att_text(_id, NC_GLOBAL, name.c_str(), strlen(val), val));
        }

        void att(const std::string &name, const std::string &val) {
            CHK(nc_put_att_text(_id, NC_GLOBAL, name.c_str(), val.length(), val.c_str()));
        }

        void att(const std::string &name, bool val) {
            unsigned short v = val ? (unsigned short) 1 : (unsigned short) 0;
            CHK(nc_put_att_ushort(_id, NC_GLOBAL, name.c_str(), NC_USHORT, 1, &v));
        }

        template<typename T>
        void att(const std::string &name, const T &val) {
            CHK(nc_put_att(_id, NC_GLOBAL, name.c_str(), nc_type_def<T>(), 1, &val));
        }

        template<typename T>
        void att(const std::string &name, const std::vector<T> &val) {
            CHK(nc_put_att(_id, NC_GLOBAL, name.c_str(), nc_type_def<T>(), val.size(), val.data()));
        }


        std::string att(const std::string &name) {
            size_t t_len;
            nc_inq_attlen (_id, NC_GLOBAL, name.c_str(), &t_len);

            char *buffer;
            buffer = (char *) malloc(t_len + 1);

            CHK(nc_get_att_text(_id, NC_GLOBAL, name.c_str(), buffer));

            buffer[t_len] = '\0';

            return std::string(buffer);
        }

        bool is(const std::string &name) {
            return att<unsigned short>(name) != 0;
        }

        template<typename T>
        T att(const std::string &name) {
            T val;
            CHK(nc_get_att(_id, NC_GLOBAL, name.c_str(), &val));
            return val;
        }

        template<typename T>
        std::vector<T> attList(const std::string &name) {
            std::vector<T> vals;
            size_t len;
            CHK(nc_inq_attlen(_id, NC_GLOBAL, name.c_str(), &len));
            vals.resize(len);
            CHK(nc_get_att(_id, NC_GLOBAL, name.c_str(), vals.data()));
            return vals;
        }

    protected:
        int _id;
        int _parentid;
        std::string _name;

        NCGroup()
                : _name("root") {}
    };

    class NCFile : public NCGroup {
    public:

        NCFile(const std::string & filename, bool create = false, bool read_only=false) {
            if(create)
                nc_create(filename.c_str(), NC_NETCDF4 | NC_CLOBBER, &_id);
            else if(read_only)
                nc_open(filename.c_str(), NC_NOWRITE, &_id);
            else
                nc_open(filename.c_str(), NC_WRITE, &_id);
            _name = "root";
        }

        ~NCFile() {
            nc_close(_id);
        }

    };


    typedef std::vector<NCDim> vd;

}

#endif //STEMSALABIM_NETCDF_UTILS_HPP
