/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_MPI_HPP
#define STEMSALABIM_MPI_HPP

#include <mpi.h>
#include <memory>
#include <vector>
#include <iostream>
#include "algorithms.hpp"

/** @file */

namespace stemsalabim {

    /*!
     * Utility functions and classes for MPI communication.
     */
    namespace mpi {
        /*!
         * Wrapper for getting the correct MPI types.
         */
        namespace detail {
            // @formatter:off

        /// Translate C++ primitive type into corresponding MPI type
        template <typename T> class mpi_type {};

        #define MPI_DETAIL_MAKETYPE(_mpitype_, _cxxtype_)      \
        template <>                                            \
        class mpi_type<_cxxtype_> {                            \
            public:                                            \
                typedef _cxxtype_ value_type;                  \
                operator MPI_Datatype () { return _mpitype_; } \
        }

        MPI_DETAIL_MAKETYPE(MPI_CHAR,char);
        MPI_DETAIL_MAKETYPE(MPI_SHORT,signed short int);
        MPI_DETAIL_MAKETYPE(MPI_INT,signed int);
        MPI_DETAIL_MAKETYPE(MPI_LONG,signed long int);
        MPI_DETAIL_MAKETYPE(MPI_LONG_LONG,signed long long int);
        MPI_DETAIL_MAKETYPE(MPI_SIGNED_CHAR,signed char);
        MPI_DETAIL_MAKETYPE(MPI_UNSIGNED_CHAR,unsigned char);
        MPI_DETAIL_MAKETYPE(MPI_UNSIGNED_SHORT,unsigned short int);
        MPI_DETAIL_MAKETYPE(MPI_UNSIGNED,unsigned int);
        MPI_DETAIL_MAKETYPE(MPI_UNSIGNED_LONG,unsigned long int);
        MPI_DETAIL_MAKETYPE(MPI_UNSIGNED_LONG_LONG,unsigned long long int);
        MPI_DETAIL_MAKETYPE(MPI_FLOAT,float);
        MPI_DETAIL_MAKETYPE(MPI_DOUBLE,double);
        MPI_DETAIL_MAKETYPE(MPI_LONG_DOUBLE,long double);
        MPI_DETAIL_MAKETYPE(MPI_CXX_BOOL,bool);

        #undef MPI_DETAIL_MAKETYPE
        // @formatter:on
        }

        /*!
         * Class to wrap MPI_Status
         */
        class Status {
        public:
            friend class Environment;

            Status() {
                _mpi_status = std::make_shared<MPI_Status>();
            }

            /*!
             * Return the message size
             */
            template<typename T>
            size_t count(const std::vector<T> & val) const {
                MPI_Datatype dt = detail::mpi_type<T>();
                return count(dt);
            }

            size_t count(const std::string & val) const {
                algorithms::ignore(val);
                MPI_Datatype dt = detail::mpi_type<char>();
                return count(dt);
            }

            template<typename T>
            size_t count(const T & val) const {
                MPI_Datatype dt = detail::mpi_type<T>();
                return count(dt);
            }

            /*!
             * Return source
             */
            int source() const {
                return _mpi_status->MPI_SOURCE;
            }

            /*!
             * Return the message tag
             */
            int tag() const {
                return _mpi_status->MPI_TAG;
            }

        private:
            /// The actual MPI_Status object is saved in a pointer.
            std::shared_ptr<MPI_Status> _mpi_status;

            size_t count(MPI_Datatype t) const {
                int mpi_message_size;
                MPI_Get_count(_mpi_status.get(), t, &mpi_message_size);
                return (size_t)mpi_message_size;
            }
        };

        /*!
         * Class to wrap MPI_Request
         */
        class Request {
        public:
            friend class Environment;

            Request(MPI_Datatype type)
                    : _mpi_type(type) {
                _request = std::make_shared<MPI_Request>();
            }

            Request() {
                _request = std::make_shared<MPI_Request>();
            }

            Request& operator=(Request other) {
                _request = other._request;
                _mpi_type = other._mpi_type;
                _valid = other._valid;
                _flag = other._flag;
                return *this;
            }

            /*!
             * Test whether the request is valid already. This function is destructive,
             * as soon as the request becomes valid, this shouldn't be called again.
             */
            bool test() {
                if(!_valid)
                    return false;

                MPI_Test(_request.get(), &_flag, MPI_STATUS_IGNORE);

                if(_flag != 0)
                    _valid = false;
                return _flag != 0;
            }

            /*!
             * Cancel the request.
             */
            void cancel() {
                if(!_valid)
                    return;

                MPI_Cancel(_request.get());
            }

            /*!
             * Set the request valid or invalid.
             */
            void valid(bool v) {
                _valid = v;
            }

        private:
            /// The actual MPI_Status object is saved in a pointer.
            std::shared_ptr<MPI_Request> _request;

            /// This boolean indicates, whether the request is created
            /// by an MPI action.
            bool _valid{false};

            /// Flag that becomes true when the Request becomes valid
            int _flag{0};

            /// The MPI data type
            MPI_Datatype _mpi_type;
        };

        /*!
         * Singleton implementation of the MPI environment. The MPI_Init and MPI_Finalize is
         * taken care of automatically.
         */
        class Environment {
        public:
            const static int MASTER = 0;
            const static int ANY_SOURCE = MPI_ANY_SOURCE;
            const static int ANY_TAG = MPI_ANY_TAG;

            const static int MPI_CANCEL = 3;
            const static int MPI_WORK = 1;
            const static int MPI_RESULT = 2;
            const static int MPI_TAG_TASKS_REQUEST = 11;
            const static int MPI_TAG_TASKS = 12;
            const static int MPI_TAG_RESULTS_REQUEST = 13;
            const static int MPI_TAG_RESULTS = 14;


            /*!
             * Return an instance of the mpi::Environment class. Objects should only
             * be created here.
             */
            static Environment &getInstance() {
                static Environment instance;
                return instance;
            }

            /*!
             * Wait for all processors to reach this point.
             */
            void barrier() {
                MPI_Barrier(MPI_COMM_WORLD);
            }

            /*!
             * Check, if MPI is initialized
             */
            static bool initialized() {
                int ini;
                MPI_Initialized(&ini);
                return ini != 0;
            }

            /*!
             * Check, if MPI is finalized
             */
            static bool finalized() {
                int fin;
                MPI_Finalized(&fin);
                return fin != 0;
            }

            /*!
             * Try to find out, whether MPI parallelization is enabled by checking
             * whether the number of processors is larger than 1.
             */
            bool isMpi() const {
                int world_size;
                MPI_Comm_size(MPI_COMM_WORLD, &world_size);
                return world_size > 1;
            }

            /*!
             * Check if this process is MPI master, i.e., rank 0.
             */
            bool isMaster() const {
                int world_rank;
                MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
                return world_rank == MASTER;
            }

            /*!
             * Check if this process is MPI slave, i.e., rank > 0.
             */
            bool isSlave() const {
                int world_rank;
                MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
                return world_rank != MASTER;
            }

            /*!
             * Return the name of the current node, i.e., hostname of the
             * MPI processor.
             */
            std::string name() const {
                int name_len;
                char processor_name[MPI_MAX_PROCESSOR_NAME];
                MPI_Get_processor_name(processor_name, &name_len);
                return std::string(processor_name);
            }

            /*!
             * Get the rank of the current MPI process
             */
            int rank() const {
                int myrank;
                MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
                return myrank;
            }

            /*!
             * Get the size of the MPI world communicator.
             */
            int size() const {
                int sz;
                MPI_Comm_size(MPI_COMM_WORLD, &sz);
                return sz;
            }

            /*!
             * Probe for a message from source with tag tag.
             */
            Status probe(int source, int tag) {
                Status s;
                MPI_Probe(source, tag, MPI_COMM_WORLD, s._mpi_status.get());
                return s;
            }

            /*!
             * Send an empty message with a tag to some destination
             */
            void send(int destination, int tag) {
                int dummy = 0;
                send(&dummy, 0, destination, tag);
            }

            /*!
             * Send a single value of some supported type to another processor.
             */
            template<typename T>
            void send(T &val, int destination, int tag) {
                send(&val, 1, destination, tag);
            }

            /*!
             * Send a single string to another processor.
             */
            void send(std::string &val, int destination, int tag) {
                send(const_cast<char *>(val.data()), val.size(), destination, tag);
            }

            /*!
             * Send a vector of some supported type to another processor.
             */
            template<typename T>
            void send(std::vector<T> &data, int destination, int tag) {
                send(data.data(), data.size(), destination, tag);
            }

            /*!
             * Send a vector of some supported type to another processor.
             */
            template<typename T>
            void send(std::vector<T> &data, size_t count, int destination, int tag) {
                send(data.data(), count, destination, tag);
            }

            template<typename T>
            Request isend(T &val, int destination, int tag) {
                return isend(&val, 1, destination, tag);
            }

            template<typename T>
            Request isend(std::vector<T> &data, int destination, int tag) {
                return isend(data.data(), data.size(), destination, tag);
            }

            Request isend(int destination, int tag) {
                int dummy;
                return isend(&dummy, 0, destination, tag);
            }

            /*!
             * non-blocking receive of a single value.
             */
            template<typename T>
            Request irecv(T &val, int source, int tag) {
                MPI_Datatype dt = detail::mpi_type<T>();
                Request req(dt);
                req.valid(true);
                MPI_Irecv(&val, 1, dt, source, tag, MPI_COMM_WORLD, req._request.get());
                return req;
            }

            /*!
             * Non-blocking probe for a message from source with tag tag.
             */
            bool iprobe(int source, int tag, Status &s) {
                int flag;
                MPI_Iprobe(source, tag, MPI_COMM_WORLD, &flag, s._mpi_status.get());
                return flag != 0;
            }

            bool iprobe(Status &s) {
                int flag;
                MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, s._mpi_status.get());
                return flag != 0;
            }

            /*!
             * non-blocking receive of a vector of values.
             */
            template<typename T>
            Request irecv(std::vector<T> &data, std::size_t max_count, int source, int tag) {
                if(data.size() != max_count)
                    data.resize(max_count);

                MPI_Datatype dt = detail::mpi_type<T>();
                Request req(dt);
                req.valid(true);
                MPI_Irecv(data.data(),
                          (int) max_count,
                          dt,
                          source,
                          tag,
                          MPI_COMM_WORLD,
                          req._request.get());
                return req;
            }

            /*!
             * non-blocking receive of a vector of values. The number of expected elements is
             * set to the size of the vector. (Use .resize() beforehand!)
             */
            template<typename T>
            Request irecv(std::vector<T> &data, int source, int tag) {
                return irecv(data, data.size(), source, tag);
            }

            /*!
             * blocking receive of a single value.
             */
            template<typename T>
            Status recv(T &val, int source, int tag) {
                Status status;
                MPI_Datatype dt = detail::mpi_type<T>();
                MPI_Recv(&val, 1, dt, source, tag, MPI_COMM_WORLD, status._mpi_status.get());
                return status;
            }

            /*!
             * blocking receive of an empty message, i.e., tag only.
             */
            Status recv(int source, int tag) {
                int dummy = 0;
                return recv(dummy, 0, source, tag);
            }

            /*!
             * blocking receive of a string.
             */
            Status recv(std::string &val, int source, int tag) {
                int count;

                Status status;
                MPI_Probe(source, tag, MPI_COMM_WORLD, status._mpi_status.get());
                MPI_Get_count(status._mpi_status.get(), detail::mpi_type<char>(), &count);

                char *buf = new char[count];
                MPI_Recv(buf, count, detail::mpi_type<char>(), source, tag, MPI_COMM_WORLD, status._mpi_status.get());
                val.assign(buf, (unsigned long) count);
                delete[] buf;

                return status;
            }

            /*!
             * blocking receive of a vector of values. The number of expected elements is
             * determined automatically.
             */
            template<typename T>
            Status recv(std::vector<T> &data, int source, int tag) {
                int count;

                MPI_Datatype dt = detail::mpi_type<T>();
                Status status;
                MPI_Probe(source, tag, MPI_COMM_WORLD, status._mpi_status.get());
                MPI_Get_count(status._mpi_status.get(), dt, &count);

                if((int) data.size() != count)
                    data.resize(count);

                MPI_Recv(data.data(),
                         count,
                         dt,
                         source,
                         tag,
                         MPI_COMM_WORLD,
                         status._mpi_status.get());

                return status;
            }

            /*!
             * broadcast values to all processors.
             */
            template<typename T>
            void broadcast(T *vals, std::size_t count, int root) {
                MPI_Datatype dt = detail::mpi_type<T>();
                MPI_Bcast(vals, (int) count, dt, root, MPI_COMM_WORLD);
            }

            /*!
             * broadcast single value to all processors.
             */
            template<typename T>
            void broadcast(T &val, int root) {
                broadcast(&val, 1, root);
            }

            /*!
             * broadcast string to all processors.
             */
            void broadcast(std::string &val, int root) {
                std::size_t root_sz = val.length();
                broadcast(root_sz, root);
                if(rank() == root) {
                    broadcast(const_cast<char *>(val.c_str()), root_sz, root);
                } else {
                    char *buf = new char[root_sz];
                    broadcast(buf, root_sz, root);
                    val.assign(buf, root_sz);
                    delete[] buf;
                }
            }

            /*!
             * broadcast a vector of values to all processors.
             */
            template<typename T>
            void broadcast(std::vector<T> &data, int root) {
                typedef std::vector<T> data_type;
                typedef typename data_type::size_type size_type;
                size_type root_sz = data.size();
                broadcast(root_sz, root);
                data.resize(root_sz);
                broadcast(data.data(), root_sz, root);
            }

        private:
            /*!
             * MPI initialization.
             */
            Environment()
                    : _initialized(false) {
                if(!initialized()) {
                    MPI_Init(NULL, NULL);
                    _initialized = true;
                }
            }

            /*!
             * MPI teardown
             */
            ~Environment() {
                if(!_initialized)
                    return;
                if(finalized())
                    return;
                MPI_Finalize();
            }

            /*!
             * Private, because we should always use STL containers such as vector, array, for which
             * there is another overload provided by the class.
             */
            template<typename T>
            void send(T *vals, std::size_t count, int destination, int tag) {
                MPI_Datatype dt = detail::mpi_type<T>();
                MPI_Send(vals, (int) count, dt, destination, tag, MPI_COMM_WORLD);
            }

            template<typename T>
            Request isend(T *vals, std::size_t count, int destination, int tag) {
                MPI_Datatype dt = detail::mpi_type<T>();
                Request req(dt);
                req.valid(true);
                MPI_Isend(vals, (int) count, dt, destination, tag, MPI_COMM_WORLD, req._request.get());
                return req;
            }

            /*!
             * non-blocking receive of a vector of values with fixed number of elements
             */
            template<typename T>
            Status recv(T &val, int count, int source, int tag) {
                Status status;
                MPI_Datatype dt = detail::mpi_type<T>();
                MPI_Recv(&val, count, dt, source, tag, MPI_COMM_WORLD, status._mpi_status.get());
                return status;
            }

            bool _initialized;

        public:
            Environment(Environment const &) = delete;

            void operator=(Environment const &)  = delete;
        };
    }
}

#endif //STEMSALABIM_MPI_HPP
