/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_MEMORY_HPP
#define STEMSALABIM_MEMORY_HPP

/** @file */

#include <list>
#include <vector>
#include <memory>
#include <iostream>
#include <mutex>


namespace stemsalabim {
    /*!
     * Utility classes and functions that help preventing memory fragmentation by providing buffers
     * and managing continous storage.
     */
    namespace memory {
        /*!
         * memory buffer
         */
        namespace buffer {

            /*!
             * A small wrapper around iterators for range-based for loop.
             */
            template<class Iter>
            class range {
            public:
                /*!
                 * Init constructor
                 */
                range(Iter b, Iter e)
                        : b(b)
                        , e(e) {}

                // required for range-based for loops
                Iter begin() { return b; }

                Iter end() { return e; }

                Iter begin() const { return b; }

                Iter end() const { return e; }

                /*!
                 * Number of elements in this range
                 */
                long size() const { return std::distance(begin(), end()); }

                /*!
                 * Calculates the size in bytes of the whole range of objects
                 * that this range holds.
                 */
                long byte_size() const { return sizeof(*begin()) * size(); }

            private:
                /// begin iterator
                Iter b;

                /// end itertor
                Iter e;
            };

            template<typename prec_t>
            class number_buffer;

            /*!
             * The type of an entry of the memory buffer. Holds start and end offsets as well
             * as the index of the chunk where the associated data is stored.
             */
            template<typename prec_t>
            class entry {
            public:
                friend class number_buffer<prec_t>;

                entry(size_t chunk, size_t start, size_t end)
                        : _chunk(chunk)
                        , _start(start)
                        , _end(end) {}

                entry() = default;

                bool operator==(const entry<prec_t> &rhs) const {
                    return _start == rhs._start && _end == rhs._end && _chunk == rhs._chunk;
                }

                bool operator!=(const entry<prec_t> &rhs) const {
                    return !(rhs == *this);
                }

                size_t size() const { return _end - _start; }

                size_t byte_size() const {
                    return size() * sizeof(prec_t);
                }

            private:
                size_t _chunk{0};
                size_t _start{0};
                size_t _end{0};
            };


            /*!
             * A memory buffer class to store and release same-sized arrays of numeric data.
             * The memory is continous in chunks that are multiples of the stored arrays.
             * Each array is guaranteed to be at the same position in memory until it is removed
             * from the buffer. After removal, free entries are re-used for newly added arrays.
             */
            template<typename prec_t>
            class number_buffer {
            public:
                friend class entry<prec_t>;

                /*!
                 * Construct the buffer and allocate the first chunk.
                 */
                number_buffer(size_t entry_length, size_t chunk_size)
                        : _entry_length(entry_length)
                        , _chunk_size(chunk_size) {
                    resize(1);
                }

                /*!
                 * Add more chunks to the buffer
                 */
                void resize(size_t num_chunks) {
                    std::unique_lock<std::mutex> lck(_mtx);

                    while(_data.size() < num_chunks)
                        _data.emplace_back(_chunk_size * _entry_length, 0);
                }

                /*!
                 * Add an empty entry (i.e. numeric array) to the buffer.
                 */
                entry<prec_t> &add_empty() {
                    std::unique_lock<std::mutex> lck(_mtx);

                    entry<prec_t> e;
                    size_t start;
                    size_t chunk;

                    // first check, if there is an entry in the free containers array.
                    // If so, re-use that.
                    if(!_free.empty()) {
                        e = _free.back();
                        _free.pop_back();
                        start = e._start;
                        chunk = e._chunk;
                    } else {
                        // if a new entry is created and the current chunk is full,
                        // use the next free chunk. If there is none, create a new one.
                        if(_current_end >= _chunk_size * _entry_length) {
                            _current_end = 0;
                            _current_chunk++;

                            if(_current_chunk == _data.size()) {
                                _data.emplace_back(_chunk_size * _entry_length, 0);
                            }
                        }

                        start = _current_end;
                        chunk = _current_chunk;

                        _current_end += _entry_length;
                    }

                    e._start = start;
                    e._chunk = chunk;
                    e._end = start + _entry_length;

                    _contents.push_back(e);
                    return _contents.back();
                }

                /*!
                 * Add an array of numbers to the buffer.
                 */
                entry<prec_t> &add(const std::vector<prec_t> &data) {
                    if(data.size() != _entry_length)
                        throw std::invalid_argument("Entry length does not equal data length!");

                    entry<prec_t> &added = add_empty();
                    std::copy(data.begin(), data.end(), _data[added._chunk].begin() + added._start);
                    return added;
                }

                /*!
                 * Get a range<Iter> object for the data of some entry. This can be iterated over using
                 * range-based for loops.
                 */
                range<typename std::vector<prec_t>::iterator> get(const entry<prec_t> &entry) const {
                    return range<typename std::vector<prec_t>::iterator>(_data[entry._chunk].begin() + entry._start,
                                                                         _data[entry._chunk].begin() + entry._end);
                }

                /*!
                 * Get a range<Iter> object for the data of some entry. This can be iterated over using
                 * range-based for loops.
                 */
                range<typename std::vector<prec_t>::iterator> get(const entry <prec_t> &entry) {
                    return range<typename std::vector<prec_t>::iterator>(_data[entry._chunk].begin() + entry._start,
                                                                         _data[entry._chunk].begin() + entry._end);
                }

                /*!
                 * Get a pointer to the ith element of the data associated with entry.
                 */
                const prec_t *ptr(const entry<prec_t> &entry, int i) const {
                    return &_data[entry._chunk][entry._start + i];
                }

                /*!
                 * Get a pointer to the ith element of the data associated with entry.
                 */
                prec_t *ptr(const entry <prec_t> &entry, int i) {
                    return &_data[entry._chunk][entry._start + i];
                }

                /*!
                 * Get a pointer to the first element of the data associated with entry.
                 */
                const prec_t *ptr(const entry <prec_t> &entry) const {
                    return &_data[entry._chunk][entry._start];
                }

                /*!
                 * Get a pointer to the first element of the data associated with entry.
                 */
                prec_t *ptr(const entry <prec_t> &entry) {
                    return &_data[entry._chunk][entry._start];
                }

                /*!
                 * Get the value of the ith element of the data associated with entry.
                 */
                prec_t value(const entry<prec_t> &entry, int i) const {
                    return _data[entry._chunk][entry._start + i];
                }

                /*!
                 * Get the value of the ith element of the data associated with entry.
                 */
                prec_t value(const entry <prec_t> &entry, int i) {
                    return _data[entry._chunk][entry._start + i];
                }

                /*!
                 * Remove an entry from the buffer.
                 */
                void remove(entry<prec_t> &entry) {
                    std::unique_lock<std::mutex> lck(_mtx);

                    _contents.remove(entry);
                    _free.push_back(entry);
                }

                /*!
                 * Clear the buffer and release all allocated storage.
                 */
                void clear() {
                    std::unique_lock<std::mutex> lck(_mtx);

                    _contents.clear();
                    _free.clear();
                    _data.clear();
                    _current_end = 0;
                    _current_chunk = 0;
                }

            private:
                /// a mutex protecting multi-threaded access
                std::mutex _mtx;

                /// the length of the stored numeric arrays
                size_t _entry_length;

                /// current end position of the current chunk
                size_t _current_end{0};

                /// current chunk index
                size_t _current_chunk{0};

                /// number of arrays that each chunk holds
                size_t _chunk_size;

                /// list of entries contained in the buffer
                std::list<entry<prec_t>> _contents;

                /// list of free entries that can be reused.
                std::list<entry<prec_t>> _free;

                /// the data
                std::vector<std::vector<prec_t>> _data;
            };

        }
    }
}

#endif //STEMSALABIM_MEMORY_HPP
