/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_NDARRAY_HPP
#define STEMSALABIM_NDARRAY_HPP

#include <vector>
#include <tuple>

template<typename T>
class vecnd : private std::vector<T> {
public:
    vecnd(const std::initializer_list<size_t> &dims, const T &fill_value)
            : _dims(dims) {
        auto data_len = std::accumulate(dims.begin(), dims.end(), 1, std::multiplies<>());
        resize(data_len, fill_value);
    }

private:
    std::initializer_list<size_t> _dims;
}

template<typename T>
class vec2d : private std::vector<T> {
public:
    vec2d(const std::vector<size_t> &dims, const T &fill_value)
            : _d1(dims[0])
            , _d2(dims[1]) {
        assert(dims.size() == 2);
        resize(_d1 * _d2, fill_value);
    }

    vec2d(const std::vector<size_t> &dims)
            : _d1(dims[0])
            , _d2(dims[1]) {
        assert(dims.size() == 2);
        resize(_d1 * _d2);
    }

    void resize(const std::vector<size_t> &dims) {
        assert(dims.size() == 2);
        _d1 = (dims[0]);
        _d2 = (dims[1]);
        resize(_d1 * _d2);
    }

    void reserve(const std::vector<size_t> &dims) {
        assert(dims.size() == 2);
        _d1 = (dims[0]);
        _d2 = (dims[1]);
        reserve(_d1 * _d2);
    }

    T &operator()(size_t i1, size_t i2) {
        return (*this)[i1 * _d2 + i2];
    }

    const T &operator()(size_t i1, size_t i2) const {
        return (*this)[i1 * _d2 + i2];
    }

private:
    size_t _d1;
    size_t _d2;
};

template<typename T>
class vec3d : private std::vector<T> {
public:
    vec3d(const std::vector<size_t> &dims, const T &fill_value)
            : _d1(dims[0])
            , _d2(dims[1])
            , _d3(dims[2]) {
        assert(dims.size() == 3);
        resize(_d1 * _d2 * _d3, fill_value);
    }

    vec3d(const std::vector<size_t> &dims)
            : _d1(dims[0])
            , _d2(dims[1])
            , _d3(dims[2]) {
        assert(dims.size() == 3);
        resize(_d1 * _d2 * _d3);
    }

    void resize(const std::vector<size_t> &dims) {
        assert(dims.size() == 3);
        _d1 = (dims[0]);
        _d2 = (dims[1]);
        _d3 = (dims[2]);
        resize(_d1 * _d2 * _d3);
    }

    void reserve(const std::vector<size_t> &dims) {
        assert(dims.size() == 3);
        _d1 = (dims[0]);
        _d2 = (dims[1]);
        _d3 = (dims[2]);
        reserve(_d1 * _d2* _d3);
    }

    T &operator()(size_t i1, size_t i2, size_t i3) {
        return (*this)[(i1 * _d2 + i2) * _d3 + i3];
    }

    const T &operator()(size_t i1, size_t i2, size_t i3) const {
        return (*this)[(i1 * _d2 + i2) * _d3 + i3];
    }

private:
    size_t _d1;
    size_t _d2;
    size_t _d3;
};

template<typename T>
class vec4d : public std::vector<T> {
public:
    vec4d(const std::vector<size_t> &dims, const T &fill_value)
            : _d1(dims[0])
            , _d2(dims[1])
            , _d3(dims[2])
            , _d4(dims[3]) {
        assert(dims.size() == 4);
        resize(_d1 * _d2 * _d3 * _d4, fill_value);
    }

    vec4d(const std::vector<size_t> &dims)
            : _d1(dims[0])
            , _d2(dims[1])
            , _d3(dims[2])
            , _d4(dims[3]) {
        assert(dims.size() == 4);
        resize(_d1 * _d2 * _d3 * _d4);
    }

    void resize(const std::vector<size_t> &dims) {
        assert(dims.size() == 4);
        _d1 = (dims[0]);
        _d2 = (dims[1]);
        _d3 = (dims[2]);
        _d4 = (dims[3]);
        resize(_d1 * _d2 * _d3 * _d4);
    }

    void reserve(const std::vector<size_t> &dims) {
        assert(dims.size() == 3);
        _d1 = (dims[0]);
        _d2 = (dims[1]);
        _d3 = (dims[2]);
        _d4 = (dims[3]);
        reserve(_d1 * _d2* _d3* _d4);
    }

    T &operator()(size_t i1, size_t i2, size_t i3, size_t i4) {
        return (*this)[((i1 * _d2 + i2) * _d3 + i3) * _d4 + i4];
    }

    const T &operator()(size_t i1, size_t i2, size_t i3, size_t i4) const {
        return (*this)[((i1 * _d2 + i2) * _d3 + i3) * _d4 + i4];
    }

private:
    size_t _d1;
    size_t _d2;
    size_t _d3;
    size_t _d4;
};



#endif //STEMSALABIM_NDARRAY_HPP
