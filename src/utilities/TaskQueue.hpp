/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_WORKQUEUE_HPP
#define STEMSALABIM_WORKQUEUE_HPP

#include <mutex>
#include <atomic>
#include <condition_variable>
#include "../classes/GridManager.hpp"

/** @file */

namespace stemsalabim {

    /*!
     * This is a concurrent index based queue. The indices of objects are stored and received via
     * pop() and popMultiple(). The user is responsible for mapping an index to the objects.
     *
     */
    class TaskQueue {
    public:

        /*!
         * Append multiple tasks to the queue by specifiying a vector of any object. The
         * enqueued items are the indices of the objects in the vector.
         */
        template<typename T>
        void append(const std::vector<T> &tasks) {
            for(unsigned int i = 0; i < tasks.size(); ++i)
                append(i);
        }

        /*!
         * Append a vector of indices.
         */
        void append(const std::vector<unsigned int> &tasks) {
            for(auto p: tasks)
                append(p);
        }

        /*!
         * Append an index to the queue.
         */
        void append(unsigned int task) {
            std::unique_lock<std::mutex> lck(_queue_lock);
            _tasks.push_back(task);
            _queue.push_back(task);
            _finished = false;
        }

        /*!
         * Mark multiple indices as finished.
         */
        void finish(const std::vector<unsigned int> &tasks) {
            for(auto p: tasks)
                finish(p);
        }

        /*!
         * Mark a single index as finished
         */
        void finish(unsigned int id) {
            std::unique_lock<std::mutex> lck(_queue_lock);

            _finished_indices.push_back(id);
            _dirty_indices.push_back(id);

            _finished = _finished_indices.size() == _tasks.size();
            //printf("%lu %lu\n", _finished_indices.size(), _tasks.size());
            if(_finished) {
                _finished_conditional.notify_all();
            }
        }

        /*!
         * Check if the queue is finished, i.e., is empty.
         */
        bool finished() {
            return _finished;
        }

        /*!
         * Check if queue is dirty, i.e., elements are finished but no
         * clean() was called since.
         */
        std::vector<unsigned int> getDirtyAndClean() {
            std::unique_lock<std::mutex> lck(_queue_lock);
            std::vector<unsigned int> d = _dirty_indices;
            _dirty_indices.clear();
            return d;
        }

        /*!
         * Returns the ratio of indices that are finished yet.
         */
        float progress() {
            std::unique_lock<std::mutex> lck(_queue_lock);

            return _finished_indices.size() / (float) _tasks.size();
        }

        /*!
         * Reset the queue, i.e., clear it.
         */
        void reset() {
            std::unique_lock<std::mutex> lck(_queue_lock);

            _finished_indices.clear();
            _queue.clear();
            _tasks.clear();
            _finished = false;
        }

        /*!
         * Get the size of the queue
         */
        unsigned int totalSize() {
            return (unsigned int) _tasks.size();
        }

        /*!
         * Retrieve a single index from the queue.
         */
        bool pop(unsigned int &t) {
            std::unique_lock<std::mutex> qlck(_queue_lock);

            unsigned int current_queue_size = (unsigned int) _queue.size();

            if(current_queue_size == 0)
                return false;

            t = _queue.back();
            _queue.pop_back();

            return true;
        }

        /*!
         * Retrieve multiple indices from the queue.
         */
        std::vector<unsigned int> popMultiple(unsigned int number) {
            std::unique_lock<std::mutex> qlck(_queue_lock);

            unsigned int current_queue_size = (unsigned int) _queue.size();

            std::vector<unsigned int> ret;

            if(number > current_queue_size)
                number = current_queue_size;

            for(unsigned int i = 0; i < number; ++i) {
                unsigned int id = _queue.back();
                ret.push_back(id);
                _queue.pop_back();
            }

            return ret;
        }

        /*!
         * Wait until the queue is finished.
         */
        void waitUntilFinished() {
            std::unique_lock<std::mutex> qlck(_queue_lock);
            while(!_finished) {
                _finished_conditional.wait(qlck);
            }
        }


    private:
        std::mutex _queue_lock;
        std::vector<unsigned int> _tasks;
        std::vector<unsigned int> _finished_indices;
        std::vector<unsigned int> _queue;
        bool _finished{false};
        std::vector<unsigned int> _dirty_indices;
        std::condition_variable _finished_conditional;
    };

}

#endif //STEMSALABIM_WORKQUEUE_HPP
