/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#cmakedefine HAVE_CURL 1

#define PKG_VERSION_MAJOR "@PACKAGE_VERSION_MAJOR@"
#define PKG_VERSION_MINOR "@PACKAGE_VERSION_MINOR@"
#define PKG_VERSION_PATCH "@PACKAGE_VERSION_PATCH@"
#define PKG_NAME "@PACKAGE_NAME@"
#define PKG_AUTHOR "@PACKAGE_AUTHOR@"
#define PKG_AUTHOR_EMAIL "@PACKAGE_AUTHOR_EMAIL@"
#define BUILD_TYPE "@CMAKE_BUILD_TYPE@"
#define BUILD_DATE "@DATE@"
#define GIT_COMMIT_HASH "@GIT_SHA1@"