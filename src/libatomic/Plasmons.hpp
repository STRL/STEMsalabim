/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef STEMSALABIM_PLASMONS_HPP
#define STEMSALABIM_PLASMONS_HPP

#include <memory>
#include <mutex>
#include <algorithm>
#include <cmath>

#include <gsl/gsl_integration.h>

#include "../utilities/algorithms.hpp"
#include "../classes/GridManager.hpp"
#include "../classes/IO.hpp"

namespace stemsalabim { namespace atomic {

    class Plasmons {
    public:

        static Plasmons &getInstance() {
            static Plasmons instance;
            return instance;
        }

        void populateCache(const std::shared_ptr<GridManager> &gridman, double electron_density) {
            if(_cache_populated)
                return;

            Params &p = Params::getInstance();

            if(!p.plasmonsEnabled())
                return;

            unsigned int lx = gridman->samplingX();
            unsigned int ly = gridman->samplingY();
            double sdense2_x = pow(1. / 3. * gridman->densityX(), 2);
            double sdense2_y = pow(1. / 3. * gridman->densityY(), 2);

            _cache.init(lx, ly);
            _cache.setIsKSpace(true);

            for(unsigned int ix = 0; ix < lx; ix++) {
                for(unsigned int iy = 0; iy < ly; iy++) {
                    double theta = sqrt(pow(gridman->kx(ix), 2) + pow(gridman->ky(iy), 2)) * p.wavelength();

                    if(!p.bandwidthLimiting() ||
                       pow(gridman->kx(ix), 2) / sdense2_x + pow(gridman->ky(iy), 2) / sdense2_y < 1) {

                        // Only calculate at single energy instead of integrating, as it was done in former versions.
                        _cache(ix, iy) = plasmonDoubleDifferentialCrossSection(p.energyLoss(), theta, electron_density);
                    }
                }
            }
            _cache.backwardFFT();

            double cutoffPixel = gridman->scalingFactorNanometerToGridPixelX() * p.plasmonsScatteringfunctionCutoffNanometer();

            double measureX, measureY;
            for(unsigned int ix = 0; ix < lx; ix++) {
                for(unsigned int iy = 0; iy < ly; iy++) {
                    if ((ix > 0.5 * lx )) {
                        measureX = (double) ix - (double) lx;
                    }
                    else{
                        measureX = (double) ix;
                    }
                    if ((iy > 0.5 * ly )) {
                        measureY = (double) iy - (double) ly;
                    }
                    else{
                        measureY = (double) iy;
                    }

                    if (sqrt(measureX*measureX + measureY*measureY) > cutoffPixel){
                        _cache(ix, iy) = 0;
                    }

                }

            }

            // Take here the square root of the scattering function.
            // It has to be done AFTER it is transformed to real space!
            _cache.sqareRoot();

            _cache_populated = true;

        }

        const Wave &scatteringFunction() const {
            return _cache;
        }

        double beta0() const {
            return _beta_0;
        }

        double beta1() const {
            return _beta_1;
        }

        double beta2() const {
            return _beta_2;
        }

    private:
        explicit Plasmons() {

            Params &p = Params::getInstance();

            _plasmon_energy = p.plasmonEnergy();
            _plasmon_fwhm = p.plasmonFWHM();
            _incident_energy = p.beamEnergy();
            _beta_0 = exp(-p.sliceThickness() / p.plasmonMeanFreePath());
            _beta_1 = p.sliceThickness() / p.plasmonMeanFreePath() * _beta_0;
            _beta_2 = p.sliceThickness() / p.plasmonMeanFreePath() * 0.5 * _beta_1;
        }

    /*!
     * Calculates the double differential cross section for plasmon scattering.
     * The formula is taken from Gunawan's dissertation (2013), page 65.
     * Quantities are in the units:
             * energies in  [eV]
             * lengths in   [nm]
             * angles in    [rad]
     * @param E energy in eV
     * @param theta scattering angle in rad
     * @param electronDensity electron density in [1 / (nm^3)]
     * @return double differential cross section
     */
        double
        plasmonDoubleDifferentialCrossSection(double E, double theta, double electronDensity) {
            // FWHM (also known as damping coefficient) of energy loss function [ev]
            double DeltaEp = _plasmon_fwhm;


            double incident_energyEV = _incident_energy * 1000; // incident electron energy [eV]

            // fermiFactor = hbar^2 / (2 m_0) * (3 * pi^2)^(2/3) in [eV nm^2]
            // with the electron mass m_0 = 9.109 * 10^-31 kg
            double fermiFactor = 0.36464500;                    // [eV nm^2]
            // electronDensity in [nm^(-3)]
            double fermiEnergy = pow(electronDensity, 2.0 / 3.0) * fermiFactor;  // [eV]

            // gamma: density dependent constant
            double gamma = 3.0 / 5.0 * fermiEnergy / _plasmon_energy;

            double m0 = 9.1093837015 * 1E-31;                   // electron rest mass in [kg]
            double c = 299792458.0;                             // speed of light in vacuum in [m/s]
            double m0timesCsquared = m0 * pow(c, 2);         // [Joule]
            double jouleToeV =  1.0 / (1.602176634 * 1E-19);    // convert Joule to eV //
                                                                //
            m0timesCsquared *= jouleToeV;                       // convert to [ev]

            // Characteristic angle in [rad]
            double thetaE = E / incident_energyEV * (incident_energyEV + m0timesCsquared) / (incident_energyEV + 2 * m0timesCsquared);

            // Due to normalization in STEMsalabim it does not make a difference if there is a constant prefactor or not.
            // But parts which are functions of the angle of course have to be implemented.
            double prefactor = (sin(theta)) / (theta * theta + thetaE * thetaE);

            double numerator = E * DeltaEp * _plasmon_energy * _plasmon_energy;

            double denominator = pow((E * E - _plasmon_energy * _plasmon_energy -
                               gamma * 4 * _plasmon_energy * incident_energyEV * (theta * theta + thetaE * thetaE)), 2) +
                          E * E * DeltaEp * DeltaEp;

            // Plasmon double differential cross section, taken from Gunawan's dissertation (2013), page 65
            return prefactor * numerator / denominator;
        }


        double _incident_energy;
        double _plasmon_energy;
        double _plasmon_fwhm;
        double _beta_0;
        double _beta_1;
        double _beta_2;
        bool _cache_populated{false};

        Wave _cache;

        /// pi
        constexpr static double pi = 3.14159265358979323846;
        /// electron charge
        constexpr static double e = 1.60217662e-19;
        /// electron rest mass in keV. \f$m_0  c^2\f$ in keV
        constexpr static double ELECTRON_REST_MASS = 510.99906;
        /// bohr radius
        constexpr static double a0 = 5.2917721067e-11;
    };

}}
#endif //STEMSALABIM_PLASMONS_HPP
