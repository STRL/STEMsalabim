/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_ELEMENT_HPP_
#define STEMSALABIM_ELEMENT_HPP_

#include <utility>
#include <string>
#include <iostream>
#include <sstream>
#include <map>

#ifdef __GNUG__
#if __GNUG__ > 6
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
#endif
#endif

#include "../3rdparty/jsoncpp/json/json.h"

#ifdef __GNUG__
#if __GNUG__ > 6
#pragma GCC diagnostic pop
#endif
#endif

#include "elements_json.hpp"

namespace stemsalabim { namespace atomic {


    /*!
     * Class representing a chemical element along with its scattering parameters.
     */
    class Element {

    public:

        template<class Archive>
        void serialize(Archive &archive) {
            archive(_name,
                    _symbol,
                    _atomic_number,
                    _atomic_radius,
                    _atomic_weight,
                    _atomic_radius,
                    _valence,
                    _lattice_constant,
                    _scattering_params_dt,
                    _lattice_structure);
        }

        /*!
         * Constructor
         */
        Element(std::string name, std::string symbol, unsigned atomic_number)
                : _name(std::move(name))
                , _symbol(std::move(symbol))
                , _atomic_number(atomic_number)
                , _scattering_params_dt(12) {
        };

        /*!
         * Default constructor needed for std::map
         */
        Element() = default;

        /*!
         * Get the element's symbolic description.
         */
        std::string symbol() const {
            return _symbol;
        }

        /*!
         * Get the full name of the element.
         */
        std::string name() const {
            return _name;
        }

        /*!
         * Get the atomic number of the element.
         */
        unsigned int atomicNumber() const {
            return _atomic_number;
        }

        /*!
         * Get the atomic weight of the element.
         */
        float atomicWeight() const {
            return _atomic_weight;
        }

        /*!
         * Get the atomic valence of the element.
         * @return atomic valence
         */
        int valence() const {
            return _valence;
        }

        /*!
         * Set atomic weight of the element.
         */
        void setAtomicWeight(float atomic_weight) {
            _atomic_weight = atomic_weight;
        }

        /*!
         * Get atomic radius of the element.
         */
        float atomicRadius() const {
            return _atomic_radius;
        }

        /*!
         * Set atomic radius of the element.
         */
        void setAtomicRadius(const float atomic_radius) {
            _atomic_radius = atomic_radius;
        }

        /*!
         * Set atomic valence of the element.
         * @param valence atomic valence
         */
        void setValence(const int valence) {
            _valence = valence;
        }

        /*!
         * Set the Doyle Turner scattering parameters of the element.
         * They are encoded as a sequence in a vector of doubles, where
         * the elements are:
         * a1, b1, a2, b2, a3, b3, c1, d1, c2, d2, c3, d3
         */
        void setScatteringParamsDT(const std::vector<double> &s) {
            _scattering_params_dt = s;
        }

        /*!
         * Get the Doyle Turner scattering parameters of the element.
         * They are encoded as a sequence in a vector of doubles, where
         * the elements are:
         * a1, b1, a2, b2, a3, b3, c1, d1, c2, d2, c3, d3
         */
        const std::vector<double> &scatteringParamsDT() const {
            return _scattering_params_dt;
        }

        /*!
         * Get the lattice constant of the element when in solid phase.
         */
        float latticeConstant() const {
            return _lattice_constant;
        }

        /*!
         * Set the lattice constant of the element when in solid phase.
         */
        void setLatticeConstant(const float lattice_constant) {
            _lattice_constant = lattice_constant;
        }

        /*!
         * Get a string representation of the lattice structure when crystallized.
         */
        const std::string &latticeStructure() const {
            return _lattice_structure;
        }

        /*!
         * Set a string representation of the lattice structure when crystallized.
         */
        void setLatticeStructure(std::string const &lattice_structure) {
            _lattice_structure = lattice_structure;
        }

    private:
        int _valence;
        std::string _name{""};
        std::string _symbol{""};
        unsigned _atomic_number{0};
        float _atomic_weight{0};
        float _atomic_radius{0};
        float _lattice_constant{0};
        std::vector<double> _scattering_params_dt;
        std::string _lattice_structure{""};
    };

/*!
 * Singleton class for reading and storing elements via the elements_json.hpp file.
 */
    class ElementProvider {
    public:
        /*!
         * The only way to get an instance of ElementProvider is via this function.
         */
        static ElementProvider &getInstance() {
            static ElementProvider instance;
            return instance;
        }

        /*!
         * Return an Element via its symbol string
         */
        std::shared_ptr<Element> elementBySymbol(std::string const &symbol) {
            return _elements[symbol];
        }

        /*!
         * Check if element is known by its symbol
         */
        bool hasElement(std::string const &symbol) {
            return _elements.count(symbol) > 0;
        }

    private:
        /*!
         * Private constructor to be called only once.
         * Here, we read element data via elements_json.hpp
         */
        ElementProvider() {

            if(ElementProvider::_elements.empty()) {

                Json::Value root;
                Json::Reader reader;
                bool parsingSuccessful = reader.parse(elements_json, root, false);
                if(!parsingSuccessful) {
                    printf("Failed to parse elements json");
                    exit(1);
                }

                for(Json::ValueIterator itr = root.begin(); itr != root.end(); itr++) {
                    auto &val = (*itr);
                    std::shared_ptr<Element> e(new Element(itr.key().asString(),
                                                           val.get("symbol", "").asString(),
                                                           val.get("atomic_number", 0).asUInt()));

                    e->setAtomicWeight(val.get("atomic_weight", 0.f).asFloat());
                    e->setValence(val.get("valence_electrons", 0).asInt());

                    try {
                        // convert pm to angstrom
                        e->setAtomicRadius(val.get("atomic_radius", 0.f).asFloat());
                    } catch(const Json::LogicError &err) {
                        e->setAtomicRadius(0.0);
                    }

                    std::vector<double> scattering_params_dt(12);

                    scattering_params_dt[0] = val["scattering_dt"].get("a1", 0).asDouble();
                    scattering_params_dt[1] = val["scattering_dt"].get("b1", 0).asDouble();
                    scattering_params_dt[2] = val["scattering_dt"].get("a2", 0).asDouble();
                    scattering_params_dt[3] = val["scattering_dt"].get("b2", 0).asDouble();
                    scattering_params_dt[4] = val["scattering_dt"].get("a3", 0).asDouble();
                    scattering_params_dt[5] = val["scattering_dt"].get("b3", 0).asDouble();
                    scattering_params_dt[6] = val["scattering_dt"].get("c1", 0).asDouble();
                    scattering_params_dt[7] = val["scattering_dt"].get("d1", 0).asDouble();
                    scattering_params_dt[8] = val["scattering_dt"].get("c2", 0).asDouble();
                    scattering_params_dt[9] = val["scattering_dt"].get("d2", 0).asDouble();
                    scattering_params_dt[10] = val["scattering_dt"].get("c3", 0).asDouble();
                    scattering_params_dt[11] = val["scattering_dt"].get("d3", 0).asDouble();

                    e->setScatteringParamsDT(scattering_params_dt);

                    _elements[val.get("symbol", "").asString()] = e;
                }
            }


        };

        std::map<std::string, std::shared_ptr<Element>> _elements;
    };

}}
#endif // STEMSALABIM_ELEMENT_HPP_
