/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_CELL_H
#define STEMSALABIM_CELL_H

#include <tuple>
#include <vector>
#include <set>
#include <complex>
#include <memory>
#include <cmath>
#include <stdlib.h>

#include "Element.hpp"
#include "Atom.hpp"
#include "Scattering.hpp"

namespace stemsalabim { namespace atomic {

    /*!
     * The Cell class manages the crystal, references to all Atom objects and supercell size boundaries.
     */

    class Cell {

    public:

        template<class Archive>
        void serialize(Archive &ar) {
            ar(_atoms, _elements, _size_x, _size_y, _size_z, _atom_density, _volume, _electron_density);
        }

        /*!
         * Returns a reference to the vector of pointers to all Atom objects.
         */
        const std::vector<std::shared_ptr<Atom>> &getAtoms() const {
            return _atoms;
        }

        /*!
         * return the x length of the crystal in nanometer
         */
        double sizeX() const {
            return _size_x;
        }

        /*!
         * return the y length of the crystal in nanometer
         */
        double sizeY() const {
            return _size_y;
        }

        /*!
         * return the z length of the crystal in nanometer
         */
        double sizeZ() const {
            return _size_z;
        }

        /*!
         * Get the total number of atoms in the crystal
         */
        unsigned long numberOfAtoms() const {
            return _atoms.size();
        }

        /*!
         * Get the number of different elements that appear in the crystal
         */
        unsigned long numberOfElements() const {
            return _elements.size();
        }

        double atomicDensity() const {
            return _atom_density;
        }

        double electronDensity() const {
            return _electron_density;
        }

        /*!
         * Get a set of all elements contained in the crystal.
         */
        const std::set<std::shared_ptr<Element>> &elements() const {
            return _elements;
        }

        /*!
         * Add an element to the collection of elements.
         */
        void addElement(const std::shared_ptr<Element> &el) {
            _elements.insert(el);
        }

        void initScattering() {
            Scattering &sf = Scattering::getInstance();
            for(auto & e : _elements)
                sf.initPotential(e);
        }

        /*!
         * Add an element to the collection of elements.
         */
        void addAtom(const std::shared_ptr<Atom> &a) {
            _atoms.push_back(a);
            _atom_density += 1/_volume;
            _electron_density += a->getElement()->valence() / _volume;
        }

        void setLengths(float lx, float ly, float lz) {
            _size_x = lx;
            _size_y = ly;
            _size_z = lz;
            _volume = lx*ly*lz;
        }

    private:
        std::vector<std::shared_ptr<Atom>> _atoms;
        std::set<std::shared_ptr<Element>> _elements;
        double _size_x, _size_y, _size_z;

        double _volume{0};
        double _electron_density{0};
        double _atom_density{0};

    };
}}
#endif //STEMSALABIM_CELL_H
