/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_ATOM_HPP
#define STEMSALABIM_ATOM_HPP


#include <utility>
#include "Element.hpp"

namespace stemsalabim { namespace atomic {

    /*!
     * Class to represent an atom at a fixed position in space. Holds information about the
     * chemical element, position, a unique ID and the thermal mean square displacement of
     * the frozen phonon vibrations.
     */
    class Atom {

    public:
        /*!
         * Empty constructor
         */
        Atom() = default;

        template<class Archive>
        void serialize(Archive &ar) {
            ar(_x, _y, _z, _element, _msd, _id, _slice, _has_slice);
        }

        /*!
         * Constructor with initial values
         */
        Atom(double x, double y, double z, double msd, std::shared_ptr<Element> el, size_t id)
                : _x(x)
                , _y(y)
                , _z(z)
                , _element(std::move(el))
                , _msd(msd)
                , _id(id) {

        };

        /*!
         * Constructor with initial values
         */
        Atom(double x, double y, double z, double msd, std::shared_ptr<Element> el, size_t id, unsigned int slice)
                : _x(x)
                , _y(y)
                , _z(z)
                , _element(std::move(el))
                , _msd(msd)
                , _id(id)
                , _slice(slice)
                , _has_slice(true) {



        };

        /*!
         * Get the x coordinate
         */
        double getX() const {
            return _x;
        }

        /*!
         * Get the y coordinate
         */
        double getY() const {
            return _y;
        }

        /*!
         * Get the z coordinate
         */
        double getZ() const {
            return _z;
        }

        /*!
         * Get the mean square displacement
         */
        double getMSD() const {
            return _msd;
        }

        /*!
         * Get the ID of the atom
         */
        size_t getId() const {
            return _id;
        }

        /*!
         * Get the custom slice of the atom
         */
        unsigned int getSlice() const {
            return _slice;
        }

        /*!
         * true if has custom slice
         */
        bool hasSlice() const {
            return _has_slice;
        }

        /*!
         * Get the element of the atom
         */
        std::shared_ptr<Element> getElement() const {
            return _element;
        }

    private:
        double _x;
        double _y;
        double _z;
        std::shared_ptr<Element> _element;
        double _msd;
        size_t _id;
        unsigned int _slice{0};
        bool _has_slice{false};
    };
}}

#endif //STEMSALABIM_ATOM_HPP
