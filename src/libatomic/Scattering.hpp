/*
 * STEMsalabim: Magical STEM image simulations
 *
 * Authors: Jan Oliver Oelerich
 *          Damien Heimes <damien.heimes@physik.uni-marburg.de>
 *
 * Copyright (c) 2016-2019 Jan Oliver Oelerich
 * Copyright (c) 2019-2022 Damien Heimes <damien.heimes@physik.uni-marburg.de>
 * Copyright (c) 2016-2022 Structure and Technology Research Laboratory, Philipps-Universität Marburg, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * THIS SOFTWARE MUST UNDER NO CIRCUMSTANCES BE USED FOR COMMERCIAL PURPOSES!
 *
*/

#ifndef STEMSALABIM_POTENTIAL_HPP
#define STEMSALABIM_POTENTIAL_HPP

#include <memory>
#include <mutex>
#include <algorithm>

#include "Element.hpp"

namespace stemsalabim { namespace atomic {

        /*!
         * Class to calculate and cache atomic structure factors.
         */

        class Scattering {
        public:

            /*!
             * Singleton instance creation of the Scattering class
             */
            static Scattering &getInstance(double cache_grating = 720, double cutoff_radius = 0.3) {
                static Scattering instance(cache_grating, cutoff_radius);
                return instance;
            }
//
//        /*!
//         * Get the grating scale of the (cached) atomic potentials. Note, that this scale
//         * is different to that of the Crystal and phase gratings, to make it more precise.
//         */
//        static double gratingScale() {
//            Params &p = Params::getInstance();
//            int grating_atompot = 8 * algorithms::round_even(p.samplingDensity() * p.maxPotentialRadius());
//            double length_atompot = 2 * p.maxPotentialRadius();
//            return grating_atompot / length_atompot;
//        }

            /*!
             * Get index of the potential vector given some distance
             */
            int getIndex(double distance) const {
                if(fabs(distance) > _cutoff)
                    return -1;

                return (int)std::min(round(fabs(distance) * _grating), ceil(_grating * _cutoff)-1);
            }

            /*!
             * Return reference to the array of the cached potential for some element.
             */
            const std::vector<double> & getPotential(const std::shared_ptr<Element> &element)const  {
                return _cache.at(element->symbol());
            }

            /*!
             * Initialize (i.e., calculate and cache) the potential for element element
             */
            void initPotential(const std::shared_ptr<Element> &element) {
                std::unique_lock<std::mutex> lck(_cache_mtx);

                if(!_cache.count(element->symbol())) {
                    populateCache(element);
                }
            }

            static double feKirklandReal(double r, const std::shared_ptr<Element> &element) {
// Version 5:
//             if(r < 0.0018)
//                 return feKirklandReal(0.0018, element);

                std::vector<double> sp = element->scatteringParamsDT();
                double pi2 = pi * pi;
                double r2 = r * r;
                double f = pi2 * r2;
                double sum = 2 *
                             (sp[0] * std::cyl_bessel_k(0, 2 * pi * r * sqrt(sp[1])) +
                              sp[2] * std::cyl_bessel_k(0, 2 * pi * r * sqrt(sp[3])) +
                              sp[4] * std::cyl_bessel_k(0, 2 * pi * r * sqrt(sp[5]))) +
                             sp[6] / sp[7] * exp(-f / sp[7]) +
                             sp[8] / sp[9] * exp(-f / sp[9]) +
                             sp[10] / sp[11] * exp(-f / sp[11]);
                return sum * PREFACTOR_ATOMPOT;
            }

        private:
            explicit Scattering(double cache_grating = 72, double cutoff = 3.0)
                    : _cutoff(cutoff)
                    , _grating(cache_grating) {

            }

            void populateCache(const std::shared_ptr<Element> &element) {
                auto cache_len = (unsigned long)ceil(_grating * _cutoff);
                //auto v = std::vector<double>(cache_len, 0);
                _cache.emplace(element->symbol(), std::vector<double>(cache_len, 0));

                auto & v = _cache.at(element->symbol());

                double factor = pi / (2 * _cutoff * 0.15);  // the same in both
                double tapering_radius = 0.85 * _cutoff;    // the same in both

// Version 4:
                v[0] = feKirklandReal(0.5 / _grating, element);

//             for(unsigned int i = 0; i < cache_len; ++i) {
                for(unsigned int i = 1; i < cache_len; ++i) {   // starting at 1 instead of 0
                    double distance = i / _grating;
                    v[i] = feKirklandReal(distance, element);

                    // apply tapering approach to smoothen the atomic potential at the edges
                    // as explained in I. Lobato, et al, Ultramicroscopy 168, 17 (2016).
                    if(distance >= tapering_radius) {
                        v[i] *= cos((distance - tapering_radius) * factor);
                    }

                }
            }

            std::mutex _cache_mtx;
            double _cutoff;
            double _grating;

            std::map<std::string, std::vector<double>> _cache;

            /// this is \f$2\pi^2 a_0 e\f$, with \f$e = 2R_ya_0\f$ (\f$R_y = 13.6 eV\f$)
            constexpr static double PREFACTOR_ATOMPOT = 1.504120584;
            /// pi
            constexpr static double pi = 3.14159265358979323846;
        };

    }}
#endif //STEMSALABIM_POTENTIAL_HPP
